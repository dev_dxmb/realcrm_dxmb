<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/4/2019
 * Time: 3:11 PM
 */
?>
@extends('layouts.admin.layoutAdmin')
@section('cssfile')
@endsection
@section('content')
    <div class="page-header">
        <h2 class="text-success"><i class="fa fa-exchange"></i> Thống kê Giao dịch</h2>
    </div>

    <form class="filterForm row" method="GET" autocomplete="off">
        <div class="col-md-3">
            <div class="form-group">
                <div class='input-group date' id='filterFrom'>
                    <input type='text' class="form-control" placeholder="Từ ngày" name="from" value="{{ $filter['from']??'' }}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class='input-group date' id='filterTo'>
                    <input type='text' class="form-control" placeholder="Đến ngày" name="to" value="{{ $filter['to']??'' }}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i> Lọc</button>
            <button type="button" data-toggle="modal" data-target="#importModal" class="btn btn-warning btn-sm">
                <i class="fa fa-download"></i> Nhập excel
            </button>
        </div>
    </form>
    <br>

    <div>
        @if($errors->all())
            <div class="alert alert-danger backend">
                @foreach($errors->all() as $error)
                    <p><i class="fa fa-exclamation-triangle"></i> {{ $error }}</p>
                @endforeach
            </div>
        @endif
        @if(session()->get('message'))
            <div class="alert alert-success">
                <p>{{ session()->get('message') }}</p>
            </div>
        @endif
    </div>

    <div class="panel panel-info">
        <!-- Default panel contents -->
        <div class="panel-heading">Danh sách</div>
        <div class="panel-body">
            <!-- Table -->
            <div class="table-responsive">
                <table class="table table-hover table-striped" id="report_table">
                    <thead>
                    <tr>
                        <th class="text-center"><strong>#</strong></th>
                        {{--<th class="text-center"><strong>Toà nhà</strong></th>--}}
                        <th class="text-center"><strong>Dự án</strong></th>
                        <th class="text-center"><strong>Sàn, phòng ban</strong></th>
                        <th class="text-center" width="150px"><strong>Số giao dịch</strong></th>
                        @if(!$filter || $filter['from']===$filter['to'])
                        <th class="text-center"><strong>Thao tác</strong></th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @if(!isset($data) || count($data)==0)
                        <tr>
                            <td colspan="6" class="text-center">
                                <p>Không tìm thấy dữ liệu</p>
                            </td>
                        </tr>
                    @else
                        <?php $total=0; ?>
                        @foreach($data as $key => $item)
                            <?php $total+=$item->value ?>
                            <tr class="text-center">
                                <td>{{ $key+1 }}</td>
                                {{--<td>--}}
                                    {{--<label>{{ $buildings[$item->building]? $buildings[$item->building]->title : '(Không rõ)' }}</label>--}}
                                    {{--<select data-select-remote="true"--}}
                                            {{--data-url="{{ route('ajax-list-category') }}?level=2"--}}
                                            {{--name="building"--}}
                                            {{--class="form-control form-control-sm hidden"--}}
                                            {{--disabled--}}
                                            {{--data-placeholder="Chọn tòa nhà">--}}
                                    {{--</select>--}}
                                {{--</td>--}}
                                <td>
                                    <p>{{ $projects[$item->project]? $projects[$item->project]->title : '(Không rõ)' }}</p>
                                </td>
                                <td>
                                    <label>{{ $departments[$item->department]? $departments[$item->department]->text : '(Không rõ)' }}</label>
                                    <select name="department"
                                            class="form-control form-control-sm hidden"
                                            data-placeholder="Chọn sàn, phòng ban"
                                            disabled>
                                    </select>
                                </td>
                                <td>
                                    <label>
                                        <span class="badge badge-info">
                                            {{ $item->value }}
                                        </span>
                                    </label>
                                    <input disabled type="number" name="count" class="form-control form-control-sm hidden">
                                </td>
                                @if(!$filter || $filter['from']===$filter['to'])
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <button
                                            class="btn btn-warning"
                                            data-toggle="editable"
                                            data-json="{{ json_encode($item) }}"
                                        >
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button
                                            data-role="remove"
                                            data-url="{{ route('ajax-delete-bill-report', ['id' => $item->id]) }}"
                                            class="btn btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    @if(isset($total))
                        <tr>
                            <td colspan="3" class="text-right">
                                <p class="text-success"><i>TỔNG SỐ:</i></p>
                            </td>
                            <td class="text-center">
                                <span class="label label-success">{{ $total }}</span>
                            </td>
                        </tr>
                        {{--@if($filter)
                        <tr>
                            <td class="text-center" colspan="4">
                                <a href="{{ route('compose-report', [
                                'from'  => $filter['from'],
                                'to'    => $filter['to'],
                                ]) }}" class="btn btn-danger">
                                    <i class="fa fa-newspaper-o"></i> Soạn tin báo cáo
                                </a>
                            </td>
                        </tr>
                        @endif--}}
                    @endif
                    {{--<tr>--}}
                        {{--<td class="text-center" colspan="5">--}}
                            {{--<button class="btn btn-sm btn-success" data-toggle="modal" data-target="#reportForm">--}}
                                {{--<i class="fa fa-plus"></i> Thêm--}}
                            {{--</button>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <!-- Form Modal -->
    <div id="reportForm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thêm báo cáo</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('report-bill') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Chọn tòa nhà:</label>
                            <div class="col-sm-9">
                                @if(isset($buildings))
                                    <select name="building" required
                                            class="form-control form-control-sm"
                                            data-placeholder="Chọn tòa nhà">
                                        <option value="" selected>Chọn tòa nhà</option>
                                        @foreach($buildings as $key => $building)
                                            <option value="{{ $key }}">{{ $building->title . " ($building->code)" }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="department">Chọn sàn:</label>
                            <div class="col-sm-9">
                                @if(isset($departments))
                                    <select name="department" required
                                            class="form-control form-control-sm"
                                            data-placeholder="Chọn sàn, phòng ban">
                                        <option value="" selected>Chọn sàn, phòng ban</option>
                                        @foreach($departments as $department)
                                            @if($department->text)
                                            <option value="{{ $department->id }}">{{ $department->text }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="date">Ngày giao dịch:</label>
                            <div class="col-sm-9">
                                <input required type="text" class="form-control" name="date"
                                       placeholder="Chọn ngày báo cáo..."
                                       data-datepicker="true"
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="figure">Số giao dịch:</label>
                            <div class="col-sm-9">
                                <input required min="0" type="number" class="form-control" name="figure" placeholder="Nhập số giao dịch...">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success">Lưu ngay</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Import Modal -->
    <div id="importModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Nhập excel</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form action="{{ route('import-bill-report') }}" method="POST" enctype="multipart/form-data" id="importForm">
                                {{ @csrf_field() }}
                                <div class="form-group">
                                    <div class='input-group date' id="reportDate" data-datepicker="true">
                                        <input type='text' class="form-control" placeholder="Chọn ngày báo cáo" name="date" required />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file">Duyệt tập tin:</label>
                                    <input type="file" name="file" id="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6 text-right">
                            <p>
                                <a href="{{ route('bill-report-template') }}" download>
                                    <i class="fa fa-download"></i> Tải mẫu template
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button form="importForm" type="submit" class="btn btn-success">Tải lên</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('jsfile')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/vi.js"></script>
    <script>
        $(function () {
            const FROM = $('#filterFrom');
            const TO = $('#filterTo');
            FROM.datetimepicker({
                format: 'YYYY-MM-DD',
                showTodayButton: true,
                locale: 'vi',
                showClear: true
            });
            TO.datetimepicker({
                useCurrent: false,
                showTodayButton: true,
                showClear: true,
                locale: 'vi',
                format: 'YYYY-MM-DD'
            });
            FROM.on("dp.change", function (e) {
                TO.data("DateTimePicker").minDate(e.date);
            });
            TO.on("dp.change", function (e) {
                FROM.data("DateTimePicker").maxDate(e.date);
            });

            $('[data-datepicker="true"]').datetimepicker({
                format: 'YYYY-MM-DD',
                showTodayButton: true,
                locale: 'vi',
                showClear: true
            });

            const REPORT_MODAL = $('#reportForm');
            REPORT_MODAL.find('select').select2();

            $('[data-toggle="editable"]').on('click', function () {
                const json = $(this).data('json');
                REPORT_MODAL.find('select[name="building"]').val(json.building).select2({disabled: true, readonly: true});
                REPORT_MODAL.find('select[name="department"]').val(json.department).select2({disabled: true, readonly: true});
                REPORT_MODAL.find('input[name="date"]').val(json.report_date).attr({readonly: true});
                REPORT_MODAL.find('[name="figure"]').val(json.value);
                REPORT_MODAL.find('form').attr('action', '{{ route('report-bill') }}?building='+json.building+'&department='+json.department);
                REPORT_MODAL.modal('show');
            });

            $('[data-target="#reportForm"]').on('click', function () {
                REPORT_MODAL.find('select').val('').select2({disabled: false, readonly: false});
                REPORT_MODAL.find('input[name="date"]').val('').removeAttr('readonly');
                REPORT_MODAL.find('form').attr('action', '{{ route('report-bill') }}');
                REPORT_MODAL.find('[name="figure"]').val('');
            });

            $('[data-role="remove"]').on('click', function (e) {
                e.preventDefault();
                const url = $(this).data('url');
                if (confirm('Bạn có chắc muốn xóa bản ghi này?')) {
                    $.ajax(url, {
                        type: 'DELETE',
                        success: (response) => {
                            if (response.success) {
                                location.reload();
                            } else {
                                alert(response.msg);
                            }
                        },
                        error: (error) => {
                            console.log(error);
                            alert('Xóa không thành công!');
                        }
                    });
                }
            });
        });
    </script>
@endsection
