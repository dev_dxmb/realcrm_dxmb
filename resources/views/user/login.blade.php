
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Booking Online - DXMB</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo URL::to('/'); ?>/css/bootstrap.min.css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" href="<?php echo URL::to('/'); ?>/css/ie10-viewport-bug-workaround.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo URL::to('/'); ?>/css/signin.css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo URL::to('/'); ?>/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <form class="form-signin" method="post" action="">
          <input type="hidden" name="_token" value="{!! csrf_token() !!}">
          <input type="hidden" id="tbl" value="b_o_users">
        <h2 class="form-signin-heading">Đăng nhập Quản trị</h2>
        <label for="inputEmail" class="sr-only">Tài khoản</label>
        <input type="text" name="username" id="inputEmail" class="form-control" placeholder="Nhập tài khoản..."
               required autofocus>
        
        <label for="inputPassword" class="sr-only">Mật khẩu</label>
        <input type="password" name="pwd" id="inputPassword" class="form-control" placeholder="Nhập mật khẩu..." required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Nhớ mật khẩu
          </label>
        </div>
        <button
            class="btn btn-lg btn-primary btn-block"
            type="submit">Đăng nhập ngay</button>
          <br>
          <div class="text-center">
              <p>
                  <a href="/"><i class="glyphicon glyphicon-home"></i> Quay lại trang chủ</a>
              </p>
          </div>
        <input type="hidden" name="submit" value="submit">
      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo URL::to('/'); ?>/js/ie10-viewport-bug-workaround.js"></script>
    <script src="<?php echo URL::to('/'); ?>/js/jquery.min.js"></script>
    <script src="<?php echo URL::to('/'); ?>/js/global.js"></script>
  </body>
</html>
