<!--Loading bootstrap css-->
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/fonts.googleapis.com/css6f1c.css?family=Open+Sans:400italic,400,300,700">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/fonts.googleapis.com/css39f3.css?family=Oswald:400,700,300">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/font-awesome/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/bootstrap/css/bootstrap.min.css">
<!--LOADING STYLESHEET FOR PAGE-->
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/intro.js/introjs.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/calendar/zabuto_calendar.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/sco.message/sco.message.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/intro.js/introjs.css">
<!--Loading style vendors-->
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/animate.css/animate.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/jquery-pace/pace.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/iCheck/skins/all.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/jquery-notific8/jquery.notific8.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
<!--Loading style-->
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/css/themes/style1/orange-blue.css" class="default-style">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/css/themes/style1/orange-blue.css" id="theme-change" class="style-change color-change">
<link type="text/css" rel="stylesheet" href="<?php echo URL::to('/');?>/frontend/css/style-responsive.css">