@extends('layouts.admin.layoutAdmin')
@section('content')

    <h3 class="col-xs-12 no-padding text-uppercase"><?php echo (isset($i_id) ? 'Sửa Template' : 'Thêm Template').' '.($isDefault == 1 ? 'Mặc định' : 'Khác') ?></h3>
    <div class="alert alert-danger hide backend"></div>
    <form id="fileupload" class="form-horizontal" method="post" action="">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

        <div class="form-group col-md-12">
            <div class="no-padding">
                <label for="project" class="col-md-2 control-label text-left">Loại template</label>
                <div class="col-md-2 no-padding">
                    <select class="form-control" id="typeTemplate" name="type" is_default="{{ $isDefault }}" required>
                        <option value="">Chọn loại Template</option>
                        <?php foreach (\App\TemplateEmail::TYPE as $index => $title) { ?>
                        <option value="{{ $index }}" {{ $temp->type == $index ? 'selected' : '' }}>{{ $title  }}</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>

        <?php if($isDefault == 0) { ?>
        <div class="form-group col-md-12">
            <div class="no-padding">
                <label for="project" class="col-md-2 control-label text-left">Chọn Dự Án</label>
                <div class="col-md-4 no-padding">
                    <select class="form-control input-sm js-select2" id="project" name="project" required onchange="GLOBAL_JS.v_fLoadBuilding(this)">
                        <option value=""> Chọn Dự án</option>
                        @foreach ($a_Project as $o_project)
                            <option value="{{ $o_project->cb_id }}" <?php echo $o_project->cb_id == $temp->project ? 'selected' : '' ?>>{{ $o_project->cb_title }} ({{ $o_project->cb_code }})</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <?php } else { ?>
        <div class="form-group col-md-12">
            <div class="no-padding">
                <label for="project" class="col-md-2 control-label text-left">Loại Dự án</label>
                <div class="col-md-2 no-padding">
                    <select class="form-control" id="is_apartment" name="is_apartment" required>
                        <option value="">Chọn loại Dự án</option>
                        <option value="1" {{ $temp->is_apartment == 1 ? 'selected' : ''  }}>Chung cư</option>
                        <option value="0" {{ isset($i_id) && $temp->is_apartment == 0 ? 'selected' : '' }}>Liền kề, biệt thự, đất nền</option>
                    </select>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="form-group col-md-12">
            <div class="no-padding">
                <label for="code" class="col-md-2 control-label text-left">Mã template</label>
                <div class="col-md-4 no-padding">
                    <input id="code" name="code" field-name="Mã" type="text" value="<?php echo isset($temp->code) ? $temp->code : "" ?>" class="form-control" />
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <div class="no-padding">
                <label for="title" class="col-md-2 control-label text-left">Tiêu đề</label>
                <div class="col-md-4 no-padding">
                    <input id="title" name="title" field-name="Nhãn" type="text" value="<?php echo isset($temp->title) ? $temp->title : "" ?>" class="form-control" placeholder="title" required />
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <div class="no-padding">
                <label for="html" class="col-md-2 control-label text-left">Nội dung Email</label>
                <div class="col-md-10 no-padding">
                    <textarea required class="form-control html" name="html" rows="5" id="html"><?php echo isset($temp->html) ? $temp->html : "" ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <div class="no-padding">
                <label for="html_pdf" class="col-md-2 control-label text-left">Nội dung PDF</label>
                <div class="col-md-10 no-padding">
                    <textarea required class="form-control html_pdf" name="html_pdf" rows="5" id="html_pdf"><?php echo isset($temp->html_pdf) ? $temp->html_pdf : "" ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6 col-sm-3 no-padding text-right">
                <button type="reset" class="btn btn-default">Nhập lại</button>
                <input type="submit" name="submit" class="btn btn-primary btn-sm submit" value="Cập nhật">
            </div>
        </div>
    </form>


@endsection
@section('jsfile')
    <script src="<?php echo URL::to('/'); ?>/ckeditor/ckeditor.js"></script>
    <script src="<?php echo URL::to('/'); ?>/ckeditor/config.js"></script>
    <script src="<?php echo URL::to('/'); ?>/ckeditor/styles.js"></script>.
    <script src="<?php echo URL::to('/'); ?>/ckeditor/build-config.js"></script>
    <script>
        CKEDITOR.replace('html');
        CKEDITOR.replace('html_pdf');
    </script>
@endsection
