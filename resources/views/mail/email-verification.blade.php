<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2/25/2019
 * Time: 11:18 AM
 */
?>
<style>
    * {
        font-family: Avenir,Helvetica,sans-serif;
        color: #74787e;
        font-size: 16px;
        line-height: 1.5em;
    }
</style>
<div style="text-align: center">
    <a href="{{ url('/') }}"><img src="{{ asset('images/logo-text.png') }}" alt="Booking Online - DXMB" style="max-height: 100px"></a>
</div>
<h3>Xin chào {{ $name?? 'bạn' }}</h3>
<p>Chào mừng bạn đã tham gia vào hệ thống Booking Online - Đất Xanh Miền Bắc.</p>
<p>Để hoàn tất quá trình đăng ký, bạn vui lòng kích hoạt tài khoản bằng cách mở liên kết dưới đây:</p>
<div style="text-align: center; padding: 40px;">
    <a style="padding:12px 25px;background-color:#db5609;text-decoration: none;color: #fff;text-transform:uppercase;min-width: 140px;"
       href="{{ $url }}?csrf_token={{ time() }}">Kích hoạt tài khoản</a>
</div>
<br>
<hr>
<p><i>Thông tin thêm</i></p>
<ul>
    <li>Email được kích hoạt có thể giúp bạn khôi phục mật khẩu trong trường hợp không nhớ mật khẩu</li>
    <li>Nếu bạn không kích hoạt tài khoản, bạn vẫn có thể đăng nhập vào app Đất Xanh Miền Bắc</li>
    <li>Liên kết kích hoạt sẽ mất hiệu lực sau khi bạn đã mở</li>
    <li>Email đã kích hoạt có thể thay đổi</li>
</ul>
<hr>
<p>
    Regards,
    <br>
    Hệ thống Booking Online - Đất Xanh Miền Bắc
</p>
