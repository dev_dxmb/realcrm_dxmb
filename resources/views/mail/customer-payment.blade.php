
<h3>Kính chào Quý khách!</h3>
@if($status == 1)
    <p>Đất Xanh Miền Bắc xác nhận đã nhận được số tiền của Quý khách với thông tin như sau:</p>
    <p><strong>- Tên khách hàng: </strong>{{ $customer  }}</p>
    <p><strong>- Mã căn hộ: {{ $apartment_code  }}</strong></p>
    <p><strong>- Dự án: {{ $project  }}</strong></p>
    <p><strong>- Số tiền nhận: {{ $response_money }} VNĐ</strong></p>
@else
    <p>Đất Xanh Miền Bắc thông báo đã nhận được thông tin chuyển tiền của Quý khách hàng như sau:</p>
    <p><strong>- Tên khách hàng: </strong>{{ $customer  }}</p>
    <p><strong>- Mã căn hộ: {{ $apartment_code  }}</strong></p>
    <p><strong>- Dự án: {{ $project  }}</strong></p>
    <p><strong>- Số tiền KH yêu cầu nộp: {{ $request_money }} VNĐ</strong></p>
    <p>Trong vòng 24h kể từ thời điểm nhận được tiền chúng tôi sẽ thông báo lại cho Quý khách.</p>
@endif
<p><strong>TRÂN TRỌNG!</strong></p>

