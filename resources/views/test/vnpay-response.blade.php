<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>VNPAY RESPONSE</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css')  }}" rel="stylesheet"/>
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/jumbotron-narrow.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery.min.css')  }}"></script>
</head>
<body>
<?php
    $vnp_HashSecret = \App\Http\Controllers\HomeController::$vnp_HashSecret; //Chuỗi bí mật
    if (!$_GET['vnp_SecureHash']) {
        return false;
    }
    $vnp_SecureHash = $_GET['vnp_SecureHash'];
    $inputData = array();
    foreach ($_GET as $key => $value) {
        if (substr($key, 0, 4) == "vnp_") {
            $inputData[$key] = $value;
        }
    }
    unset($inputData['vnp_SecureHashType']);
    unset($inputData['vnp_SecureHash']);
    ksort($inputData);
    $i = 0;
    $hashData = "";
    foreach ($inputData as $key => $value) {
        if ($i == 1) {
            $hashData = $hashData . '&' . $key . "=" . $value;
        } else {
            $hashData = $hashData . $key . "=" . $value;
            $i = 1;
        }
    }

    $secureHash = md5($vnp_HashSecret . $hashData);
?>
<!--Begin display -->
<div class="container">
    <div class="header clearfix">
        <h3 class="text-muted">VNPAY RESPONSE</h3>
    </div>
    <div class="table-responsive">
        <div class="form-group">
            <label >Mã đơn hàng:</label>

            <label>{{ $_GET['vnp_TxnRef']  }}</label>
        </div>
        <div class="form-group">

            <label >Số tiền:</label>
            <label>{{ $_GET['vnp_Amount']  }}</label>
        </div>
        <div class="form-group">
            <label >Nội dung thanh toán:</label>
            <label>{{ $_GET['vnp_OrderInfo'] }}</label>
        </div>
        <div class="form-group">
            <label >Mã phản hồi (vnp_ResponseCode):</label>
            <label>{{ $_GET['vnp_ResponseCode'] }}</label>
        </div>
        <div class="form-group">
            <label >Mã GD Tại VNPAY:</label>
            <label>{{ $_GET['vnp_TransactionNo'] }}</label>
        </div>
        <div class="form-group">
            <label >Mã Ngân hàng:</label>
            <label>{{ $_GET['vnp_BankCode'] }}</label>
        </div>
        <div class="form-group">
            <label >Thời gian thanh toán:</label>
            <label>{{ $_GET['vnp_PayDate'] }}</label>
        </div>
        <div class="form-group">
            @if($secureHash == $vnp_SecureHash)
                <div class="alert {{$_GET['vnp_ResponseCode'] == '00'? 'alert-success' : 'alert-warning'}}">
                    <p>{{$_GET['vnp_ResponseCode'] == '00'? 'Giao dịch thành công' : 'Giao dịch không thành công'}}</p>
                </div>
            @else
                <div class="alert alert-danger">
                    <p>Chữ ký không hợp lệ!</p>
                </div>
            @endif
        </div>
    </div>
    <p>
        &nbsp;
    </p>
    <footer class="footer">
        <p>&copy; VNPAY 2015</p>
    </footer>
</div>
</body>
</html>
