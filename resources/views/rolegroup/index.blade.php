@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Danh sách</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="form-group">
        <input id="rg_title" name="rg_title" type="text" class="form-control input-sm" placeholder="Nhập tên" value="<?php echo isset($a_search['rg_title'])?$a_search['rg_title']:''?>">
    </div>
    <div class="form-group">
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="rg_staff_id" name="rg_staff_id" class="js-select2">
                    <option value=""> Chọn </option>
                    @foreach ($a_Users as $a_User)
                    <option value="{{$a_User->ub_id}}" <?php if(isset($a_search['rg_staff']) && $a_search['rg_staff'] == $a_User->ub_id) echo"selected"; ?>>{{$a_User->ub_account_tvc}}</option>
                    @endforeach
                </select>
            </div>
    </div>
    
    <div class="form-group">
        <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
        <input type="submit" class="btn btn-success btn-sm submit hide">
    </div>
</form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tên Nhóm Quyền</strong></td>
                <td class="bg-success"><strong>Mô Tả</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($a_RoleGroup as $a_val)
            <tr>
                <td>    {{ $a_val->stt }}</td>
                <td>    {{ $a_val->rg_title }}</td>
                <td>    {{ $a_val->rg_description }}</td>
                <td>
                    <?php
                        if($a_val->rg_status == 1){
                    ?>
                    <a title="Edit" href="<?php echo Request::root().'/role_group/addedit?id='.$a_val->id;?>" title="Edit" class="not-underline">
                        <i class="fa fa-edit fw"></i>
                    </a>
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},1,'rg_status')" title="Cho vào thùng rác" class="not-underline">
                    <i class="fa fa-trash fa-fw text-danger"></i>
                    </a>&nbsp;
<!--                    <a title="Config" href="<?php echo Request::root().'/roles/addedit?rolegroupID='.$a_val->id;?>" title="Config" class="not-underline">
                        <i class="fa fa-wrench"></i>
                    </a>-->
                    <?php }else if($a_val->rg_status == 0){ ?>
                    <a title="Khôi phục user" href="javascript:GLOBAL_JS.v_fRecoverRow({{ $a_val->id }},'rg_status')"  title="Edit" class="not-underline">
                        <i class="fa fa-upload fw"></i>
                    </a>&nbsp;
                    <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $a_val->id }},0,'rg_status')" title="Xóa vĩnh viễn" class="not-underline">
                        <i class="fa fa-trash-o fa-fw text-danger"></i>
                    </a>&nbsp;
<!--                    <a title="Config" href="<?php echo Request::root().'/roles/addedit?rolegroupID='.$a_val->id;?>" title="Config" class="not-underline">
                        <i  class="fa fa-wrench"></i>
                    </a>-->
                    <?php }?>
                </td>
            </tr>
        @endforeach
            
        
        </table>
        
              
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="b_o_role_groups">
<?php echo (empty($a_search)) ? $a_RoleGroup->render(): $a_RoleGroup->appends($a_search)->render();?>

@endsection