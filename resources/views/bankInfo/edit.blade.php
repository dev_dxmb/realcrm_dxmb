@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == '' ? 'Thêm ngân hàng' : 'Sửa ngân hàng' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="b_o_customers">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="bank_title" class="col-xs-12 col-sm-3 control-label text-left">Tên Ngân Hàng</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="bank_title" name="bank_title" field-name="Tên" type="text" value="<?php echo isset($a_BankData->bank_title) ? $a_BankData->bank_title : "" ?>" class="form-control check-duplicate" placeholder="Tên Ngân Hàng" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="bank_code" class="col-xs-12 col-sm-3 control-label text-left">Code</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="bank_code" name="bank_code" field-name="Số điện thoại" type="text" value="<?php echo isset($a_BankData->bank_code) ? $a_BankData->bank_code : "" ?>" class="form-control check-duplicate" placeholder="Code" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="bank_holder" class="col-xs-12 col-sm-3 control-label text-left">Chủ Tài Khoản</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="bank_code" name="bank_holder" field-name="bank_holder" type="text" value="<?php echo isset($a_BankData->bank_holder) ? $a_BankData->bank_holder : "" ?>" class="form-control check-duplicate" placeholder="Chủ Tài Khoản" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="bank_number" class="col-xs-12 col-sm-3 control-label text-left">Số Tài Khoản</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="bank_code" name="bank_number" field-name="bank_number" type="text" value="<?php echo isset($a_BankData->bank_number) ? $a_BankData->bank_number : "" ?>" class="form-control check-duplicate" placeholder="Số Tài Khoản" required />
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="payment_method_ids" class="col-xs-12 col-sm-3 control-label text-left">Chọn PTTT</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select class="form-control input-sm js-select2" id="payment_method_ids" name="payment_method_ids[]" multiple="multiple">
                    <option value="0"><span class="text-center">Chọn PTTT</span></option>
                    @if(count($paymentMethod) > 0)
                        @foreach($paymentMethod as $key => $val )
                        <option value="{{$val->pm_id}}" <?php if(isset($a_BankData->payment_method_ids) &&  in_array($val->pm_id, (array) json_decode($a_BankData->payment_method_ids))) echo"selected"; ?> > {{$val->pm_title}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="branch_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn chi nhánh</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="branch_id" name="branch_id" class="form-control input-sm ">
                    <option value="1">Chi nhánh 1</option>
                    <option value="2">Chi nhánh 2</option>
                    <option value="3">Chi nhánh 3</option>
                    <option value="4">Chi nhánh 4</option>
                    <option value="5">Chi nhánh 5</option>
                    <option value="6">Chi nhánh 6</option>
                </select>

            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-3 no-padding">
            <label for="bank_status" class="col-xs-6 control-label text-left">Trạng thái</label>
            <div class="col-xs-6 no-left-padding">
                <input id="bank_status" name="bank_status" type="checkbox" class="form-control" <?php if (isset($a_BankData->bank_status) && $a_BankData->bank_status): ?>checked<?php endif ?>>
            </div>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitProjectValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>


@endsection
