@extends('layouts.admin.layoutAdmin')
@section('content')
    <h3 class="col-xs-12 no-padding text-uppercase">Danh sách các yêu cầu Lock căn</h3>
    <div class="">
        <table id="table_id" class="table table-responsive table-hover table-striped table-bordered">
            <thead>
                <tr class="header-tr">
                    <td class="bg-success"><strong>STT</strong></td>
                    <td class="bg-success"><strong>Thông tin căn hộ</strong></td>
                    <td class="bg-success"><strong>Nhân viên </strong></td>
                    <td class="bg-success"><strong>Ngày yêu cầu Lock</strong></td>
                    <td class="bg-success"><strong>Trạng thái</strong></td>

                    <td class="bg-success"><strong>Hành động</strong></td>
                </tr>
            </thead>
            <tbody>
            @foreach ($a_AllTransaction as $index => $a_val)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>
                        <p><strong>Mã căn hộ:</strong> {{ isset($a_val->product)?$a_val->product->code:'-' }}</p>
                        <p><strong>Tòa:</strong> {{ isset($a_val->product->category)?$a_val->product->category->building:'-' }}</p>
                        <p><strong>Dự án:</strong> {{ isset($a_val->product->category->sibling)?$a_val->product->category->sibling->project:'-' }}</p>
                    </td>
                    <td>
                        <p><strong>Họ tên:</strong> {{ isset($a_val->staff)?$a_val->staff->name:'-' }}</p>
                        <p><strong>Phòng ban:</strong> {{ isset($a_val->staff->group)? $a_val->staff->group->name : '-' }}</p>
                    </td>
                    <td>{{ $a_val->trans_created_time }}</td>
                    <td>{{ $a_val->trans_title }}</td>
                    <td>
                        <?php if($a_val->trans_code == 'R-LCK') { ?>
                        <p><a href="/transaction/changeStatus?trans_id={{ $a_val->trans_id }}&code=M-LCK" class="btn btn-sm btn-primary">Đồng ý Lock</a></p>
                        <p><a href="/transaction/changeStatus?trans_id={{ $a_val->trans_id }}&code=M-CANCEL" class="btn btn-sm btn-danger">Từ chối Lock</a></p>
                        <?php } else if ($a_val->trans_code == 'R-CANCEL') { ?>
                        <p><a href="/transaction/changeStatus?trans_id={{ $a_val->trans_id }}&code=M-CANCEL" class="btn btn-sm btn-primary" onclick="return confirm('Hành động không thể hoàn tác. Bạn có muốn tiếp tục?')">Đồng ý y/c Hủy</a></p>
                        <?php } ?>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_transactions">

@endsection