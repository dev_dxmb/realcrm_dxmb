@extends('layouts.admin.layoutAdmin')
@section('content')

<div class="col-xs-6 alert alert-success">
  <strong>Chú ý!</strong> bạn cần nhập file (xls, xlsx) <br/>
  File phải bao gồm các cột là (*) phone, name (tên), email
</div>
<h3 class="col-xs-12 no-padding">Tải Nhâp Khách Hàng</h3>
<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
    @if(isset($a_Res))
    <div class="form-group">
        <div class="col-xs-12 col-sm-12 no-padding">
            <label class="alert alert-warning">{!! $a_Res !!}</label>
        </div>
    </div>
    @endif
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ro_key" class="col-xs-12 col-sm-3 control-label text-left">Tải File Mẫu Tại Đây</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <a href="/TaiNhapKhachHang.xlsx">Click here</a>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ro_key" class="col-xs-12 col-sm-3 control-label text-left">Chọn File</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <input type="file" name="excel" id="excel" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ro_key" class="col-xs-12 col-sm-3 control-label text-left">Chọn Dự Án</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="project_id" name="project_id"  class="js-select2">
                    <option value=""> Chọn Project</option>
                    @foreach ($a_projects as $a_project)
                    <option value="{{$a_project->cb_id}}">{{$a_project->cb_title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="assign_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn Thư ký Để Phân bổ</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="assign_id" name="assign_id[]"  class="js-select2" multiple="multiple">                    
                    @if(count($a_Saffs) > 0)
                        @foreach($a_Saffs as $key => $valStaff )
                        <option value="{{$valStaff->ub_id}}"> {{$valStaff->ub_account_tvc}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="note" class="col-xs-12 col-sm-3 control-label text-left">Ghi Chú</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control" name="note" rows="5" placeholder="Ghi Chú..."></textarea>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="submit" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm ">
        </div>
    </div>
</form>

@endsection