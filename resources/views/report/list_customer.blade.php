@extends('layouts.admin.layoutAdmin')
@section ('cssfile')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<style>
.alert {
    padding: 3px !important;}
.pagination {margin: 0 !important;
}
    
</style>

@stop
@section('content')
    
     <div class="row">
    <form method="get" action="" id="frmFilter" name="frmFilter"  >
        <!-- <input type="hidden" name="_token" value="{!! csrf_token() !!}"> -->
        <div class='col-lg-12'>
            <div class="col-lg-4" style="text-align:left;">
                <h3 class="no-padding text-uppercase">Danh sách khách hàng <a class="btn btn-primary btn-xs" href="{{url('report/1/list-action/?type_report=2')}}">
                                <i class="fa fa fa-bar-chart-o"></i> Xem biểu đồ
                            </a></h3>

            </div>
           <div class="col-lg-4"></div>
        </div>
        <div class='row'>
             <div class="form-group col-lg-2">
            <input  class="form-control" value = "{{isset($request['r_text'])?$request['r_text']:''}}" name = 'r_text' id='r_text' placeholder="Nhập tên khách hàng.." onchange="this.form.submit()">
        </div>
       
        <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="department_id" name="department_id" onchange="this.form.submit()">
                <option value="">Tìm theo sàn</option>
                @foreach ($r_departments as $department)
                    <option @if(isset($request['department_id'])&&$request['department_id']==$department->gb_id) selected @endif value="{{ $department->gb_id }}">{{ $department->gb_title }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-2">
            <select class="form-control js-select2" id="r_staff_id" name="staff_id" onchange="this.form.submit()">
                <option value="">Tìm theo nhân viên</option>
                @foreach ($r_staffs as $r_staff)
                    <option @if(isset($request['staff_id'])&&$request['staff_id']==$r_staff->ub_id) selected @endif value="{{ $r_staff->ub_id }}">{{ $r_staff->ub_tvc_code }}-{{ $r_staff->ub_title }}</option>
                @endforeach
            </select>
        </div>
       
        
        <div class="form-group col-lg-3">
          <div class="input-group" id="datepicker">
           
             <input type="text" class="input-sm form-control input-daterange" name="start_date" value="{{isset($request['start_date'])&&$request['start_date']?$request['start_date']:''}}" placeholder="từ ngày"  />                    
            <span class="input-group-addon">tới</span>
            <input type="text" class="input-sm form-control input-daterange" name="end_date" value="{{isset($request['end_date'])&&$request['end_date']?$request['end_date']:date('dd-mm-YYYY')}}" />
        </div>
        </div>
        
        <div class="form-group col-lg-1">
                  
            <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
            <input type="submit" class="btn btn-success btn-sm submit hide">
        </div>
    </div>
    <div class='row' >

        <div class="alert alert-danger col-lg-1 " text-align="right" style="margin-left:20px">
        Tổng số: <b> {{ $list_customer->total() }}</b>
        </div> 
         <div class="col-lg-1">
            <input hidden name = 'export_excel' id='export_excel'>
          
            <label class="checkbox-inline btn btn-primary btn-sm" onclick="exportExcel(this.form);"><i class="fa fa-download" >Xuất file Excel</i></label>
        </div>
        <div class='col-lg-9'> 
            <div class="form-group col-lg-2"> 
            <select class="form-control js-select2" id="itemperpage" name="itemperpage" onchange="this.form.submit()">
                <option value="20" @if(isset($request['itemperpage'])&&$request['itemperpage']==20) selected @endif >20 kết quả /trang</option>
                <option value="50" @if(isset($request['itemperpage'])&&$request['itemperpage']==50) selected @endif >50 kết quả /trang</option>
                <option value="0" @if(isset($request['itemperpage'])&&$request['itemperpage']==0) selected @endif >Tất cả </option>
               
            </select> 
            </div>
            <div class='col-lg-8'>  {{ $list_customer->links() }}</div>
        <input hidden name = 'kind' value="{{isset($request['kind'])?$request['kind']:'bill'}}">    
   
    </div>
     </form>
   </div>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered" id = 'example-export'>
            <thead class="header-tr">
                <th class="bg-success"><strong>STT</strong></th>     
                <th class="bg-success"><strong>Nhân viên - sàn</strong></th>           
                
                 <th class="bg-success"><strong>Mã khách hàng</strong></th>
             
                  <th class="bg-success"><strong>Title</strong></th>
                <th class="bg-success"><strong>Thông tin</strong></th>              
                
                <th class="bg-success"><strong>Địa chỉ </strong></th>   
                           
                <th class="bg-success"><strong>CMT/Passport</strong></th>
                
                <th class="bg-success"><strong>Ảnh</strong></th>
                <th class="bg-success"><strong>Tạo lúc</strong></th>
                <th class="bg-success"><strong>Cập nhật lúc</strong></th>
               
            </thead>     
           
            @foreach ($list_customer as $index =>  $trans)
                <tbody>
                    <tr>
                        <td>{{ $index + 1 }}</td>
                         <td>{{ isset($trans->bo_user)?$trans->bo_user->ub_title:'-' }} 
                            <br/>{{ isset($trans->bo_user->group)?$trans->bo_user->group->gb_title:'-' }}</td>
                        <td>Mã TN:  <b>{{ $trans->potential_reference_code }}</b><br/>Mã CT: <b> {{ $trans->customer_reference_code }}</b></td>
                        <td>{{ $trans->c_title }}</td>
                        <td>Họ tên: <b>{{ $trans->c_name }}</b><br/>Điện thoại:  <b>{{ $trans->c_phone }}</b><br/>Email: <b> {{ $trans->email }}</b>
                        <td>Địa chỉ: <b>{{ $trans->c_address }}</b><br/>Thường trú:<b>{{ $trans->permanent_address }}</b></td>
                        <td>Số CMT/HC:  <b>{{ $trans->id_passport }}</b><br/>Ngày cấp: {{ $trans->issue_date }}</b><br/> Nơi cấp: <b>{{ $trans->issue_place }}</b></td>  
                        <td >@if(is_array($trans->c_images)) @foreach($trans->c_images as $image) <img style="width:100px;" src="{{ url($image) }}" /> @endforeach @endif</td>                            
                        <td>{{ date('d-m-Y h:s:i', strtotime($trans->created_at)) }}</td>
                        <td>{{ date('d-m-Y h:s:i', strtotime($trans->updated_at)) }}</td>
                                              
                    </tr>
                </tbody>
            @endforeach
            
        </table>
        {{ $list_customer->links() }}
    </div>
    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_bills">
@endsection
@section('jsfile')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script> 

<script type="text/javascript">
    $('.js-select2').select2();
    $('.input-daterange').datetimepicker({ format: 'DD-MM-YYYY' });
      function exportExcel(){
        alert('Hoàn thiện sau');
        // $('#export_excel').val(1);
        form = $('form[name="frmFilter"]');
        // form.submit();
    }
</script>

@endsection