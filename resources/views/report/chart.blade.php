
@extends('layouts.admin.layoutAdmin')
@section('content')
    <form class="row" method="get" action="" id="frmFilter" name="frmFilter"  >
        <!-- <input type="hidden" name="_token" value="{!! csrf_token() !!}"> -->
        <div class='col-lg-12'>
            <div class="col-lg-4" style="text-align:left;">
                <h3 class="no-padding text-uppercase">Biểu đồ <a class="btn btn-primary btn-xs" href="{{url('report/1/list-action')}}">
                                <i class="fa fa fa-bar-chart-o"></i> Xem danh sách
                            </a> </h3>
            </div>
           
        </div>
        <div class='col-lg-12'>
       
        <div class="form-group col-lg-3">
          <div class="input-daterange input-group" id="datepicker">
            <input type="text" class="input-sm form-control" name="start_date" id="start_date" value="{{date('d-m-Y',strtotime('-7 days'))}}"  />
            <span class="input-group-addon">tới</span>
            <input type="text" class="input-sm form-control" name="end_date" id="end_date" value="{{date('d-m-Y')}}" />
        </div>
        </div>
        
        <div class="form-group col-lg-1">
                  
            <input type="button" class="btn btn-success btn-sm" value="Lọc tìm kiếm" onclick="thisweekstatistic()">
            <input type="submit" class="btn btn-success btn-sm submit hide">
        </div>
    </div>
    </form>
       <div class="row">
       
        <div class="col-md-6" > 
            <div id="department-pie-chart"></div>
        </div>
        
          <div class='col-md-6'> 
            <div id="staff-pie-chart"></div>
        </div>

        </div>
    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_bills">
@endsection
@section('jsfile')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="{{url('js/jquery-highcharts/highcharts.js')}}"></script>
<script src="{{url('js/jquery-highcharts/data.js')}}"></script>
<script src="{{url('js/jquery-highcharts/charts-highchart-pie-ccn.js')}}"></script>
<!-- <script src="http://code.highcharts.com/highcharts.js"></script> -->
<script type="text/javascript">
   
    function thisweekstatistic(){
        // var input = [];
        // input['project_id'] = $('#project_id').val();
        // input['department_id'] =$('#department_id').val();
        // input['start_date'] = $('#start_date').val();
        // input['end_date'] = $('#end_date').val();
                
        $.ajax({
                // data: input,
                url: "{{url('report/1/getStatisticCCN')}}"
            })
            .done(function(data) {
                LoadSampleCCN('department-pie-chart','Tỷ lệ doanh thu theo chi nhánh',data.department);
                LoadSampleCCN('staff-pie-chart','Tỷ lệ doanh thu theo nhân viên',data.staff);
                // alert('sUCCESS');
            });
    }
</script>
<script type="text/javascript">
    try{

        thisweekstatistic();        
        $('.js-select2').select2();
        $('.datePicker').datepicker({
            format: 'mm/dd/yyyy',
        });
    }catch(e)
    {
        console.log(e);
    }
    
</script>
@endsection

