<?php
$roleMenuWeb = \App\Http\Controllers\RoleController::getRoleMenuWebByUserId();
?>

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

            <li class="<?php if(isset($roleMenuWeb['report_admin'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i><b> Siêu báo cáo </b><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li ><a class="" href="{{url('/report/1/listAction')}}">Dashboard</a></li>
                    <li ><a class="" href="<?php echo Request::root() ."/report/1/listAction?kind=transaction" ?>">Thống kê yêu cầu</a></li>
                    <li ><a class="" href="<?php echo Request::root() ."/report/1/listAction?kind=bill" ?>">Thống kê hợp đồng</a></li>
                    <li ><a class="" href="<?php echo Request::root() ."/report/1/listAction?kind=product" ?>">Thống kê sản phẩm</a></li>
                    <li ><a class="" href="<?php echo Request::root() . "/report/1/listAction?kind=customer" ?>">Thống kê khách hàng</a></li>
                    <li ><a class="" href="<?php echo Request::root() . "/report/1/listAction?kind=staff" ?>">Thống kê nhân viên</a></li>



                </ul>
            </li>

            <li class="<?php if(isset($roleMenuWeb['management_customer'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i> Quản lý Khách Hàng<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['management_customer']) && in_array('import', $roleMenuWeb['management_customer'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/customer/import" ?>">Tải nhập Khách Hàng</a></li>
                    <li class="<?php if(isset($roleMenuWeb['management_customer']) && in_array('file_manager', $roleMenuWeb['management_customer'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/customer/file_management" ?>">Danh Sách File</a></li>
                    <li class="<?php if(isset($roleMenuWeb['management_customer']) && in_array('list', $roleMenuWeb['management_customer'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/customer" ?>">Danh Sách Khách Hàng</a></li>                    
                    <li class="<?php if(isset($roleMenuWeb['management_customer']) && in_array('offical_customer', $roleMenuWeb['management_customer'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/offical_customer" ?>">Khách Hàng Chính Thức</a></li>
                </ul>
            </li>
            <li class="<?php if(isset($roleMenuWeb['management_bank'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i> Quản lý Ngân Hàng<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['management_bank']) && in_array('list', $roleMenuWeb['management_bank'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/bank" ?>">Danh Sách Ngân Hàng</a></li>
                    <li class="<?php if(isset($roleMenuWeb['management_bank']) && in_array('sync', $roleMenuWeb['management_bank'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/syncBankTVC" ?>">Đồng Bộ Ngân Hàng</a></li>
                <!--<li class="<?php if(isset($roleMenuWeb['management_bank']) && in_array('addedit', $roleMenuWeb['management_bank'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/bank/addedit" ?>">Thêm / Sửa ngân hàng</a></li> -->
                </ul>
            </li>
            <li class="<?php if(isset($roleMenuWeb['management_payment_method'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i> Quản lý Phương Thức TT<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['management_payment_method']) && in_array('list', $roleMenuWeb['management_payment_method'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/payment_medthod" ?>">Danh Sách PTTT</a></li>
                    <li class="<?php if(isset($roleMenuWeb['management_payment_method']) && in_array('add', $roleMenuWeb['management_payment_method'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/payment_medthod/addedit" ?>">Thêm mới PTTT</a></li>
                </ul>
            </li>
            <li class="<?php if(isset($roleMenuWeb['management_role_group'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user-secret"></i> Quản lý nhóm quyền<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['management_role_group']) && in_array('list', $roleMenuWeb['management_role_group'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/role_groups" ?>">Danh Sách</a></li>
                    <li class="<?php if(isset($roleMenuWeb['management_role_group']) && in_array('add', $roleMenuWeb['management_role_group'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/role_group/addedit" ?>">Thêm mới</a></li>
                </ul>
            </li>
            @if(isset($roleMenuWeb['management_role']))
            <li id="menu_role">
                <a href="#"><i class="fa fa-key"></i> Phân quyền<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    @if(in_array('list', $roleMenuWeb['management_role']))
                    <li><a href="{{ route('role-list') }}">Danh Sách Quyền</a></li>
                    @endif
                    @if(in_array('add', $roleMenuWeb['management_role']))
                    <li><a href="{{ route('role-form') }}">Thêm quyền</a></li>
                    @endif
                    @if(in_array('list_menu', $roleMenuWeb['management_role']))
                    <li><a href="{{ route('app-menu') }}">[APP] Quyền Menu</a></li>
                    @endif
                    @if(in_array('config_menu', $roleMenuWeb['management_role']))
                    <li><a href="{{ route('app-menu-config') }}">[APP] Phân quyền Menu</a></li>
                    @endif
                    @if(in_array('list_menu_web', $roleMenuWeb['management_role']))
                    <li><a href="{{ route('web-menu') }}">[WEB] Quyền Menu</a></li>
                    @endif
                    @if(in_array('config_menu_web', $roleMenuWeb['management_role']))
                    <li><a href="{{ route('web-menu-config') }}">[WEB] Phân quyền Menu</a></li>
                    @endif
                </ul>
            </li>
            @endif

            <li class="<?php if(isset($roleMenuWeb['payment'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i> Thanh Toán<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['payment']) && in_array('list', $roleMenuWeb['payment'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/payment" ?>">Danh Sách</a></li>
                    <li class="<?php if(isset($roleMenuWeb['payment']) && in_array('add', $roleMenuWeb['payment'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/payment/addnew" ?>">Thêm đợt thanh toán</a></li>
                </ul>
            </li>

            <li class="<?php if(isset($roleMenuWeb['user'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i> Nhân Viên<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['user']) && in_array('list', $roleMenuWeb['user'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/userBO" ?>">Danh Sách</a></li>
                    <li class="<?php if(isset($roleMenuWeb['user']) && in_array('add', $roleMenuWeb['user'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/userBO/addedit" ?>">Thêm Nhân Viên</a></li>
                </ul>
            </li>

            <li class="<?php if(isset($roleMenuWeb['category'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i>Dự án - Tòa nhà<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['category']) && in_array('list', $roleMenuWeb['category'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/category" ?>">Danh sách</a></li>
                    <li class="<?php if(isset($roleMenuWeb['category']) && in_array('add', $roleMenuWeb['category'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/category/addedit" ?>">Thêm mới</a></li>
                </ul>
            </li>

            <li class="<?php if(isset($roleMenuWeb['investor'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i>Chủ đầu tư<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['investor']) && in_array('list', $roleMenuWeb['investor'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/investor" ?>">Chủ đầu tư</a></li>
                    <li class="<?php if(isset($roleMenuWeb['investor']) && in_array('add', $roleMenuWeb['investor'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/investor/addedit" ?>">Thêm chủ đầu tư</a></li>
                </ul>
            </li>

            <li class="<?php if(isset($roleMenuWeb['product_list'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i> Bảng hàng<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['product_list']) && in_array('import', $roleMenuWeb['product_list'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/apartment/import" ?>">Tải nhập bảng hàng</a></li>
                    <li class="<?php if(isset($roleMenuWeb['product_list']) && in_array('list', $roleMenuWeb['product_list'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/apartment" ?>">Danh sách bảng hàng</a></li>
                    <li class="<?php if(isset($roleMenuWeb['product_list']) && in_array('tranfer', $roleMenuWeb['product_list'])) echo''; else echo'hidden';?>"><a href="<?php echo Request::root() . "/syncProduct" ?>">Chuyển nhượng bảng hàng</a></li>
                    <li class="<?php if(isset($roleMenuWeb['product_list']) && in_array('crawprice', $roleMenuWeb['product_list'])) echo''; else echo'hidden';?>"><a href="<?php echo Request::root() . "/crawPrice" ?>">Kéo giá TVC về BO</a></li>
                    <li class="<?php if(isset($roleMenuWeb['product_list']) && in_array('crawproduct', $roleMenuWeb['product_list'])) echo''; else echo'hidden';?>"><a href="<?php echo Request::root() . "/crawProduct" ?>">Kéo BH từ TVC về BO</a></li>
                </ul>
            </li>

            @if(isset($roleMenuWeb['booking_online']))
            <li>
                <a href="#"><i class="fa fa-user"></i>Book Online<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    @if(in_array('request_lock', $roleMenuWeb['booking_online']))
                    <li><a class="" href="{{ route('list-transaction') }}">Quản lý yêu cầu Lock</a></li>
                    @endif
                    @if(in_array('management_bill', $roleMenuWeb['booking_online']))
                    <li><a class="" href="{{ route('list-bill') }}">Quản lý Hợp đồng</a></li>
                    @endif
                </ul>
            </li>
            @endif

            <li class="<?php if(isset($roleMenuWeb['management_template'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i> Template Email<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['management_template']) && in_array('list', $roleMenuWeb['management_template'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/template_email" ?>">Danh Sách Template</a></li>
                    <li class="<?php if(isset($roleMenuWeb['management_template']) && in_array('add', $roleMenuWeb['management_template'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/template_email/addedit?is_default=1" ?>">Thêm Template mặc định</a></li>
                    <li class="<?php if(isset($roleMenuWeb['management_template']) && in_array('add', $roleMenuWeb['management_template'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/template_email/addedit?is_default=0" ?>">Thêm Template Khác</a></li>
                </ul>
            </li>
            <li class="<?php if(isset($roleMenuWeb['customer_notice'])) echo''; else echo'hidden';?>">
                <a href="#"><i class="fa fa-user"></i> Thông báo khách hàng<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    <li class="<?php if(isset($roleMenuWeb['customer_notice']) && in_array('list', $roleMenuWeb['customer_notice'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/customer_notice/list" ?>">Danh Sách Thông báo</a></li>
                    <li class="<?php if(isset($roleMenuWeb['customer_notice']) && in_array('add', $roleMenuWeb['customer_notice'])) echo''; else echo'hidden';?>"><a class="" href="<?php echo Request::root() . "/customer_notice/addedit" ?>">Thêm Thông báo</a></li>
                </ul>
            </li>
            @if(isset($roleMenuWeb['post']))
            <li>
                <a href="#"><i class="fa fa-newspaper-o"></i> QL Tin tức & báo cáo<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" aria-expanded="false">
                    @if(in_array('list', $roleMenuWeb['post']))
                    <li><a class="" href="{{ route('post-list') }}?category=0">Tin phòng ban</a></li>
                    <li><a class="" href="{{ route('post-list') }}?category=1">Tin dự án</a></li>
                    @endif
                    @if(in_array('report', $roleMenuWeb['post']))
                    <li><a href="{{ route('report-bill') }}">Báo cáo giao dịch</a></li>
                    @endif
                </ul>
            </li>
            @endif
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
