@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase">Danh sách Khách Hàng</h3>
<form method="get" action="" id="frmFilter" name="frmFilter"  class="form-inline">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    
    <div class="form-group">
        <input id="cb_name" name="cb_name" type="text" class="form-control input-sm" placeholder="Nhập tên" value="<?php echo isset($a_search['cb_name'])?$a_search['cb_name']:''?>">
    </div>
    
    <div class="form-group">
        
        <select class="form-control input-sm " id="source_id" name="source_id">
                    <option value=""><span class="text-center">Chọn Nguồn</span></option>
                    @if(count($sourceCustomer) > 0)
                        @foreach($sourceCustomer as $key => $val )
                        <option value="{{$key}}" <?php echo isset($a_search['source_id']) && $a_search['source_id'] == $key ? 'selected':''?> > {{$val}}</option>
                        @endforeach
                    @endif
        </select>
    </div>
    
    <div class="form-group">
        <input type="button" class="btn btn-success btn-sm" value="Tìm kiếm" onclick="GLOBAL_JS.v_fSearchSubmitAll()">
        <input type="submit" class="btn btn-success btn-sm submit hide">
    </div>
    
    <div class="form-group">
        <input type="button" class="btn btn-warning btn-sm" value="Phân Bổ" onclick="GLOBAL_JS.v_fTransferData()">
    </div>
</form>
    <div class="">
        <table class="table table-responsive table-hover table-striped table-bordered">
            <tr class="header-tr">
                <th class="bg-success"><input type="checkbox" id="check_all" class="checkAll"></th>
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Tên File</strong></td>
                <td class="bg-success"><strong>Dự Án</strong></td>
                <td class="bg-success"><strong>Thư Ký</strong></td>
                <td class="bg-success"><strong>Người Tạo</strong></td>
                <td class="bg-success"><strong>Thời gian Tạo</strong></td>
                <td class="bg-success"><strong>Ghi Chú</strong></td>
                <td class="bg-success"><strong>Phản Hồi</strong></td>
            </tr>
            @foreach ($a_Customer as $a_val)
            <tr>
                <td><input type="checkbox" class="chk_item" value="<?php echo $a_val->id?>" name="check[]"/></td>
                <td>    {{ $a_val->stt }}</td>
                <td><a href="/customer/tranfer-customer?id={{ $a_val->cf_id }}">    {{ $a_val->cf_title }}</a></td>
                <td>    {{ $a_val->projectname }}</td>
                <td>    {{ $a_val->cf_assign_name }}</td>
                <td>    {{ $a_val->created_by_staff }}</td>
                <td>    {{ $a_val->cf_create_time }}</td>
                <td>    {{ $a_val->cf_note }}</td>
                <td>    Phản Hồi </td>                
            </tr>
        @endforeach
            
        
        </table>
        
              
    </div>

<!--Hidden input-->
<input type="hidden" name="tbl" id="tbl" value="b_o_customers">
<?php echo (empty($a_search)) ? $a_Customer->render(): $a_Customer->appends($a_search)->render();?>

@endsection