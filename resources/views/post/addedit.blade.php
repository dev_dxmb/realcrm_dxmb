@extends('layouts.admin.layoutAdmin')
@section('content')
    <div class="page-header">
        <h2><i class="fa fa-edit"></i> {{ $item? 'Cập nhật' : 'Thêm' }} <small><a href="{{ route('post-list') }}">tin nội bộ</a></small></h2>
    </div>
    @if($errors->all())
        <div class="alert alert-danger backend">
            @foreach($errors->all() as $msg)
                <p><i class="fa fa-exclamation-triangle"></i> {{ $msg }}</p>
            @endforeach
        </div>
    @endif
    @if(session()->get('message'))
        <div class="alert alert-success">
            <p>{{ session()->get('message') }}</p>
            <p><a href="{{ route('post-list') }}"><i class="fa fa-arrow-left"></i> Quay lại danh sách</a></p>
        </div>
    @endif
    <form id="postForm" method="post" class="row" action="{{ request()->url() }}" autocomplete="on">
        {{ csrf_field() }}
        <main class="col-md-8">
            <div class="panel panel-primary">
                <div class="panel-heading">Thông tin cơ bản</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="title" class="control-label text-left">Tiêu đề (*)</label>
                        <input id="title" name="title" type="text" value="{{ $item->title??'' }}"
                               class="form-control check-duplicate" placeholder="Tiêu đề bài" required
                        />
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label text-left">Mô Tả ngắn (*)</label>
                        <div>
                            <textarea maxlength="500" class="form-control" name="description" rows="2" placeholder="Mô Tả Ngắn..." required>{{ $item->description??'' }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content" class="control-label text-left">Nội dung (*)</label>
                        <div>
                            <textarea required class="form-control tinymce" name="content" rows="5" id="content" placeholder="Nội dung...">{{ $item->raw_content??'' }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        {{--<input type="hidden" name="category" value="{{ $item->category?? Request::get('category')?? 0 }}">--}}
        <aside class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">Thiết lập khác</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="target_departments" class="control-label text-left">Sàn/Phòng ban nhận tin:</label>
                        <div>
                            <select autocomplete="off" required id="target_departments" name="target_departments[]" class="form-control js-select2" multiple>
                                <option {{ isset($item)&&$item->post_target&&in_array('all', $item->post_target->department_ids)? 'selected' : '' }} value="all">** Tất cả ({{ count($groups) }}) **</option>
                                <optgroup label="Tùy chọn">
                                @foreach($groups as $group)
                                    <option {{ isset($item)&&$item->post_target&&in_array($group->{\App\BOUserGroup::ID_KEY}, $item->post_target->department_ids)? 'selected' : '' }}
                                            value="{{ $group->{\App\BOUserGroup::ID_KEY} }}">- {{ $group->title }}</option>
                                @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="control-label text-left">Danh mục:</label>
                        <div>
                            <label style="font-weight: normal;">
                                <input {{ !$item||!Request::get('category')||$item->category==0? 'checked' : '' }} required name="category" type="radio" value="0"> Tin phòng ban
                            </label>
                            <label style="margin: 0 15px;font-weight: normal;">
                                <input {{ ($item&&$item->category==1)||(Request::get('category')==1)? 'checked' : '' }} required name="category" type="radio" value="1"> Tin dự án
                            </label>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="control-label text-left">Hiển thị:</label>
                        <div>
                            <label style="font-weight: normal;">
                                <input {{ !$item||$item->type==0? 'checked' : '' }} required name="type" type="radio" value="0"> Tin ảnh
                            </label>
                            <label style="margin: 0 15px;font-weight: normal;">
                                <input {{ $item&&$item->type==1? 'checked' : '' }} required name="type" type="radio" value="1"> Tin Video
                            </label>
                            <label style="font-weight: normal;">
                                <input {{ $item&&$item->type==2? 'checked' : '' }} required name="type" type="radio" value="2"> Trích dẫn
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label text-left">Mức độ Ưu tiên:</label>
                        <div>
                            <label style="font-weight: normal;">
                                <input {{ !$item||$item->priority==0? 'checked' : '' }} required name="priority" type="radio" value="0"> Bình thường
                            </label>
                            <label style="margin: 0 15px;font-weight: normal;">
                                <input {{ $item&&$item->priority==1? 'checked' : '' }} required name="priority" type="radio" value="1"> Nổi bật
                            </label>
                            <label style="font-weight: normal;">
                                <input {{ $item&&$item->priority==2? 'checked' : '' }} required name="priority" type="radio" value="2"> Hiện lên trang chủ
                            </label>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="control-label">Loại phản hồi:</label>
                        <div>
                            <label style="font-weight: normal;">
                                <input autocomplete="off" {{ !$item||$item->feedback_type==0? 'checked' : '' }} required name="feedback_type" type="radio" value="0"> Văn bản
                            </label>
                            <label style="margin: 0 15px;font-weight: normal;">
                                <input autocomplete="off" {{ $item&&$item->feedback_type==1? 'checked' : '' }} required name="feedback_type" type="radio" value="1"> Bình chọn
                            </label>
                            <label style="font-weight: normal;">
                                <input autocomplete="off" {{ $item&&$item->feedback_type==2? 'checked' : '' }} required name="feedback_type" type="radio" value="2"> Check in
                            </label>
                        </div>
                    </div>
                    <div class="form-group {{ $item&&$item->feedback_type==1? 'visible' : 'hidden' }}" id="vote_option" style="padding-left: 20px;border: 1px solid lightgrey;">
                        <fieldset>
                            <legend>
                                <h5><i>-- Lựa chọn Vote --</i></h5>
                            </legend>
                            <div>
                                @if($item && $item->feedback_options)
                                    @foreach($item->feedback_options as $option)
                                        <div class="input-group input-group-sm" style="margin-bottom: 3px;">
                                            <span class="input-group-addon" data-role="remove-row">
                                                <i class="fa fa-remove text-danger"></i>
                                            </span>
                                            <input placeholder="Nhập lựa chọn..." class="form-control input-sm" type="text"
                                                   name="feedback_options[]" value="{{ $option }}">
                                        </div>
                                    @endforeach
                                @endif
                                <div class="input-group input-group-sm hidden" style="margin-bottom: 3px;" id="defaultOption">
                                    <span class="input-group-addon" data-role="remove-row">
                                        <i class="fa fa-remove text-danger"></i>
                                    </span>
                                    <input placeholder="Nhập lựa chọn..." class="form-control input-sm" type="text" name="feedback_options[]" disabled>
                                </div>
                            </div>
                            <p class="text-center">
                                <button data-role="clone-row" data-target="#defaultOption" class="btn btn-xs btn-success" title="Thêm lựa chọn">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </p>
                        </fieldset>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="project" class="control-label text-left">Dự án liên quan</label>
                        <div>
                            <select class="form-control input-sm js-select2" multiple="multiple" id="project" name="projects[]">
                                <option value="0"><span class="text-center">Chọn Dự án</span></option>
                                @if(count($projects) > 0)
                                    @foreach($projects as $project )
                                        <option {{ $item&&is_array($item->bo_category_ids)&&in_array($project->{\App\BOCategory::ID_KEY}, $item->bo_category_ids)? 'selected' : '' }}
                                                value="{{$project->{\App\BOCategory::ID_KEY} }}">{{$project->cb_title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="video" class="control-label text-left">Link Video</label>
                        <div>
                            <input id="video" name="video" type="text" value="{{ $item->video?? '' }}" class="form-control" placeholder="Link Video" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tags" class="control-label text-left">Tags</label>
                        <div>
                            <input id="tags" name="tags" type="text" value="{{ $item->tags?? '' }}" class="form-control" placeholder="Các tag cách nhau bởi dấu phẩy...." />
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <h4 title="Ảnh đầu tiên sẽ được chọn làm ảnh đại diện" class="text-secondary">
                    <label><i class="fa fa-image"></i> Ảnh bài viết</label>
                </h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="pictureTable">
                        <thead>
                        <tr>
                            <th width="70px" class="text-center">Thứ tự</th>
                            <th>
                                Xem trước
                            </th>
                            <th class="text-center"><i class="fa fa-cogs"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="hidden draggable">
                            <td width="70px" class="text-center" style="vertical-align:middle;">
                                <button class="btn btn-xs btn-warning drag-area"><i class="fa fa-list-ol"></i></button>
                            </td>
                            <td>
                                <input type="hidden" name="images[]" readonly disabled>
                                <a class="fancybox_image" href="">
                                    <img src="#" class="img-responsive upload-preview" style="max-height: 50px;">
                                </a>
                            </td>
                            <td class="text-center" style="vertical-align:middle;">
                                <button type="button" data-role="remove" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>
                        @if($item && $item->pictures)
                            @foreach($item->pictures as $key => $picture)
                            <tr class="draggable">
                                <td width="70px" class="text-center" style="vertical-align:middle;">
                                    <button class="btn btn-xs btn-warning drag-area"><i class="fa fa-list-ol"></i></button>
                                </td>
                                <td>
                                    <input type="hidden" name="images[]" readonly value="{{ asset($picture) }}">
                                    <a rel="album_{{ $key }}" href="{{ asset($picture) }}" class="fancybox_image">
                                        <img src="{{ asset($picture) }}" class="img-responsive upload-preview" style="max-height: 50px;">
                                    </a>
                                </td>
                                <td class="text-center" style="vertical-align:middle;">
                                    <button type="button" data-role="remove" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3" class="text-center">
                                <input type="hidden" name="__pic" id="__pic">
                                <a href="{{ url('plugin/filemanager/filemanager/dialog.php?type=1&field_id=__pic&flrd=') }}"
                                   class="btn btn-sm btn-primary btn-fancybox" type="button">
                                    <i class="fa fa-file-image-o"></i> Thêm ảnh
                                </a>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="col-xs-12">
                <fieldset>
                    <legend>
                        <label for="notification" class="control-label text-left"><i class="fa fa-bell-o"></i> Thông báo app</label>
                    </legend>
                    <div class="target_type">
                        <label for="to_all">
                            @if($group_all)
                                <input {{ (!isset($target_type)||($target_type=='all'))? 'checked' : '' }} id="to_all" type="radio" required name="target_type" value="all"> Tất cả người dùng ({{ count($users) }})
                            @else
                                <input id="to_all" type="radio" required name="target_type" readonly disabled>
                                <span class="text-danger" title="Không thể gửi tới tất cả. Liên hệ BQT!"> Tất cả người dùng&nbsp;<i class="fa fa-exclamation-triangle"></i></span>
                            @endif
                        </label>
                        <label for="to_department" style="margin: 0 30px;">
                            <input {{isset($target_type)&&$target_type=='group'? 'checked' : ''}} id="to_department" type="radio" required value="department" name="target_type"> Tới phòng ban
                        </label>
                        <label for="to_user">
                            <input {{ isset($target_type)&&$target_type=='user'? 'checked' : '' }} id="to_user" type="radio" required value="user" name="target_type"> Tới người dùng
                        </label>
                    </div>
                    <div id="targets" style="min-height: 60px;">
                        <div id="users" style="visibility: hidden;">
                            <select id="notified_users" name="notified_users[]" class="form-control js-select2" multiple data-placeholder="Chọn người nhận thông báo..."></select>
                        </div>
                        <div id="groups" style="visibility: hidden;">
                            <select id="notified_departments" name="notified_departments[]" class="form-control" multiple data-placeholder="Chọn phòng ban nhận thông báo..."></select>
                        </div>
                        <input type="hidden" name="__users" value="{{ isset($users)? json_encode($users) : '' }}">
                        <input type="hidden" name="__groups" value="{{ isset($groups)? json_encode($groups) : '' }}">
                        <input type="hidden" name="__notified_targets" value="{{ isset($targets)? json_encode($targets) : '' }}">
                    </div>
                    @if(isset($item))
                        <div class="alert alert-warning text-center">
                            <label for="renotify">
                                <input id="renotify" type="checkbox" name="renotify"> Gửi lại thông báo (xóa thông báo cũ)
                            </label>
                        </div>
                    @endif
                </fieldset>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-12 text-center">
                <button type="reset" class="btn btn-default">Nhập lại</button>
                <button class="btn btn-success" type="submit">Lưu lại</button>
            </div>
        </div>
    </form>

@endsection


@section('cssfile')
    <link rel="stylesheet" href="{{ asset('plugin/fancybox-2.1.7/source/jquery.fancybox.css') }}" />
@endsection


@section('jsfile')
    <script src="{{ asset('plugin/fancybox-2.1.7/source/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('js/drag-arrange.min.js') }}"></script>
    <script>
        function getEncodedData(fieldName) {
            let field = $('input[name="'+fieldName+'"]'), decoded_value = null;
            if (field.length) {
                let data = field.val();
                if (data && data.trim()!=='') {
                    // console.log('Decoded: ', JSON.parse(data));
                    decoded_value = JSON.parse(data);
                } else {
                    console.log('Empty value: ' + fieldName);
                }
            }
            return decoded_value;
        }
        function setValuesSelect2(fieldID, data = [], selections = [], idKey = 'id', textKey = 'text', comparedKey = 'id', suffixKey = '') {
            let formattedData = [], selected = [];
            $.each(data, (key, item) => {
                let text = item[textKey];
                if (suffixKey!='' && item[suffixKey]) text += ` - ${item[suffixKey]}`;
                let obj = {
                    id: item[idKey],
                    text
                };
                formattedData.push(obj);
                if (selections && selections.includes(item[comparedKey])) {
                    selected.push(item[idKey]);
                }
            });
            $('#'+fieldID).empty().select2({
                data: formattedData,
            });
            if (selected.length>0) $('#'+fieldID).val(selected).trigger('change');
        }
        function toggleTargetType(type) {
            let select_div = $('#targets');
            select_div.find('div').fadeOut('fast');
            if (type==='department') {
                select_div.find('#groups').fadeIn().removeAttr('style');
            } else if (type === 'user') {
                select_div.find('#users').fadeIn().removeAttr('style');
            }
        }
        const pictureTable = $('#pictureTable');
        function responsive_filemanager_callback(field_id){
            let url= $('#'+field_id).val();
            console.log('update '+field_id+" with "+url);
            let row = pictureTable.find('tr.hidden').clone().removeClass('hidden');
            row.find('input[name="images[]"]').removeAttr('disabled').val(url);
            row.find('a.fancybox_image').attr('href', url).fancybox();
            row.find('img.upload-preview').attr('src', url);
            pictureTable.find('tbody').append(row);
            /** Trigger row dragging */
            $('.draggable').arrangeable({
                dragSelector: '.drag-area'
            });
        }

        //** Jquery Functions
        $(function () {
            let notifiedTargets = getEncodedData('__notified_targets'),
                targetType = $('input[name="target_type"]:checked').val();
            let groups  = getEncodedData('__groups'),
                users = getEncodedData('__users');
            if (groups) {
                setValuesSelect2(
                    'notified_departments',
                    groups,
                    targetType==='department'? notifiedTargets : [],
                    '{{ \App\BOUserGroup::ID_KEY }}',
                    'title',
                    'id'
                );
            }
            if (users) {
                setValuesSelect2(
                    'notified_users',
                    users,
                    targetType==='user'? notifiedTargets : [],
                    '{{ \App\BOUser::ID_KEY }}',
                    'name',
                    'id',
                    'account'
                );
            }
            toggleTargetType(targetType);

            $('[name="target_type"]').on('change', function () {
                let type = ($(this).val());
                toggleTargetType(type);
           });
           $('[name="feedback_type"]').on('change', function () {
              if ($(this).val() == '1') {
                  $('#vote_option').removeClass('hidden').fadeIn();
              } else {
                  $('#vote_option').fadeOut();
              }
           });
           //** Responsive File Manager - Standalone with Fancybox
            $('.btn-fancybox').fancybox({
                type: 'iframe',
                autoSize: false
            });
            $('.fancybox_image').fancybox();
            $('.draggable').arrangeable({
                dragSelector: '.drag-area'
            });
            pictureTable.on('click', 'button[data-role="remove"]', function () {
               $(this).parents('tr').remove();
            });
            $('button[type="submit"]').on('click', function () {
               $(this).parents('form').submit();
            });
        });
    </script>

@endsection
