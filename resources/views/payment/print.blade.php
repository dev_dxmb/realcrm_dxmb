<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="http://bo.dxmb.vn/js/jquery.min.js"></script>

</head>
<body>
<table style="width: 100%;" border="0">
    <tbody>
    <tr style="border-style: none;">
        <td>
            <p>
                <strong>CÔNG TY CP DỊCH VỤ VÀ ĐỊA ỐC ĐẤT XANH MIỀN BẮC</strong><br />
                <em>Tầng 18, Center Building, Số 1 Nguyễn Huy Tưởng, P.Thanh Xuân Trung, Q.Thanh Xuân, Hà Nội</em>
                <br>
                <em>Điện thoại: 0462638181</em>
                <br>
                <em>FAX: 0462688811</em>
            </p>
        </td>
        <td style="width: 25%;"></td>
        <td style="width: 25%;"></td>
        <td style="text-align: right;">
            <p>
                <br>
                <strong>Mẫu số: 01-TT</strong>
                <br>
                <em>(Ban hành theo Thông tư số <br> 200/18/TT-BQL ngày: 08/03/2018)</em>
                <br />Ng&agrave;y mua: @time
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <hr>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>
            Số: {{ $payment->reference_code != NULL ? $payment->reference_code : '' }}
            <br>
            Nợ: 20.000.000 <br>
        </td>
    </tr>
    </tbody>
</table>
<h1 style="text-align: center;">PHIẾU THU</h1>
<p style="text-align: center;">Ngày: 10/12/2018</p>
<div>
    <p>Họ và tên người nộp tiền: {{ $payment->customer->cb_name }}</p>
    <p>Địa chỉ: {{ $payment->customer->cb_permanent_address }}</p>
    <p>Lý do nộp tiền: {{ $payment->pb_note }}</p>
    <p>Số tiền: {{ $payment->pb_response_money }}</p>
    <p>Kèm theo: ..........</p>
</div>
<table border="0" style="width: 100%;">
    <thead>
    <tr>
        <td colspan="5" style="text-align: right;"><em>Ngày 08 tháng 03 năm 2018</em></td>
    </tr>
    </thead>
    <tbody>
    <tr style="border-style: none;">
        <td>
            <strong>Giám đốc</strong>
            <br>
            <em>(Ký, họ tên, đóng dấu)</em>
        </td>
        <td>
            <strong>Kế toán trưởng</strong>
            <br>
            <em>(Ký, họ tên)</em>
        </td>
        <td>
            <strong>Kế nộp tiền</strong>
            <br>
            <em>(Ký, họ tên)</em>
        </td>
        <td>
            <strong>Kế lập phiếu</strong>
            <br>
            <em>(Ký, họ tên)</em>
        </td>
        <td>
            <strong>Thủ quỹ</strong>
            <br>
            <em>(Ký, họ tên)</em>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td>
            <strong>Hoàng Trung</strong>
        </td>
        <td>
            <strong>Ban Quản Lý 1</strong>
        </td>
        <td></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="5">
            <p>Đã nhận đủ số tiền (viết bằng chữ): ........................................</p>
        </td>
    </tr>
    </tfoot>
</table>
</body>
</html>
<script>
    $(document).ready(function () {
        window.print();
    });
</script>