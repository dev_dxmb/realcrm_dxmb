@extends('layouts.admin.layoutAdmin')
@section('content')
    <h3 class="col-xs-12 no-padding text-uppercase">Danh sách Chủ đầu tư</h3>
    <br>
    <div>
        <a class="btn btn-primary" href="/investor/addedit">Thêm chủ đầu tư mới</a>
    </div>
    <div class="">
        <table class="table table-responsive table-hover table-bordered">
            <tr class="header-tr">
                <td class="bg-success"><strong>STT</strong></td>
                <td class="bg-success"><strong>Mã</strong></td>
                <td class="bg-success"><strong>Tên chủ đầu tư</strong></td>
                <td class="bg-success"><strong>Ngày tạo</strong></td>
                <td class="bg-success"><strong>Ngày sửa</strong></td>
                <td class="bg-success"><strong>Action</strong></td>
            </tr>
            @foreach ($investors as $index => $investor)
                <tr>
                    <td>    {{ $index + 1 }}</td>
                    <td>    {{ $investor->code }}</td>
                    <td>    {{ $investor->name }}</td>
                    <td>    {{ $investor->created_at }}</td>
                    <td>    {{ $investor->updated_at }}</td>
                    <td>
                        <a title="Edit" href="<?php echo Request::root().'/investor/addedit?id='.$investor->id;?>" title="Edit" class="not-underline">
                            <i class="fa fa-edit fw"></i>
                        </a>
                        <a id="trash_switch_" href="javascript:GLOBAL_JS.v_fDelRow({{ $investor->id }},1,'cb_status')" title="Cho vào thùng rác" class="not-underline">
                            <i class="fa fa-trash fa-fw text-danger"></i>
                        </a>&nbsp;
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    <!--Hidden input-->
    <input type="hidden" name="tbl" id="tbl" value="b_o_categories">
    <?php echo $investors->render()?>
@endsection