@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == '' ? 'Thêm Quyền Menu' : 'Sửa Quyền Menu' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="{{ route('app-menu-config') }}">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="b_o_role_menu">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="name" class="col-xs-12 col-sm-3 control-label text-left">Tên Quyền</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="mb_title" name="mb_title" field-name="Tên" type="text" value="<?php echo isset($RoleData->mb_title) ? $RoleData->mb_title : "" ?>" class="form-control check-duplicate" placeholder="Tên Quyền" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="mb_note" class="col-xs-12 col-sm-3 control-label text-left">Mô Tả</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control mb_note" name="mb_note" rows="5" id="mb_note" placeholder="Mô Tả..."><?php echo isset($RoleData->mb_note) ? $RoleData->mb_note : "" ?></textarea>
            </div>
        </div>
    </div>
    
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="role_group_id" class="col-xs-12 col-sm-3 control-label text-left">Chọn Nhóm</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="role_group_id" name="role_group_id[]" multiple="multiple" class="js-select2">
                    @foreach ($a_RoleGroups as $a_RoleGroup)
                    <option value="{{$a_RoleGroup->rg_id}}" <?php if(isset($RoleData->role_group_id) &&  in_array($a_RoleGroup->rg_id, (array) json_decode($RoleData->role_group_id))) echo"selected"; ?>>{{$a_RoleGroup->rg_title}}</option>
                    @endforeach
                    
                </select>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="mb_menu_detail" class="col-xs-12 col-sm-3 control-label text-left">Chọn Menu</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="mb_key" name="mb_menu_detail[]" multiple="multiple" class="js-select2">
                    @foreach ($app_menu as $valaction => $label)
                    <option value="{{$valaction}}" <?php if(isset($RoleData->mb_menu_detail) &&  in_array($valaction, (array) json_decode($RoleData->mb_menu_detail))) echo"selected"; ?>>{{$label}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-3 no-padding">
            <label for="mb_status" class="col-xs-6 control-label text-left">Trạng thái</label>
            <div class="col-xs-6 no-left-padding">
                <input id="mb_status" name="mb_status" type="checkbox" class="form-control" <?php if (isset($RoleData->mb_status) && $RoleData->mb_status): ?>checked<?php endif ?>>
            </div>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitProjectValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>


@endsection
