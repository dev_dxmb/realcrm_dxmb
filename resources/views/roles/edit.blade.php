@extends('layouts.admin.layoutAdmin')
@section('content')

<h3 class="col-xs-12 no-padding text-uppercase"><?php echo $i_id == '' ? 'Thêm Quyền' : 'Sửa Quyền' ?></h3>
<div class="alert alert-danger hide backend"></div>
<form id="fileupload" class="form-horizontal" method="post" action="">

    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" value="<?php echo $i_id ?>">
    <input type="hidden" id="tbl" value="b_o_roles">
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="name" class="col-xs-12 col-sm-3 control-label text-left">Tên Quyền</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <input id="ro_title" name="ro_title" field-name="Tên" type="text" value="<?php echo isset($RoleData->ro_title) ? $RoleData->ro_title : "" ?>" class="form-control check-duplicate" placeholder="Tên Quyền" required />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ro_description" class="col-xs-12 col-sm-3 control-label text-left">Mô Tả</label>
            <div class="col-xs-12 col-sm-9 no-padding">
                <textarea class="form-control ro_description" name="ro_description" rows="5" id="ro_description" placeholder="Mô Tả..."><?php echo isset($RoleData->ro_description) ? $RoleData->ro_description : "" ?></textarea>
            </div>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ro_key" class="col-xs-12 col-sm-3 control-label text-left">Chọn Danh Mục (key)</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="ro_key" name="ro_key"  class="js-select2">
                    <option value="project" selected="">project</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ro_object_ids" class="col-xs-12 col-sm-3 control-label text-left">Chi Tiết Danh Mục</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="ro_object_ids" name="ro_object_ids[]" multiple="multiple" size="10" class="js-select2">
                    @foreach ($a_projects as $key => $val)
                    <?php
                        if($val['cb_level'] == 1){
                            $beautiFomat = '';
                        }else if($val['cb_level'] == 2){
                            $beautiFomat = '--';
                        }else if($val['cb_level'] == 3){
                            $beautiFomat = '----';
                        }
                    ?>
                    
                    <option value="{{$key}}" <?php if(isset($RoleData->ro_object_ids) &&  in_array($key, (array) json_decode($RoleData->ro_object_ids))) echo"selected"; ?>>{{$beautiFomat}}{{$val['cb_title']}}</option>
                    @endforeach
                </select>
                <input type="checkbox" id="checkboxALL" >Select All
            </div>
        </div>
    </div>

    
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ro_group_ids" class="col-xs-12 col-sm-3 control-label text-left">Chọn Nhóm</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="ro_group_ids" name="ro_group_ids[]" multiple="multiple" class="js-select2">
                    @foreach ($a_RoleGroups as $a_RoleGroup)
                    <option value="{{$a_RoleGroup->rg_id}}" <?php if(isset($RoleData->ro_group_ids) &&  in_array($a_RoleGroup->rg_id, (array) json_decode($RoleData->ro_group_ids))) echo"selected"; ?>>{{$a_RoleGroup->rg_title}}</option>
                    @endforeach
                    
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 no-padding">
            <label for="ro_role_detail" class="col-xs-12 col-sm-3 control-label text-left">Chọn Action</label>
            <div class="col-xs-12 col-sm-6 no-padding">
                <select id="ro_key" name="ro_role_detail[]" multiple="multiple" class="js-select2">
                    <option value=""> Chọn Quyền </option>
                    @foreach ($role_action as $valaction)
                    <option value="{{$valaction}}" <?php if(isset($RoleData->ro_role_detail) &&  in_array($valaction, (array) json_decode($RoleData->ro_role_detail))) echo"selected"; ?>>{{$valaction}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-xs-12 col-sm-3 no-padding">
            <label for="ro_status" class="col-xs-6 control-label text-left">Trạng thái</label>
            <div class="col-xs-6 no-left-padding">
                <input id="ro_status" name="ro_status" type="checkbox" class="form-control" <?php if (isset($RoleData->ro_status) && $RoleData->ro_status): ?>checked<?php endif ?>>
            </div>
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-xs-6 col-sm-3 no-padding">
            <button type="reset" class="btn btn-default">Nhập lại</button>
            <input type="button" name="submit" VALUE="Cập nhật" class="btn btn-primary btn-sm " onclick="GLOBAL_JS.v_fSubmitProjectValidate()"/>
            <input type="submit" name="submit" class="btn btn-primary btn-sm hide submit">
        </div>
    </div>
</form>


@endsection
