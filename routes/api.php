<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API')->group(function () {
    Route::get('/test-login', 'AuthController@testLogin');
    Route::post('/login-tvc', 'AuthController@tvcLogin');
    Route::post('/renewSession', 'APIController@__TVC_RENEW_SESSION');
    Route::group(['prefix'=>'auth'], function (){
        Route::post('login', 'AuthController@login')->name("login");
        Route::post('register', 'AuthController@register')->name("register");
        Route::post('mass-create', 'AuthController@massCreate');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
        Route::post('check-role', 'AuthController@checkRole');
        Route::post('update-fcm', 'AuthController@updateFCMToken');
    });

    /** Ajax Group */
    Route::group(['prefix' => 'ajax'], function () {
        Route::get('/list-product', 'BOProductController@getProductsAjax');
        Route::get('/list-category', 'BOCategoryController@getCategoryAjax')->name('ajax-list-category');
        Route::get('/list-department', 'BOUserController@getDepartmentAjax')->name('ajax-list-department');
        Route::delete('/bill-report/delete/{id}', 'ReportController@deleteBillReportAjax')->name('ajax-delete-bill-report');
        Route::get('/list-user', 'BOUserController@getUsersAjax')->name('ajax-list-user');
    });

    /**
     * Test Group
     */
    Route::group(['prefix' => 'test'], function () {
        Route::post('/', 'APIController@testAPI');
        Route::post('vnpay', 'APIController@testVNPay');
        Route::post('mail', 'APIController@testMail');
        Route::post('image-upload', 'APIController@testImageUpload');
        Route::post('/fcm', 'NotificationController@testFCM');
        Route::post('/sms', 'NotificationController@testSMS');
        Route::post('/dxmb', 'DXMBController@testAPI');
    });
//    Route::get('/', ['middleware' => 'laravel.jwt', 'uses' => 'AuthController@index']);

    /**
     * BOUser Group
     */
    Route::group(['prefix'=>'user'], function (){
        Route::post('/sync/dxmb-fcm', 'BOUserController@syncFCMstaffDXMB');
        Route::get('/profile/{id?}', 'BOUserController@getProfile')->where(['id' => '[0-9]+']);
        Route::post('/profile-by-account/{account?}', 'BOUserController@getProfileByAccount');
        Route::post('/profile', 'BOUserController@updateProfile');
        Route::get('/verify/{uid}/{email}', 'BOUserController@verifyEmail')->name('verify-email')->where(['uid' => '[0-9]+']);
        Route::post('/getUserInBo', 'BOUserController@index');
        Route::post('/get-roles', 'BOUserController@getAppRoles');
        Route::post('/update-avatar', 'BOUserController@changeAvatar');
        Route::post('/active', 'BOUserController@listActive');
        Route::post('/departments', 'BOUserController@getDepartmentAjax');
    });

    /**
     * BOTransaction Group
     */
    Route::group(['prefix'=>'transaction'], function (){
        Route::post('/list', 'BOTransactionController@index');
        Route::post('/show/{id}', 'BOTransactionController@show')->where(['id' => '[0-9]+']);
        Route::post('/form/{id?}', 'BOTransactionController@submitForm')->where(['id' => '[0-9]+']);
        Route::post('/update-status/{id}', 'BOTransactionController@changeStatus')->where(['id' => '[0-9]+']);
        Route::post('/add-customer', 'BOTransactionController@addTransactionCustomer');
        Route::post('/tvc-add-bill-to-contract', 'BOTransactionController@TVCAddBillToContract');
        Route::post('/tvc-update-bill-status', 'BOTransactionController@TVCUpdateBillStatus');
    });
    /**
     * BOCustomer Group
     */
    Route::group(['prefix'=>'customer'], function (){
        Route::post('/list', 'BOCustomerController@index');
        Route::post('/show/{id}', 'BOCustomerController@show')->where(['id' => '[0-9]+']);
        Route::post('/form/{id?}', 'BOCustomerController@submitForm')->where(['id' => '[0-9]+']);
        Route::post('/form/customer-staff', 'BOCustomerController@updateCustomerMapping');
    });
    /**
     * Customer Diary Group
     */
    Route::group(['prefix' => 'customer-diary'], function() {
        Route::post('/list', 'BOCustomerDiaryController@index');
        Route::post('/form/{id?}', 'BOCustomerDiaryController@submitForm')->where(['id' => '[0-9]+']);
    });
    /**
     * BOProduct Group
     */
    Route::group(['prefix'=>'product'], function (){
        Route::post('/list', 'BOProductController@index');
        Route::post('/show/{id}', 'BOProductController@show')->where(['id' => '[0-9]+']);
        Route::post('/form/{id?}', 'BOProductController@submitForm')->where(['id' => '[0-9]+']);
        Route::post('/apartments-by-building', 'BOProductController@getApartmentsByBuilding');
        Route::post('/show-by-code', 'BOProductController@showByCode');
        Route::post('/projects-by-user-role', 'BOProductController@getProjectsByUserRole');
    });
    /**
     * BOBill Group
     */
    Route::group(['prefix'=>'bill'], function (){
        Route::post('/list', 'BOBillController@index');
        Route::post('/list-by-customer/{customer_id}', 'BOBillController@listByCustomer')->where(['customer_id' => '[0-9]+']);
        Route::post('/show/{id}', 'BOBillController@show')->where(['id' => '[0-9]+']);
        Route::post('/form/{id?}', 'BOBillController@submitForm')->where(['id' => '[0-9]+']);
        Route::post('/update-status/{id}', 'BOBillController@changeStatus')->where(['id' => '[0-9]+']);
        Route::post('/log/{id}', 'BOBillController@getLog')->where(['id' => '[0-9]+']);
        Route::post('/init-payment/{id}', 'BOBillController@InitPayment')->where(['id' => '[0-9]+']);
        Route::get('/report', 'BOBillController@getReport');
    });
    /**
     * BOPayment Group
     */
    Route::group(['prefix'=>'payment'], function (){
        Route::post('/list/{bid?}', 'BOPaymentController@index')->where(['bid' => '[0-9]+']);
        Route::post('/show/{id}', 'BOPaymentController@show')->where(['id' => '[0-9]+']);
        Route::post('/form', 'BOPaymentController@submitForm');
        Route::post('/payment-methods', 'BOPaymentController@getListPaymentMethod');
        Route::post('/check/{id}', 'BOPaymentController@check')->where(['id' => '[0-9]+']);
        Route::post('/list-by-bill/{bill_id}', 'BOPaymentController@getPaymentsByBill')->where(['bill_id' => '[0-9]+']);
    });
    /**
     * BOCategory Group
     */
    Route::group(['prefix'=>'category'], function (){
        Route::post('/list', 'BOCategoryController@index');
        Route::post('/show/{id}', 'BOCategoryController@show')->where(['id' => '[0-9]+']);
        Route::post('/form/{id?}', 'BOCategoryController@submitForm');
        Route::post('/buildings-by-project', 'BOCategoryController@getBuildingsByProjectCode');
    });

    /**
     * Notification Group
     */
    Route::group(['prefix' => 'notification'], function () {
        Route::post('/list', 'NotificationController@index');
        Route::post('/show/{notification}', 'NotificationController@show');
        Route::post('/remind-approval', 'NotificationController@remindApproval');
    });

    /**
     * Post Group
     */
    Route::group(['prefix' => 'post'], function () {
        Route::post('/list', 'PostController@index');
        Route::post('/show/{id}', 'PostController@show')->where('id', '[a-zA-Z0-9]+');
        Route::post('/comments/{id}', 'PostController@listComments')->where('id', '[0-9]+');
        Route::post('/send-comment/{id}', 'PostController@sendComment')->where('id', '[0-9]+');
        Route::post('/mass-assign', 'PostController@massAssign');
        Route::post('/tags', 'PostController@listTags');
    });

    /**
     * Forum Group
     */
    Route::group(['prefix' => 'forum'], function () {
        Route::post('/news-feed', 'ForumController@index');
        Route::post('/post-status', 'ForumController@create');
        Route::post('/post-status/{id}', 'ForumController@update')->where('id', '[a-zA-Z0-9]+');
        Route::post('/remove-status/{id}', 'ForumController@remove')->where('id', '[a-zA-Z0-9]+');
        Route::post('/react/{id}', 'ForumController@react')->where('id', '[a-zA-Z0-9]+');
        Route::post('/send-comment/{id}', 'ForumController@sendFeedback')->where('id', '[a-zA-Z0-9]+');
        Route::post('/comments/{post}', 'ForumController@listComment')->where('post', '[a-zA-Z0-9]+');
    });

    /**
     * Password Reset Group
     */
    Route::group(['prefix' => 'password'], function () {
        Route::post('/create', 'PasswordResetController@create');
        Route::get('/find/{token}', 'PasswordResetController@find')->middleware('web');
        Route::post('/reset', 'PasswordResetController@reset')->middleware('web')->name('api.password.request');
    });

    /**
     * Event Group
     */
    Route::group(['prefix' => 'event'], function () {
        Route::post('/check-in', 'EventCheckinController@checkIn');
        Route::post('/participants/{id}', 'EventCheckinController@getParticipants')->where(['id', '[0-9]+']);
        Route::post('/winner-congrats/{id}', 'EventCheckinController@sendCongrats')->where(['id', '[0-9]+']);
    });

    /**
     * Report Group
     */
    Route::group(['prefix' => 'report'], function () {
        Route::post('/bill', 'ReportController@reportBill');
    });

    //*** Customer API
    Route::group(['prefix' => 'customer', 'namespace' => 'Customer'], function () {
        Route::post('/login', 'AuthController@login');
        /**
         * Authenticated Customer Area
         */
        Route::group(['middleware' => 'auth:api-customer'], function () {
            Route::post('/check-login', 'AuthController@checkLogin');
            Route::post('/update-fcm', 'AuthController@updateFCMToken');
            Route::post('/notifications', 'AuthController@notifications');
            Route::post('/change-password', 'AuthController@changePassword');
            /** todo: Customer handle bills */
            Route::post('/bills', 'BOBillController@index');
            Route::post('/bill/{id}', 'BOBillController@show')->where(['id' => '[0-9]+']);
            Route::post('/staff-lookup', 'BOUserController@StaffLookup');
            Route::post('/bill/update-status/{id}', 'BOBillController@updateStatus')->where(['id' => '[0-9]+']);
            Route::post('/bill/log/{id}', 'BOBillController@getLog')->where(['id' => '[0-9]+']);
            Route::post('/bill/pay', 'BOBillController@makePayment');
            Route::post('/payment/list/{bill_id}', 'BOPaymentController@getPaymentsByBill');
            Route::post('/bill/init-payment/{id}', 'BOBillController@InitPayment')->where(['id' => '[0-9]+']);
            /** todo: Customer Notice */
            Route::post('/notices', 'NoticeController@index');
            Route::post('/notice/{id}', 'NoticeController@show');
        });
    });
});
