const Custom = {
    generateProgressBar: function (percentage, label='', type='info') {
        return `
        <div class="progress">
          <div class="progress-bar progress-bar-${type}" role="progressbar" aria-valuenow="${percentage}"
          aria-valuemin="0" aria-valuemax="100" style="width:${percentage}%">
            ${label}
          </div>
        </div>
        `;
    },
    generateMediaListItem: function (avatar, title, subtitle = '', href = '#') {
        return `
        <li class="media">
            <div class="media-left">
              <a href="${href}" target="_blank">
                <img style="max-height: 45px;" class="media-object" src="${avatar}" alt="${title}">
              </a>
            </div>
            <div class="media-body">
              <h6 class="media-heading"><b>${title}</b></h6>
              <p style="font-size:12px;">${subtitle}</p>
            </div>
        </li>
        `;
    }
};

$(function () {
    const BODY = $('body');
    if ($(".fancybox").length)  $(".fancybox").fancybox();
    $.fancyConfirm = function(opts) {
        opts = $.extend(
            true,
            {
                title: "Are you sure?",
                message: "",
                okButton: "OK",
                noButton: "Cancel",
                callback: $.noop
            },
            opts || {}
        );

        $.fancybox.open({
            type: "html",
            src:
                '<div class="fc-content p-5 rounded">' +
                '<h2 class="mb-3">' + opts.title + "</h2>" +
                "<p>" + opts.message + "</p>" +
                '<div class="text-right" style="margin-top:20px;">' +
                '<button data-value="0" data-fancybox-close class="btn btn-light">' +
                opts.noButton +
                "</button>" +
                '<button data-value="1" data-fancybox-close class="btn btn-danger">' +
                opts.okButton +
                "</button>" +
                "</div>" +
                "</div>",
            opts: {
                animationDuration: 350,
                animationEffect: "material",
                modal: true,
                baseTpl:
                    '<div class="fancybox-container fc-container" role="dialog" tabindex="-1">' +
                    '<div class="fancybox-bg"></div>' +
                    '<div class="fancybox-inner">' +
                    '<div class="fancybox-stage"></div>' +
                    "</div>" +
                    "</div>",
                afterClose: function(instance, current, e) {
                    let button = e ? e.target || e.currentTarget : null;
                    let value = button ? $(button).data("value") : 0;
                    opts.callback(value);
                }
            }
        });
    };

    //** todo: Clone hidden input button
    $('[data-role="clone-input"]').on('click', function (e) {
       e.preventDefault();
       const target = $(this).data('target');
       let __new = $(target).clone().attr('type', 'text').removeAttr('disabled').removeAttr('id');
        $(target).parent().append(__new).fadeIn();
    });
    // ** todo: Clone hidden row div
    $('[data-role="clone-row"]').on('click', function (e) {
        e.preventDefault();
        const target = $(this).data('target');
        let __new = $(target).clone().removeAttr('id').removeClass('hidden');
        __new.find('input').removeAttr('disabled');
        $(target).parent().append(__new).fadeIn();
    });
    // ** todo: Remove row div
    BODY.on('click', '[data-role="remove-row"]', function (e) {
        e.preventDefault();
        $(this).parent().remove();
    });

    const ProductSelect2 = $('#productSelect2');
    if (ProductSelect2.length) {
        ProductSelect2.select2({
            placeholder: "Nhập mã căn hộ...",
            minimumInputLength: 3,
            delay: 250,
            ajax: {
                url: '/api/ajax/list-product',
                data: (params) => {
                    // console.log('Params', params);
                    return {
                        page: params.page || 1,
                        size: 15,
                        keyword: params.term
                    }
                },
                processResults: (response, params) => {
                    params.page = params.page || 1;
                    if (response && response.success) {
                        // console.log('Processed data');
                        return {
                            results: response.data,
                            pagination: {
                                more: response.data.length>=15
                            }
                        };
                    } else {
                        return {results: [], pagination: false};
                    }
                },
                error: (error) => {
                    console.log(error);
                }
            },
        });
    }

    const UserSelect2 = $('#userSelect2');
    if (UserSelect2.length) {
        UserSelect2.select2({
            placeholder: "Người tạo (tên / tài khoản)",
            minimumInputLength: 3,
            delay: 250,
            ajax: {
                url: '/api/ajax/list-user',
                data: (params) => {
                    // console.log('Params', params);
                    return {
                        page: params.page || 1,
                        size: 15,
                        keyword: params.term
                    }
                },
                processResults: (response, params) => {
                    params.page = params.page || 1;
                    if (response && response.success) {
                        // console.log('Processed data', response.data);
                        let data = response.data;
                        data.forEach((item, key) => {
                            item.text = item.name + ' - ' + item.account;
                            data[key] = item;
                        });
                        return {
                            results: data,
                            pagination: {
                                more: data.length>=15
                            }
                        };
                    } else {
                        return {results: [], pagination: false};
                    }
                },
                error: (error) => {
                    console.log(error);
                }
            },
        });
    }
});
