<?php

use Illuminate\Database\Seeder;

class UserGroupAll extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $query = new \App\BOUserGroup();
        $query->{\App\BOUserGroup::ID_KEY} = null;
        $query->gb_status = 1;
        $query->gb_title = 'Tất cả người dùng';
        $query->gb_code = 'ALL';
        $query->reference_code = 'ALL';
        $query->level_id = 0;
        $query->gb_created_time = null;
        $query->gb_updated_time = null;
        $query->gb_updated_user = 0;
        $query->gb_created_user = 0;
        $query->save();
    }
}
