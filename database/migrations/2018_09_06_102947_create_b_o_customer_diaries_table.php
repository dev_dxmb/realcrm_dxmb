<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOCustomerDiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_customer_diaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cd_id')->unique();
            $table->tinyInteger('cd_status')->default(0);
            $table->integer('cd_user_id')->comment('Nhân viên');
            $table->integer('cd_customer_id')->comment('Khách hàng');
            $table->text('cd_description')->nullable()->comment('Nội dung chăm sóc');
            $table->tinyInteger('cd_rating')->nullable()->comment('Điểm nhân viên đánh giá KH tiềm năng, thang điểm 5');
            $table->timestamp('cd_time')->nullable()->comment('Thời gian thực hiện');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_customer_diaries');
    }
}
