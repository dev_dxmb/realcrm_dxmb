<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bo_content')->comment('Nội dung log');
            $table->integer('bo_object')->nullable()->comment('ID Đối tượng log');
            $table->integer('bo_user')->nullable()->comment('ID Người thực hiện');
            $table->tinyInteger('bo_object_type')->default(0)->comment('Loại đối tượng: 0 - default, 1 - Product, 2 - Category, 3 - Transaction, 4 - Bill, 5- Customer, 6 - User');
            $table->tinyInteger('bo_type')->default(0)->comment('Loại log: 0 - info, 1 - warning, 2 - error');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
