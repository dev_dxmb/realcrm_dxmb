<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditBOTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_transactions', function (Blueprint $table) {
            $table->dropColumn("status_id");
            $table->dropColumn("customer_id");
            $table->dropColumn("trans_money");
            $table->dropColumn("trans_log_edit");
            $table->integer("product_id")->after("product_ids")->comment('Mã 1 sản phảm GD')->nullable();
            $table->json("trans_status_log")->comment('Log các thay đổi của trạng thái GD')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_transactions', function (Blueprint $table) {
            $table->integer("status_id")->nullable();
            $table->integer("customer_id")->nullable();
            $table->integer("trans_money")->nullable();
            $table->json("trans_log_edit")->nullable();
            $table->dropColumn("product_id");
            $table->dropColumn("trans_status_log");
        });
    }
}
