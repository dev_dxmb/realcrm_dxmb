<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBOBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_o_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_id')->unique();
            $table->integer('bill_status')->nullable();
            $table->string('bill_code')->nullable();
            $table->string('bill_title')->nullable();
            $table->string('bill_reference_code')->nullable();
            $table->text('bill_note')->nullable();
            $table->json('bill_product_ids')->nullable()->comment('sản phẩm');
            $table->integer('staff_id')->nullable()->comment('nhân viên');
            $table->integer('customer_id')->comment('khách hàng');
            $table->integer('status_id')->nullable();
            $table->string('sale_program_id')->nullable()->comment('chương trình bán hàng trên crm');
            $table->double('bill_total_money')->comment('tổng tiền');
            $table->timestamp('bill_customer_verified_time')->nullable()->comment('thời gian khách hàng verify');
            $table->integer('staff_verified_staff_id')->nullable();
            $table->integer('product_verified_staff_id')->nullable();
            $table->timestamp('bill_created_time')->nullable();
            $table->timestamp('bill_staff_verified_time')->nullable();
            $table->timestamp('bill_completed_time')->nullable();
            $table->text('trans_log_edit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_o_bills');
    }
}
