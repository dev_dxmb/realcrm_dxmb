<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->comment('ID bài post');
            $table->json('department_ids')->comment('ID các phòng ban nhận tin');
            $table->json('read_by')->nullable()->comment('ID người dùng đã đọc tin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_targets');
    }
}
