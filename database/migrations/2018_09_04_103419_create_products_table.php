<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unique();
            $table->integer('product_status')->default(0);
            $table->string('product_code')->nullable();
            $table->string('product_title')->comment('tiêu đề');
            $table->text('product_short_description')->nullable()->comment('mô tả ngắn');
            $table->text('product_ description')->nullable()->comment('mô tả');
            $table->string('product_image_thumb')->nullable()->comment('ảnh đại diện');
            $table->text('product_media')->nullable()->comment('file');
            $table->integer('category_id')->nullable()->comment('id danh mục tương ứng');
            $table->integer('owner_id')->nullable()->comment('chủ nhà');
            $table->json('tag_ids')->nullable()->comment('hashtag');
            $table->string('product_addr')->nullable()->comment('địa chỉ');
            $table->string('product_location_longlat')->nullable()->comment('vị trí tọa độ');
            $table->integer('location_id')->nullable();
            $table->json('product_reference_codes')->nullable()->comment('code nối đến hệ thông khác');
            $table->double('product_import_price')->nullable()->comment('giá nhập');
            $table->double('product_sale_price')->nullable()->comment('giá bán');
            $table->json('staff_ids')->nullable()->comment('danh sách nhân viên phụ trách');
            $table->json('customer_ids')->nullable()->comment('danh sách khách hàng');
            $table->json('permission_group_ids')->nullable();
            $table->json('permission_staff_ids')->nullable();
            $table->integer('created_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->timestamp('product_created_time')->nullable();
            $table->timestamp('product_updated_time')->nullable();
            $table->text('product_log_edit')->nullable();
            $table->text('product_log_prices')->nullable();
            $table->text('product_log_customers')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
