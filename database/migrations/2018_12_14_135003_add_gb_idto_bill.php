<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGbIdtoBill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_o_bills', function (Blueprint $table) {
            $table->integer('gb_id')->nullable()->comment('Phòng ban người tạo->báo cáo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_o_bills', function (Blueprint $table) {
            $table->dropColumn('gb_id');
        });
    }
}
