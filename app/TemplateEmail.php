<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateEmail extends Model
{
    protected $table = 'template_email';

    const TYPE = [
        '1' => 'Đặt mua',
        '2' => 'Đặt chỗ thưởng phạt',
        '3' => 'Đặt chỗ không thưởng phạt',
        '4' => 'Chuyển chỗ sang cọc',
    ];

    public static function getAll() {
        $aryResult = TemplateEmail::get();
        return $aryResult;
    }
}
