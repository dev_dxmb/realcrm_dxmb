<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Util;
class CustomerNotice extends Model
{
    public $timestamps = false;
    const ID_KEY = 'cn_id';
    protected $table = 'customer_notice';

    protected $casts = [
        'array_images' => 'array',
        'updated_time' => 'datetime:H:i d-m-Y'
    ];

    /**
     * @param $value
     * @return null|string|string[]
     */
    public function getContentAttribute($value) {
        //** todo: Regex Notice content for HTML webview */
        if (!$value) return $value;
        $value = preg_replace('/(height|width)[=\\?\'?"?0-9?px?]+/', '', $value);
        $value = preg_replace('/(height|width):[0-9]+px?/', '', $value);
        return $value;
    }

    /**
     * @Auth: Dienct
     * @Des: Add/edit project
     * @Since: 02/03/2017
     */
    public function AddEditCustomerNotice($id) {
        $a_DataUpdate = array();
        $a_DataUpdate['cn_title'] = Input::get('cn_title');
        $a_DataUpdate['cn_status'] = Input::get('cn_status') == 'on' ? 1 : 0;
        $a_DataUpdate['cn_note'] = Input::get('cn_note');
        $a_DataUpdate['cn_shot_detail'] = Input::get('cn_shot_detail');
        
        $a_images = Input::get('images');
        $a_filename = Input::get('filename');
        $a_deleteUrl = Input::get('deleteUrl');
        if(is_array($a_images) && count($a_images) > 0){
            foreach ($a_images as $key => $val){
                $aryIMG['img'] = $val;
                $aryIMG['filename'] = $a_filename[$key];
                $aryIMG['deleteUrl'] = $a_deleteUrl[$key];
                $aryReturn[] = $aryIMG;
            }
        }
        
        if(isset($aryReturn)){
            $a_DataUpdate['images'] = json_encode($aryReturn);
        }

        if (is_numeric($id) == true && $id != 0) {
            $a_DataUpdate['updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('customer_notice')->where('id', $id)->update($a_DataUpdate);
        } else {
            $a_DataUpdate['cn_id'] = time();
            $a_DataUpdate['created_time'] = date('Y-m-d H:i:s', time());
            $a_DataUpdate['updated_time'] = date('Y-m-d H:i:s', time());
            DB::table('customer_notice')->insert($a_DataUpdate);
        }
    }

    /**
     * @Auth: Dienct
     * @Des: 
     * @Since: 2/3/2017
     */
    public function getCustomerNoticeById($id) {

        $a_Data = DB::table('customer_notice')->where('id', $id)->first();        
        $a_Data->images  = json_decode($a_Data->images);
        return $a_Data;
    }

    /**
     * @Auth:Dienct
     * @Des: get all record news table
     * @Since: 16/08/2018
     */
    public function getAllSearch() {
        $a_data = array();
        $o_Db = DB::table('customer_notice')->select('*');
        $a_data = $o_Db->where('cn_status', '!=', -1);
        $a_search = array();
        //search

        $sz_cn_title = Input::get('cn_title', '');
        if ($sz_cn_title != '') {
            $a_search['cn_title'] = $sz_cn_title;
            $a_data = $o_Db->where('cn_title', 'like', '%' . $sz_cn_title . '%');
        }
        
        $a_data = $o_Db->orderBy('updated_time', 'desc')->paginate(20);

        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        $a_return = array('a_data' => $a_data, 'a_search' => $a_search);
        return $a_return;
    }
    /*
     * @auth: Dienct
     * @Des: get all Payment Method
     * @Since: 06/09/2018
     * 
     * **/
   public static function getAllCustomerNotice(){
       $o_Db = DB::table('customer_notice')->select('*')->where('cn_status', '!=', -1)->get();
       return $o_Db;
   }
}
