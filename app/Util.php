<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Util extends Model
{
    const AVATAR_RANDOM_BG = [
        '000066', '003399', '003366', '006666', '006600',
        '336600', '660066', '669900', '3399ff', 'cc3300', '663300'
    ];

    /** Calculate time difference from now
     * @param $timestamp
     * @return string
     */
    public static function timeAgo($timestamp) {
        $time_ago        = strtotime($timestamp);
        $current_time    = time();
        $time_difference = $current_time - $time_ago;
        $seconds         = $time_difference;

        $minutes = round($seconds / 60); // value 60 is seconds
        $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec
        $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;
        $weeks   = round($seconds / 604800); // 7*24*60*60;
        $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60
        $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60

        if ($seconds <= 60){
            return "Vừa xong";
        } else if ($minutes <= 60){
            return "$minutes phút trước";
        } else if ($hours <= 24){
            return "$hours giờ trước";
        } else if ($days <= 7){
            if ($days == 1){
                return "Hôm qua";
            } else {
                return "$days ngày trước";
            }
        } else if ($weeks <= 4.3){
            if ($weeks == 1){
                return "Tuần trước";
            } else {
                return "$weeks tuần trước";
            }
        } else if ($months <= 12){
            return "$months tháng trước";
        } else {
            return "$years năm trước";
        }
    }

    /**
     * @return int
     */
    public static function microtime_float() {
        return (int)(microtime(true) * 100000);
    }

    /**
     * @param string $name
     * @param int $random_key
     * @param bool $rounded
     * @return string
     */
    public static function generateAvatarByNameInitials($name='BO', $random_key = 10, $rounded = false) {
        $name = str_replace(' ', '+', $name);
        return 'https://ui-avatars.com/api/?name='.$name.'&size=128&rounded='.$rounded.'&color=fff&background='.self::AVATAR_RANDOM_BG[$random_key];
    }

    /**
     * @param string $value
     * @return string
     */
    public static function escapeHTMLforWebView($value = '') {
//        $value = preg_replace('/(table|thead|tbody)/', 'div', $value);
//        $value = preg_replace('/(<tr)/', '<div', $value);
//        $value = preg_replace('/(tr>)/', 'div>', $value);
//        $value = preg_replace('/(<td)/', '<p', $value);
//        $value = preg_replace('/(td>)/', 'p>', $value);
//        $value = preg_replace('/(<th)/', '<p><strong', $value);
//        $value = preg_replace('/(th>)/', 'strong></p>', $value);
        $value = preg_replace('/(<table)(.)+(<\/table>)/', '', $value);
        $value = preg_replace('/text-align[:\\? ?\'?"?]+start["?;?]/', '', $value);
        $value = preg_replace('/(font-family|line-height):[\w\s?\'?,?-?]+/', '', $value);
        $value = preg_replace('/(height|width)[=\?\'?"?0-9?]+/', '', $value);
        $value = preg_replace('/border-style:[\w\s]+;/', '', $value);
        return htmlspecialchars($value);
    }

    /**
     * @param string $code
     * @return string
     */
    public static function convertApartmentCode($code='') {
        if (!$code || strlen($code)!==12) return 'Không hợp lệ';
//        $str = '6EL0D112A009';
        $arr = [];
        $i=0;
        while(strlen($code)>=3){
            $temp = substr($code,0,3);
            $arr[] = ltrim($temp,'0');
            if($i==3 && strlen($arr[3]) < 2) {
                $arr[3] = '0'.$arr[3];
            }
            $code = substr($code,3);
            $i++;
        }
        if(count($arr)>3){
            return ($arr[1]!=''? $arr[1] : $arr[0]) . '-' . $arr[2] . $arr[3];
        }else{
            return 'Không hợp lệ';
        }
    }

    /**
     * @param string $str
     * @return string
     */
    public static function stripVN($str='') {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);

        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        return $str;
    }

    /**
     * @param string $phone
     * @return string
     */
    public static function formatPhoneVN($phone = '') {
        $phone = str_replace(' ', '', $phone);
        $phone = preg_replace('/[\x00-\x1F\x7f-\xFF]/', '', $phone);
        if (starts_with($phone, '0')) {
            $phone = '84' . substr($phone, 1);
        }
        return $phone;
    }


    /**
     * @param $element
     * @param array $array
     * @return array
     */
    public static function unsetArrayValue($element, Array $array) {
        $index = array_search($element, $array);
        if($index !== false){
            unset($array[$index]);
        }
        return array_values($array);
    }

    public static function b_fCheckArray($the_a_Array)
    {
        return isset($the_a_Array) && is_array($the_a_Array) && $the_a_Array;
    }

    
    public static function b_fCheckObject($the_o_Object)
    {
        return isset($the_o_Object) && is_object($the_o_Object) && $the_o_Object;
    }

    
    public static function sz_fCurrentDateTime($the_sz_Format = 'Y-m-d H:i:s')
    {
        return date($the_sz_Format, time());
    }

    
    public static function a_fMultiToSingleArray($the_a_MultiArray, $the_sz_Key = 'id', $the_sz_Value = 'name', $the_b_SortByValue = false, $the_b_SortDesc = false) {
        $a_SingleArray = array();
        foreach ($the_a_MultiArray as $a_Single) {
            $a_SingleArray[$a_Single[$the_sz_Key]] = $a_Single[$the_sz_Value];
        }
        return $a_SingleArray;
    }
    
    
    public static function i_fNumberOfDays($i_FromTime,$i_ToTime,$sz_day)
    {
        $i_FromTime = strtotime(date('Y-m-d 00:00:00',$i_FromTime)) ;
        $i_ToTime = strtotime(date('Y-m-d 00:00:00',$i_ToTime)) ;
        $dt = Array ();
        for($i=$i_FromTime; $i<=$i_ToTime;$i=$i+86400) {
                if(date("l",$i) == $sz_day) {
                        $dt[] = date("l Y-m-d ", $i);
                }
        }
        return count($dt);        
    }
    
    /**

     * @Auth: Dienct
     * @Des: Format date time
     * @Since: 8/1/2016     
     */
    public static function sz_DateTimeFormat($date_time){
        $date = date_create($date_time);
        return date_format($date,"m/d/Y H:i:s");
    }
    /**

     * @Auth: Dienct
     * @Des: Format date time
     * @Since: 8/1/2016     
     */
    public static function sz_DateTimeGetAPI($date_time){
        $date = date_create($date_time);
        return date_format($date,"Y-m-d H:i");
    }
    public static function sz_DateTimeGetAPI2222($date_time){
        $date = date_create($date_time);
        return date_format($date,"Y-m-d H:i:s");
    }
    
    public static function sz_DateFinishFormat($date_time){
        $date = date_create($date_time);
        return date_format($date,"m/d/Y");
    }
}
