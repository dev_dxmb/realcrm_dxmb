<?php

namespace App;

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\BOBillController;
use App\Http\Controllers\API\BOProductController;
use Illuminate\Database\Eloquent\Model;

class BOBill extends Model
{
    public $timestamps = true;
    const CREATED_AT = 'bill_created_time';
    const UPDATED_AT = 'bill_updated_time';

    protected $fillable = ['bill_created_time', 'bill_updated_time'];
    protected $dates = ['bill_created_time', 'bill_updated_time'];

    const ID_KEY = "bill_id";
    const OBJECT_KEY = "bill";

    const STATUSES = [
        'STATUS_CANCEL_APPROVED' => -11, // TKKD duyệt huỷ HĐ > chờ DVKH duyệt
        'STATUS_CANCEL_REQUEST' => -10, // NVKD gửi yc huỷ HĐ
        'STATUS_BOOK_TO_DEPOSIT' => -5, // NVKD huỷ HĐ chỗ - Tạo HĐ cọc mới
        'STATUS_CANCELED' => -4, // Huỷ hợp đồng hoàn toàn
        'STATUS_REQUEST' => 0, // Gửi yêu cầu giao dịch => YCDCO =>TVC
        'STATUS_APPROVED' => 1, // Thư ký duyệt yêu cầu => CDDCO => TVC
        'STATUS_DISAPPROVED' => -1, // TKKD huỷ yêu cầu => Status giao dịch C
        'STATUS_APPROVED_BY_CUSTOMER' => 2, // Khách hàng xác nhận giao dịch
        'STATUS_DISAPPROVED_BY_CUSTOMER' => -2, // Khách hàng HỦY GIAO DỊCH => Status giao dịch C
        'STATUS_APPROVED_BY_MANAGER' => 3, // Quản lý KD duyệt
        'STATUS_DISAPPROVED_BY_MANAGER' => -3, // Quản lý KD từ chối
        'STATUS_PAID_ONCE' => 4, // NVKD tạo yc thanh toán lần đầu >> TKKD tạo giao dịch lên TVC
        'STATUS_PAYING' => 5, // Khách hàng đang thanh toán
        'STATUS_PAID' => 6, // Khách hàng Thanh toán đủ
        'STATUS_BOOKED' => 7, // Đặt chỗ thành công => DCO => tvc
        'BOOK_TO_DEPOSIT_REQUEST' => 8, // NVKD Yêu cầu chuyển chỗ sang cọc
        'STATUS_DEPOSITED' => 9, // Đặt cọc thành công => DCO => tvc
        'STATUS_FINALIZED' => 10, // Y/C TKKD Xác nhận Hoàn thành
        'STATUS_SUCCESS' => 11, // Đã Xác nhận Hoàn thành
    ];

    const STATUS_COLORS = [
        '-11' => 'darkred',
        '-10' => 'red',
        '-5' => '#7c7b7b',
        '-4' => '#724347',
        "0" => "#b2a40c",
        "1" => "#b2a40c",
        "-1" => "#1974d6",
        "2" => "#1974d6",
        "-2" => "#724347",
        "3" => "#4D1217",
        '-3' => '#724347',
        "4" => "#4D1217",
        "5" => "#4D1217",
        "6" => "#0a870a",
        "7" => "#0a870a",
        "8" => "#0a870a",
        "9" => "#0a870a",
        "10" => "#79bf16",
        "11" => "#79bf16",
    ];

    const STATUS_TEXTS = [
        "-11" => "TKKD Y/C Huỷ",
        "-10" => "NVKD Y/C Huỷ",
        "-5" => "Chuyển cọc",
        "-4" => "NVKD huỷ",
        "-3" => "QLKD huỷ",
        "0" => "Chờ duyệt",
        "1" => "TK duyệt",
        "-1" => "TK huỷ",
        "2" => "KH duyệt",
        "-2" => "KH huỷ",
        "3" => "QLKD duyệt",
        "4" => "TT đợt 1",
        "5" => "Đang TT",
        "6" => "TT xong",
        "7" => "Đặt chỗ",
        "8" => "Chờ C.Cọc",
        "9" => "Đặt cọc",
        "10" => "XN H.Thành",
        "11" => "Ký HĐMB",
    ];

    const STATUS_TEXTS_WEB = [
        "-11" => "TKKD Yêu cầu Huỷ",
        "-10" => "NVKD Yêu cầu Huỷ",
        "-5" => "Chuyển cọc",
        "-4" => "Nhân viên huỷ",
        "-3" => "Quản lý KD huỷ",
        "0" => "Chờ duyệt",
        "1" => "TKKD duyệt",
        "-1" => "TKKD huỷ",
        "2" => "KH đã xác nhận",
        "-2" => "KH huỷ",
        "3" => "Quản lý KD duyệt",
        "4" => "TT đợt 1",
        "5" => "Đang thanh toán",
        "6" => "Đã thanh toán xong",
        "7" => "Đặt chỗ",
        "8" => "Y/C chuyển cọc",
        "9" => "Đặt cọc",
        "10" => "YCXN Hoàn thành",
        "11" => "Ký HĐMB",
    ];
    const STATUS_TEXTS_WEB_COLOR = [
        "-11" => "<span class='alert alert-danger'>TKKD Yêu cầu Huỷ </span>",
        "-10" => "<span class='alert alert-danger'>NVKD Yêu cầu Huỷ </span>",
        "-5" => "<span class='alert alert-info'>Chuyển cọc </span>",
        "-4" => "<span class='alert alert-danger'>Nhân viên huỷ </span>",
        "-3" => "<span class='alert alert-danger'>Quản lý KD huỷ </span>",
        "0" => "<span class='alert alert-warning'>Chờ duyệt </span>",
        "1" => "<span class='alert alert-info'>TKKD duyệt </span>",
        "-1" => "<span class='alert alert-danger'>TKKD huỷ </span>",
        "2" => "<span class='alert alert-success'>KH đã xác nhận </span>",
        "-2" => "<span class='alert alert-danger'>KH huỷ </span>",
        "3" => "<span class='alert alert-success'>Quản lý KD duyệt </span>",
        "4" => "<span class='alert alert-info'>TT đợt 1 </span>",
        "5" => "<span class='alert alert-warning'>Đang thanh toán </span>",
        "6" => "<span class='alert alert-info'>Đã thanh toán xong </span>",
        "7" => "<span class='alert alert-success'>Đặt chỗ </span>",
        "8" => "<span class='alert alert-warning'>Y/C chuyển cọc </span>",
        "9" => "<span class='alert alert-success'>Đặt cọc </span>",
        "10" => "<span class='alert alert-success'>YCXN Hoàn thành </span>",
        "11" => "<span class='alert alert-success'>Ký HĐMB </span>",
    ];

    const TYPE_DEPOSIT = 1; //Đặt cọc
    const TYPE_BOOK_TO_DEPOSIT = 2; // Đặt chỗ, có thể lên cọc
    const TYPE_BOOK_ONLY = 3; // Đặt chỗ, phải Huỷ chỗ để sang Cọc mới.
    const TYPE_TEXTS = [
      1 => 'Đặt cọc',
      2 => 'Đặt chỗ',
      3 => 'Đặt chỗ',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'bill_product_ids' => 'array',
        'bill_approval_log' => 'array',
        'approval_log' => 'array',
        'edit_log' => 'array',
        'bill_log_edit' => 'array',
        'media' => 'array',
        'bill_customer_info' => 'array',
        'bill_created_time' => 'datetime:H:i d/m/Y',
        'bill_updated_time' => 'datetime:H:i d/m/Y',
    ];

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value)
    {
        $id = time();
        switch ($key) {
            case "bill_product_ids":
            case "bill_approval_log":
            case "trans_status_log":
            case "media":
            case "bill_customer_info":
            case "bill_log_edit":
                $this->attributes[$key] = is_array($value)? json_encode($value) : $value;
                break;
            case self::ID_KEY:
                $this->attributes[$key] = $value?? $id;
                break;
            case "bill_created_time":
            case "bill_updated_time":
                $this->attributes[$key] = $value?? now();
                break;
            case "bill_code":
                $this->attributes[$key] = $value?? 'OBB'.$id;
                break;
            case "bill_title":
                $this->attributes[$key] = $value?? 'Hợp đồng ' . $id;
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }

    /**
     * @param $value
     * @return int|string
     */
    public function getTotalMoneyAttribute($value)
    {
        return $value? number_format($value, 0) : 0;
    }

    /**
     * @param $value
     * @return int|string
     */
    public function getRequiredMoneyAttribute($value) {
        return $value? number_format($value, 0) : 0;
    }

    /**
     * @param $value
     * @return null | object
     */
    public function getBillCustomerInfoAttribute($value) {
        if (!$value) return null;
        $value = is_object($value)? $value : json_decode($value);
        $images = $value->images;
        if (is_array($images)) {
            foreach ($images as &$image) {
                $image = url($image);
            }
        }
        $value->images = $images;
        return $value;
    }

    /**
     * @param $value
     * @return mixed|null
     */
    public function getMediaAttribute($value) {
        if (!$value) return null;
        $images = is_array($value)? $value : json_decode($value);
        foreach ($images as &$image) {
            $image = url($image);
        }
        return $images;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff() {
        return $this->belongsTo(BOUser::class, 'staff_id', BOUser::ID_KEY);
    }
    /** Required bo_user id for identification
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer() {
        return $this->belongsTo(UserCustomerMapping::class, 'customer_id', 'id_customer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customers() {
        return $this->hasMany(UserCustomerMapping::class, 'id_customer', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function official_customer() {
        return $this->belongsTo(BOCustomer::class, 'customer_id', BOCustomer::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product() {
        return $this->belongsTo(BOProduct::class, 'product_id', BOProduct::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction() {
        return $this->belongsTo(BOTransaction::class, 'transaction_id', BOTransaction::ID_KEY);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments() {
        return $this->hasMany(BOPayment::class, 'bill_code', 'bill_code');
    }

    /**
     * @param $type
     * @param BOProduct $apartment
     * @return string
     */
    public static function generateBillCode($type, BOProduct $apartment) {
        $count = self::where('bill_created_time', '>=', date('Y-m-d').' 00:00:00')->count();
        $count = $count? $count+1 : 1;
        if ($count<10) {
            $count = '00' . $count;
        } elseif ($count<100) {
            $count = '0' . $count;
        }
        $prefix = 'XNDM';
        if ($type !== self::TYPE_DEPOSIT) {
            $prefix = $apartment->book_type === BOProduct::BOOK_WITH_PENALTY? 'XNTP' : 'XNDC';
        }
        return $prefix . date('ymd') . $count;
    }

    /**
     * @param $status_code
     * @param int $uid
     * @param bool $api_gate
     * @return array
     */
    public static function generateApprovalLog($status_code, $uid = 0, $api_gate = true) {
        if (!in_array($status_code, self::STATUSES)) return [];
        //** Set log note */
        switch ($status_code) {
            case self::STATUSES['STATUS_APPROVED_BY_CUSTOMER']:
                $note = 'Khách hàng xác nhận giao dịch';
                break;
            case self::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER']:
                $note = 'Khách hàng từ chối giao dịch';
                break;
            case self::STATUSES['STATUS_REQUEST'];
                $note = 'Khởi tạo Yêu cầu giao dịch';
                break;
            case self::STATUSES['STATUS_APPROVED']:
                $note = 'Thư ký kinh doanh Duyệt yêu cầu Giao dịch';
                break;
            case self::STATUSES['STATUS_DISAPPROVED']:
                $note = 'Thư ký kinh doanh Từ chối yêu cầu Giao dịch';
                break;
            default:
                $note = self::STATUS_TEXTS_WEB[$status_code]?? '';
        }
        return [
            "VALUE" => $status_code,
            "TIME"  => time(),
            "CREATED_BY" => $uid>0? $uid : AuthController::getCurrentUID($api_gate),
            "NOTE"  => $note
        ];
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getBillLogEditAttribute($value) {
        if ($value) {
            $value = json_decode($value, true);
            foreach ($value as &$log) {
                $log['FROM'] = '';
                $log['TO'] = '';
                $log['OLD'] = $log['OLD']?? null;
                $log['VALUE'] = $log['VALUE']?? null;
                $log['TIME'] = $log['TIME']? date('d-m-Y H:i', (int) $log['TIME']) : '(N.A)';
                switch ($log['FIELD']) {
                    case 'type':
                        $log['LABEL'] = 'Hình thức Chỗ-Cọc';
                        if ($log['OLD']!=null) {
                            $log['FROM'] = $log['OLD']==self::TYPE_DEPOSIT?'Đặt cọc' : ($log['OLD']==self::TYPE_BOOK_ONLY?'Đặt chỗ cố định' : 'Đặt chỗ tạm thời');
                        }
                        if ($log['VALUE']!=null) {
                            $log['TO'] = $log['VALUE']==self::TYPE_DEPOSIT?'Đặt cọc' : ($log['VALUE']==self::TYPE_BOOK_ONLY?'Đặt chỗ cố định' : 'Đặt chỗ tạm thời');
                        }
                        break;
                    default:
                        $log['LABEL'] = '(Không rõ)';
                }
            }
        }
        return $value;
    }
    /**
     * @Auth:HuyNN
     * @Des: get all record transactions table
     * @Since: 05/10/2018
     */
    public static function getAllSearch() {
        $arrProjects = BOProductController::getProjectsByUserRoleWeb('approve');
        $arrApartment = BOProductController::getApartmentsByProject($arrProjects);
        $a_data = BOBill::select('*')->whereNotNull('bill_code')->whereIn('product_id',$arrApartment)->has('customers')->with(
            [
                "staff" => function($staff) {
                    return $staff->select(["ub_id", "ub_account_name AS account", "ub_title AS name", "group_ids"])
                        ->with(["group" => function($group) {
                            return $group->select(['gb_id', 'gb_title AS name']);
                        }]);
                },
                "customers" => function($customers) {
                    return $customers->select(["id_customer","id_user","c_title",  "c_address", "id_passport", "permanent_address", ]);
                },
                "payments" => function($payments) {
                    return $payments->select(["id", "bill_code", "pb_code", "created_at","pb_status"]);
                },
                "product" => function($product)  {
                    return $product->select([
                        "pb_id",
                        "pb_title AS apartment", "pb_code AS code",
                        "category_id",
                        "pb_required_money AS required_money",
                        "stage"
                    ])->with([ "category" => function($category) {
                        return $category->select([
                            "id", "cb_id", "parent_id", "cb_title AS building"
                        ])->where("cb_level", 2)
                            ->with(["sibling" => function($project){
                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])->where("cb_level", 1);
                            }]);
                    }]);
                }
            ]
        )->orderBy('bill_created_time','desc')->get();
         foreach ($a_data as $key => &$bill) {
             $bill->button = BOBillController::_getActionButtonsByRoleWeb(
                $bill->status_id,
                $bill->type,
                $bill->product? $bill->product->{BOProduct::ID_KEY} : null,
                $bill->product? $bill->product->stage : null
            );
         }

        $a_return = array('a_data' => $a_data);
        return $a_return;
    }
}
