<?php

namespace App\Jobs;

use App\BOProduct;
use App\BOTransaction;
use App\Http\Controllers\API\LogController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReleaseApartmentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $apartment;
    protected $transaction;

    /**.
     * ReleaseApartmentJob constructor.
     * @param int $apartment
     * @param int $transaction
     */
    public function __construct($transaction, $apartment = 0)
    {
        $this->apartment = $apartment;
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return mixed
     */
    
    public function handle()
    {
        /** @var $allowed_statuses */
        $allowed_statuses = [
            BOTransaction::STATUS_CODES['LOCKED_AUTO'],
            BOTransaction::STATUS_CODES['LOCKED_MANUAL'],
            BOTransaction::STATUS_CODES['REQUEST_EXTEND'],
            BOTransaction::STATUS_CODES['REQUEST_CANCEL']
        ];
        /** @var $transaction */
        $transaction = BOTransaction::where(['trans_status' => 1, BOTransaction::ID_KEY => $this->transaction])->first();
        if (!$transaction) {
            LogController::logTransaction('Bung lock giao dịch không thành công. Không tìm thấy GD '.$this->transaction, 'error', $this->transaction);
            return false;
        }
        if (!in_array($transaction->trans_code, $allowed_statuses)) return false;
        $transaction->trans_code = BOTransaction::STATUS_CODES['CANCELED_AUTO'];
        $transaction->trans_title = BOTransaction::STATUSES['A-CANCEL'];
        /** @var $logs */
        $logs = $transaction->trans_status_log?? [];
        $logs[] = BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['CANCELED_AUTO'], 1);
        $transaction->trans_status_log = $logs;
        if (!$transaction->save()) LogController::logTransaction('Bung lock GD không thành công!', 'error', $this->transaction);
        /** @var $product_id */
        $product_id = $this->apartment>0? $this->apartment : $transaction->product_id;
        $product = BOProduct::where(BOProduct::ID_KEY, $product_id)->update(['status_id' => BOProduct::STATUSES['MBA']]);
        if (!$product) LogController::logProduct('Bung lock căn hộ không thành công!', 'error', $product_id);
        return true;
    }
}
