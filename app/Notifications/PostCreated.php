<?php

namespace App\Notifications;

use App\Http\Controllers\API\AuthController;
use App\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostCreated extends Notification implements ShouldQueue
{
    use Queueable;
    protected $post;
    protected $api_gate;

    /**
     * PostCreated constructor.
     * @param Post $post
     * @param bool $api_gate
     */
    public function __construct(Post $post, $api_gate=true)
    {
        $this->post = $post;
        $this->api_gate = $api_gate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'post' => $this->post->id,
            'by'   => AuthController::getCurrentUID($this->api_gate),
            'read_by' => []
        ];
    }
}
