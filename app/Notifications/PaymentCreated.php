<?php

namespace App\Notifications;

use App\BOPayment;
use App\Http\Controllers\API\AuthController;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentCreated extends Notification implements ShouldQueue
{
    use Queueable;
    protected $payment;

    /**
     * PaymentCreated constructor.
     * @param BOPayment $payment
     */
    public function __construct(BOPayment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'payment' => $this->payment->{BOPayment::ID_KEY},
            'by'   => AuthController::getCurrentUID(),
        ];
    }
}
