<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTarget extends Model
{
    public $timestamps = true;
    protected $casts = [
        'read_by' => 'array'
    ];

    /**
     * @param $value
     * @return mixed
     */
    public function getDepartmentIdsAttribute($value) {
        return $value? json_decode($value)->value : $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed|void
     */
    public function setAttribute($key, $value) {
        switch ($key) {
            case 'department_ids':
                $this->attributes[$key] = is_array($value)? json_encode(['value' => $value]) : $value;
                break;
            case 'read_by':
                $this->attributes[$key] = is_array($value)? json_encode($value) : $value;
                break;
            default:
                $this->attributes[$key] = $value;
        }
    }
}
