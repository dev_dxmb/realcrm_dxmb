<?php

namespace App\Http\Controllers;

use App\BOBill;
use App\BOUser;
use App\Http\Controllers\API\AuthController;
use App\TemplateEmail;
use App\UserCustomerMapping;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\BOProduct;
use Pusher\Laravel\Facades\Pusher;
use App\Http\Controllers\API\APIController;
use App\Http\Controllers\API\LogController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
class Controller extends BaseController
{
    const TVC_HOST = 'https://ees.dxmb.vn/directrouter/index';
    const TVC_DTB = 'DXMBT';
    const DXMB_HOST = 'https://datxanhmienbac.com.vn';
    const SECURITY_CODE = 'D2EC-BDFF-763B-304D-2710-FBD3-1321-CB40-2445-AC12';
    const BO_PRIORITY_CODE = 'BO.PCN@DXMB!@#';
    const FCM_ACCESS_KEY = 'AAAARJk6EDE:APA91bGM3AC7ChzcOiRJd_TOWMHcWVLXqoXUo1tTy3_qqYGWeQ2jFppDuVg1G6D6MRaSpIbCSF1Kv8l76Skx4KL2dExpb2_KSeQab-gEqVtBQr0g7ppd6vMvAZfw9xHz_1BjrNJgJsW2';
    const TVC_HOST_ERP = 'https://erp.dxmb.vn/directrouter/index';

    const VNPay_SMS_Brand = 'http://14.160.87.122:8080/APISMS/SendSMS?WSDL';
    const VNPay_SMS_Exchange = 'http://210.245.12.220:19061/VCCBSMS/MTSend.asmx';


    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    static $vnp_TmnCode = "FU6CELID"; //Mã website tại VNPAY
    static $vnp_HashSecret = "OXMANDRPVKLBJSMRYYFYDMWEOUSGNYOI"; //Chuỗi bí mật
    static $vnp_Url = "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
    const ENCRYPTION_KEY = "datxanhmienbac.com.vn";
    static $pusher_apartment = [
        'channel' => 'apartment',
        'event'   => 'change-status-apartment'
    ];

    /**
     * Default role groups for quick config
     */
    const ROLE_GROUPS = [
        'SALE' => 'book',
        'PRODUCT_MANAGER' => 'approve',
        'ACCOUNTANT' => 'accounting',
        'CUSTOMER_SERVICE_STAFF' => 'confirm', // Customer Service Staff,
        'MANAGER' => 'manage'
    ];

    public static function encrypte($string,$key){
        $returnString = "";
        $charsArray = str_split("e7NjchMCEGgTpsx3mKXbVPiAqn8DLzWo_6.tvwJQ-R0OUrSak954fd2FYyuH~1lIBZ");
        $charsLength = count($charsArray);
        $stringArray = str_split($string);
        $keyArray = str_split(hash('sha256',$key));
        $randomKeyArray = array();
        while(count($randomKeyArray) < $charsLength){
            $randomKeyArray[] = $charsArray[rand(0, $charsLength-1)];
        }
        for ($a = 0; $a < count($stringArray); $a++){
            $numeric = ord($stringArray[$a]) + ord($randomKeyArray[$a%$charsLength]);
            $returnString .= $charsArray[floor($numeric/$charsLength)];
            $returnString .= $charsArray[$numeric%$charsLength];
        }
        $randomKeyEnc = '';
        for ($a = 0; $a < $charsLength; $a++){
            $numeric = ord($randomKeyArray[$a]) + ord($keyArray[$a%count($keyArray)]);
            $randomKeyEnc .= $charsArray[floor($numeric/$charsLength)];
            $randomKeyEnc .= $charsArray[$numeric%$charsLength];
        }
        return $randomKeyEnc.hash('sha256',$string).$returnString;
    }

    public static function decrypte($string,$key) {
        $returnString = "";
        $charsArray = str_split("e7NjchMCEGgTpsx3mKXbVPiAqn8DLzWo_6.tvwJQ-R0OUrSak954fd2FYyuH~1lIBZ");
        $charsLength = count($charsArray);
        $keyArray = str_split(hash('sha256',$key));
        $stringArray = str_split(substr($string,($charsLength*2)+64));
        $sha256 = substr($string,($charsLength*2),64);
        $randomKeyArray = str_split(substr($string,0,$charsLength*2));
        $randomKeyDec = array();
        if(count($randomKeyArray) < 132) return false;
        for ($a = 0; $a < $charsLength*2; $a+=2){
            $numeric = array_search($randomKeyArray[$a],$charsArray) * $charsLength;
            $numeric += array_search($randomKeyArray[$a+1],$charsArray);
            $numeric -= ord($keyArray[floor($a/2)%count($keyArray)]);
            $randomKeyDec[] = chr($numeric);
        }
        for ($a = 0; $a < count($stringArray); $a+=2){
            $numeric = array_search($stringArray[$a],$charsArray) * $charsLength;
            $numeric += array_search($stringArray[$a+1],$charsArray);
            $numeric -= ord($randomKeyDec[floor($a/2)%$charsLength]);
            $returnString .= chr($numeric);
        }
        if(hash('sha256',$returnString) != $sha256){
            return false;
        }else{
            return $returnString;
        }
    }

    /**
     * @param array $data
     * @param string $msg
     * @param array $info
     * @param array $logs
     * @return \Illuminate\Http\JsonResponse
     */
    protected static function jsonSuccess($data=[], $msg = "Thành công!", $info=[], $logs = []) {
        if ($logs&&count($logs)) {
            LogController::saveLogs($logs);
        }
        return response()->json(['success' => true, 'msg' => $msg, 'data' => $data, 'info' => $info], 200);
    }

    /**
     * @param string $reason
     * @param array $data
     * @param array $logs
     * @return \Illuminate\Http\JsonResponse
     */
    protected static function jsonError($reason = "Không thành công!", $data = [], $logs = []) {
        if ($logs&&count($logs)) {
            LogController::saveLogs($logs);
        }
        return response()->json(['success' => false, 'msg' => $reason, 'data' => $data], 200);
    }

    /**
     * @param $apartment_id
     * @param $new_status
     * @return bool
     */
    protected static function pusherToApartment($apartment_id, $new_status) {
        if (!in_array((int) $new_status, BOProduct::STATUSES)) return false;
        $data = [
            'apartmentId' => (string) $apartment_id,
            'newStatus'   => (string) $new_status,
            'statusDisplay' => BOProduct::STATUS_DISPLAY[$new_status]
        ];
        return Pusher::trigger(self::$pusher_apartment['channel'], self::$pusher_apartment['event'], $data);
    }

    /**
     * @param $number
     * @return bool|mixed|null|string
     */
    public static function convert_number_to_words($number) {
        $hyphen      = ' ';
        $conjunction = '  ';
        $separator   = ' ';
        $negative    = 'âm ';
        $decimal     = ' phẩy ';
        $dictionary  = array(
            0                   => 'không',
            1                   => 'một',
            2                   => 'hai',
            3                   => 'ba',
            4                   => 'bốn',
            5                   => 'năm',
            6                   => 'sáu',
            7                   => 'bảy',
            8                   => 'tám',
            9                   => 'chín',
            10                  => 'mười',
            11                  => 'mười một',
            12                  => 'mười hai',
            13                  => 'mười ba',
            14                  => 'mười bốn',
            15                  => 'mười năm',
            16                  => 'mười sáu',
            17                  => 'mười bảy',
            18                  => 'mười tám',
            19                  => 'mười chín',
            20                  => 'hai mươi',
            30                  => 'ba mươi',
            40                  => 'bốn mươi',
            50                  => 'năm mươi',
            60                  => 'sáu mươi',
            70                  => 'bảy mươi',
            80                  => 'tám mươi',
            90                  => 'chín mươi',
            100                 => 'trăm',
            1000                => 'nghìn',
            1000000             => 'triệu',
            1000000000          => 'tỷ',
            1000000000000       => 'nghìn tỷ',
            1000000000000000    => 'nghìn triệu triệu',
            1000000000000000000 => 'tỷ tỷ'
        );
        if (!is_numeric($number)) {
            return false;
        }
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }
        if ($number < 0) {
            return $negative . self::convert_number_to_words(abs($number));
        }
        $string = $fraction = null;
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . self::convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = self::convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= self::convert_number_to_words($remainder);
                }
                break;
        }
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        return $string;
    }

    /**
     * @param int $length
     * @return string
     */
    protected static function randString($length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function checkLoginTvcWeb($retry_login = true){
        $user = Auth::guard('loginfrontend')->user();
        if (!$user) return false;
        $tvcAcc = $user->ub_account_tvc;
        $data = [
            "action" => "ConnectDB",
            "method" => "checkSession",
            "data"   => [[]],
            "type"   => "rpc",
            "tid"    => rand(1,99)
        ];
        $response = self::TVC_POST_DECODE($data);
        if ($response) {
            if ($response['success']) return true;
        }
//        LogController::logUser('Phiên đăng nhập Tavico hết hạn. '.($retry_login? 'Thử đăng nhập lại...' : ''));
        if (!$retry_login) return false;
        /** @var $login_info */
        $login_info = [
            "id" => 1,
            "username" => $tvcAcc,
            "password" => self::decrypte(session('encryptPass'),self::ENCRYPTION_KEY),
            "appLog" => "Y",
            "securityKey" => $user->security_code
        ];
        $body = array (
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => time(),
        );
        $retry = self::TVC_POST_DECODE($body);
        $success = $retry&&$retry['success']&&$retry['data'];
//        if ($success) {
//            LogController::logUser('Đăng nhập lại Tavico thành công.', 'info');
//        } else {
//            LogController::logUser('Đăng nhập lại Tavico không thành công.', 'warning');
//        }
        return $success;
    }

    /**
     * @param $host
     * @param $method
     * @param $endpoint
     * @param array $data
     * @param array $headers
     * @param null $cookie_file
     * @param bool $new_session
     * @return bool|mixed
     */
    public static function callAPI($host, $method, $endpoint, $data = [], $headers = [], $cookie_file=null, $new_session = false){
        $curl = curl_init();
        $url = $host . $endpoint;
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            default:
                $url = sprintf("%s?%s", $url,count($data)>0?http_build_query($data):null);
        }
        $file = $cookie_file?? Auth::guard("loginfrontend")->user()->ub_account_tvc;
        $file = "/tmp/$file-cookies.txt";

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $file);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $file);
        /** Set a new cookie session */
        curl_setopt($curl, CURLOPT_COOKIESESSION, $new_session);

        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){ return false; };
        curl_close($curl);
        return $result;
    }

    /**
     * @param array $body
     * @param string $endpoint
     * @param null $cookie_file
     * @param bool $new_session
     * @return array
     */
    public static function TVC_POST_DECODE($body=[], $endpoint = '', $cookie_file=null, $new_session = false) {
        $tvcResponse = self::callAPI( self::TVC_HOST_ERP, 'POST', $endpoint, $body, [], $cookie_file, $new_session) ;
        return $tvcResponse? json_decode($tvcResponse, true)['result'] : ['success'=>false, 'data'=>$body, 'message' => 'Không thể kết nối Tavico'];
    }

    /**
     * @param string $field
     * @param null $old_value
     * @param null $new_value
     * @param int $uid
     * @param bool $api_gate
     * @return array
     */
    protected static function generateEditLog($field = '', $old_value = null, $new_value = null, $uid = 0, $api_gate = true) {
        return ['FIELD' => $field, 'OLD' => $old_value, 'VALUE' => $new_value, 'BY' => $uid>0? $uid : AuthController::getCurrentUID($api_gate), 'TIME' => time()];
    }
}
