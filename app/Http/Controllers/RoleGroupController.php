<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\BORoleGroup;
use App\BOUser;

class RoleGroupController extends Controller
{
    private $BORoleGroup;
    public function __construct() {
        $this->BORoleGroup = new BORoleGroup();
    }
    public function getAllRoleGroup(){
        $a_Data = $this->BORoleGroup->getAllSearch();
        
        $Data_view['a_RoleGroup'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];
        $a_User = BOUser::getAllUserInBo();
        $Data_view['a_Users'] = $a_User;
        
        return view('rolegroup.index',$Data_view);
        
    }
    
    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit RoleGroup
     *      */
    public function addEditRoleGroup(){
        $a_DataView = array();
        $a_User = BOUser::getAllUserInBo();
        
        $a_DataView['a_Users'] = $a_User;
        $RoleGroupId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $RoleGroupId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $this->BORoleGroup->AddEditRoleGroup($RoleGroupId);
                return redirect('role_groups')->with('status', 'Cập nhật thành công!');
        }
        
        $a_DataView['RoleGroupData'] = $RoleGroupId != 0 ? $this->BORoleGroup->getRoleGroupById($RoleGroupId) : array();

        return view('rolegroup.edit', $a_DataView );
    }
}
