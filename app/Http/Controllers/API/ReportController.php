<?php

namespace App\Http\Controllers\API;

use App\BOBill;
use App\BOCategory;
use App\BOProduct;
use App\BOUser;
use App\BOUserGroup;
use App\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * ReportController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5")?? null;
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware("laravel.jwt")->except(['deleteBillReportAjax']);
            $this->middleware("CheckStoredJWT")->except(['deleteBillReportAjax']);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function deleteBillReportAjax($id) {
        $query = Report::where('id', $id)->delete();
        return $query? self::jsonSuccess(compact('id')) : self::jsonError('Xóa không thành công');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function reportBill(Request $request) {
        $request->validate([
            'type'  => 'required|string',
            'from' => 'required|string',
        ]);
        /** @var $type */
        $type = $request->input('type');
        $source = $request->input('source', 0);
        if (!in_array($type, ['daily', 'weekly', 'monthly'])) return self::jsonError('Yêu cầu không hợp lệ!');
        /** @var $from */
        $from = $request->input('from');
        $to = $request->input('to', '');

        /** todo: List Departments */
        $departments = BOUserGroup::select([BOUserGroup::ID_KEY, 'gb_title AS title', 'gb_description AS alias'])
            ->where('gb_status', env('STATUS_ACTIVE',1))
            ->where('gb_code', 'S')
            ->get()->keyBy(BOUserGroup::ID_KEY);
        /**
         *todo: Get Department Statistics
         */
        /** @var $projects */
        $projects = [];
        $reports = $bills = [];

        if ($source===1) {
            /** @var $bills */
            $bills = BOBill::select(['id', 'staff_id', 'product_id'])
                ->where('bill_status', env('STATUS_ACTIVE', 1))
                ->whereIn('status_id', [
                    BOBill::STATUSES['STATUS_PAYING'],
                    BOBill::STATUSES['STATUS_PAID'],
                    BOBill::STATUSES['STATUS_DEPOSITED'],
                    BOBill::STATUSES['STATUS_FINALIZED'],
                    BOBill::STATUSES['STATUS_SUCCESS'],
                ]);
            switch ($type) {
                case 'daily':
                    $bills = $bills->whereDate('bill_created_time', $from);
                    break;
                case 'weekly':
                    $bills = $bills->whereBetween('bill_created_time', [$from . ' 00:00:00', $to . ' 23:59:59']);
                    break;
                case 'monthly':
                    $bills = $bills->whereBetween('bill_created_time', [$from, $to]);
                    break;
                default:
            }
            $bills = $bills
                ->whereHas('product.category.sibling')
                ->whereHas('staff')
                ->with([
                    'staff:group_ids,' . BOUser::ID_KEY,
                    'product' => function ($product) {
                        $product->select([BOProduct::ID_KEY, 'category_id'])
                            ->with(['category' => function ($category) {
                                $category->select(['parent_id', BOCategory::ID_KEY])
                                    ->with('sibling:id,' . BOCategory::ID_KEY);
                            }]);
                    }
                ])
                ->get();
//        return self::jsonSuccess($bills, 'Done', $request->all());
            if (!$bills) return self::jsonError('Không tìm thấy thống kê!');
            foreach ($bills as $key => $bill) {
                if (!isset($departments[$bill->staff->group_ids])) {
                    unset($bills[$key]);
                    continue;
                }
                $count = $departments[$bill->staff->group_ids]['count']?? 0;
                $departments[$bill->staff->group_ids]['count'] = $count + 1;
                $projects[] = $bill->product->category->sibling->{BOCategory::ID_KEY};
            }
        } else {
            $reports = Report::select('*')->whereNotNull('department')->whereNotNull('project');
            switch ($type) {
                case 'daily':
                    $reports = $reports->where('report_date', $from);
                    break;
                case 'weekly':
                    $reports = $reports->whereBetween('report_date', [$from . ' 00:00:00', $to . ' 23:59:59']);
                    break;
                case 'monthly':
                    $reports = $reports->whereBetween('report_date', [$from, $to]);
                    break;
                default:
            }
            $reports = $reports->get();
            foreach ($reports as $key => $report) {
                if (!isset($departments[$report->department])) {
                    unset($reports[$key]);
                    continue;
                }
                $count = $departments[$report->department]['count']?? 0;
                $departments[$report->department]['count'] = $count + $report->value;
                $projects[] = $report->project;
            }
        }


        /**
         * Extract item with count = 0
         */
        $weak_departments = $departments->where('count', 0);
        /**
         * Remove item with count <= 0
         */
        $departments = $departments->sortByDesc('count')->where('count', '>', 0);
        $departments = $departments->concat($weak_departments);

        /** @var $total */
        $total = 0;

        /**
         * todo: Get Project-Department statistics
         */
        /** @var $init_project_figures */
        $init_project_figures = [];
        $department_max = ['alias' => '', 'count' => 0];
        foreach ($departments as $key => $department) {
            $init_project_figures[$key] = 0;
            $total += $department['count']??0;
            if ($department_max['count'] < $department['count']) {
                $department_max = [
                    'count' =>  $department['count'],
                    'alias' =>  $department['alias'],
                ];
            } elseif ($department_max['count'] == $department['count']) {
                $department_max = [
                    'count' =>  $department['count'],
                    'alias' =>  $department_max['alias'] . ', ' . $department['alias'],
                ];
            }
        }
//        return self::jsonSuccess($departments, '', $init_project_figures);
        /** @var $project_list */
        $project_list = BOCategory::select([BOCategory::ID_KEY, 'cb_code AS code', 'cb_title AS title'])
            ->whereIn(BOCategory::ID_KEY, array_unique($projects))->get()->keyBy(BOCategory::ID_KEY);
        if ($source===1) {
            foreach ($bills as $bill) {
                $statistics = $project_list[$bill->product->category->sibling->{BOCategory::ID_KEY}]['statistics'] ?? $init_project_figures;
                $statistics[(string)$bill->staff->group_ids] += 1;
                $project_list[$bill->product->category->sibling->{BOCategory::ID_KEY}]['statistics'] = $statistics;
            }
        } else {
            foreach ($reports as $report) {
                $statistics = $project_list[$report->project]['statistics'] ?? $init_project_figures;
                $statistics[(string)$report->department] += $report->value;
                $project_list[$report->project]['statistics'] = $statistics;
            }
        }
        $project_max = ['sum' => 0, 'title' => ''];
        foreach ($project_list as &$item) {
            $item['sum'] = array_sum($item['statistics']);
            if ($project_max['sum'] < $item['sum']) {
                $project_max = ['sum' => $item['sum'], 'title' => $item['title']];
            } elseif ($project_max['sum'] == $item['sum']) {
                $project_max = ['sum' => $item['sum'], 'title' => $project_max['title'].', '.$item['title']];
            }
        }

        return self::jsonSuccess([
            'project_statistics' => $project_list,
            'department_statistics' => $departments,
        ], 'Done', [
            'count' => count($bills),
            'filter' => $request->all(),
            'reports' => count($reports),
            'bills'     => count($bills),
            'project_max'   => $project_max,
            'department_max' => $department_max,
            'total'         => $total
        ]);
    }
}
