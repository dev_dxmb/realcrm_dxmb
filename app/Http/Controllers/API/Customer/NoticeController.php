<?php

namespace App\Http\Controllers\API\Customer;

use App\CustomerNotice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoticeController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request) {
        /** @var $page */
        $page = $request->input('page', 1);
        $size = $request->input('size', env('PAGE_SIZE'));
        $offset = ($page-1) * $size;
        /** @var $uid */
        $uid = AuthController::getCurrentUID();
        /** @var $data */
        $data = CustomerNotice::select([CustomerNotice::ID_KEY, 'cn_title AS title', 'cn_shot_detail AS description', 'images AS array_images', 'updated_time'])
                ->where(['cn_status' => env('STATUS_ACTIVE', 1)])
                ->where(function ($query) use ($uid) {
                    $query->whereNull('group_customer_id')->orWhere("group_customer_id->$uid", $uid);
                })
                ->orderBy('updated_time', 'DESC')
                ->skip($offset)->take($size)->get();
        if (!$data) return self::jsonError('Không tìm thấy dữ liệu!');
        return self::jsonSuccess($data);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id = 0) {
        $data = CustomerNotice::select([
            CustomerNotice::ID_KEY,
            'cn_title AS title',
            'cn_shot_detail AS description',
            'images AS array_images',
            'updated_time',
            'cn_note AS content'
        ])->where(CustomerNotice::ID_KEY, $id)->first();
        if (!$data) return self::jsonError('Không tìm thấy dữ liệu!');
        return self::jsonSuccess($data);
    }
}
