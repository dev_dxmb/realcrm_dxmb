<?php

namespace App\Http\Controllers\API;

use App\BORole;
use App\BORoleGroup;
use App\BOUser;
use App\BOUserGroup;
use App\Mail\VerifyEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class BOUserController extends Controller
{
    /**
     * BOUserController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5")?? null;
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware("laravel.jwt")->except(['testFCM', 'verifyEmail', 'getDepartmentAjax', 'getUsersAjax']);
            $this->middleware("CheckStoredJWT")->except(['testFCM', 'verifyEmail', 'getDepartmentAjax', 'getUsersAjax']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request) {
        $options = $request->all();
        $data = BOUser::getAllUserInBo();
        $data =  DB::table('b_o_users')->join('b_o_user_groups', 'b_o_users.group_ids', '=', 'b_o_user_groups.gb_id')->select('b_o_users.*', 'b_o_user_groups.gb_title')->get();
        return self::jsonSuccess($data);
    }

    /**
     * @param null|integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getProfile($id=null) {
        /** @var $id */
        $id = $id?? Auth::guard('api')->user()->{BOUser::ID_KEY};
        /** @var $user */
        $user = BOUser::where(BOUser::ID_KEY, $id)->with('group:gb_title,'.BOUserGroup::ID_KEY)->first();
        if (!$user) return self::jsonError('Không tìm thấy hồ sơ người dùng!');
        $my_profile = Auth::guard('api')->user()->{BOUser::ID_KEY} === (int) $id;
        if (!$my_profile) {
            unset($user->is_verified);
            unset($user->signature);
        }
        unset($user->ub_token);
        unset($user->security_code);
        unset($user->remember_token);
        unset($user->remember_jwt);
        return self::jsonSuccess($user, 'Thành công', compact('my_profile'));
    }

    /**
     * @param string $account
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getProfileByAccount($account) {
        /** @var $user */
        $user = BOUser::select([
            BOUser::ID_KEY,
            'ub_title AS name',
            'ub_account_tvc AS account',
            'ub_avatar AS avatar',
            'group_ids',
            'signature',
            'is_verified'
        ])
            ->where('ub_account_tvc', trim($account))
            ->with('group:gb_title,'.BOUserGroup::ID_KEY)
            ->first();
        if (!$user) return self::jsonError('Không tìm thấy hồ sơ người dùng!');
        $my_profile = BOUser::getCurrent('ub_account_tvc') === $account;
        if (!$my_profile) {
            unset($user->is_verified);
            unset($user->signature);
        }

        return self::jsonSuccess($user, 'Thành công', compact('my_profile'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function updateProfile(Request $request) {
        $request->validate([
            'email' => 'email|required',
            'phone' => 'required'
        ]);
        /** @var $email */
        $email = trim($request->input('email', ''));
        /** @var $current_uid */
        $current_uid = Auth::guard('api')->user()->{BOUser::ID_KEY};
        /** @var $user */
        $user = BOUser::where(BOUser::ID_KEY, $current_uid)->first();
        if (!$user) return self::jsonError('Tài khoản không tồn tại!');
        if ($user->ub_status!=env('STATUS_ACTIVE')) return self::jsonError('Tài khoản chưa kích hoạt hoặc đang bị khóa!');
        if (BOUser::where(BOUser::ID_KEY, '!=', $current_uid)->where('ub_email', $email)->first()) {
            return self::jsonError('Email đã tồn tại trong hệ thống!');
        }
        $user->ub_phone = trim($request->input('phone', ''));
        if ($user->ub_email != $email) {
            $user->is_verified = false;
        }
        $user->ub_email = $email;
        if ($user->save()) {
            $verify_url = route('verify-email', ['uid' => $user->{BOUser::ID_KEY}, 'email' => $email]);
            Mail::to($email)->send(new VerifyEmail($user->ub_title, $verify_url));
            $msg = $user->is_verified? 'Cập nhật thành công' : 'Thành công, hãy kiểm tra email để kích hoạt tài khoản!';
            return self::jsonSuccess(['is_verified' => $user->is_verified], $msg);
        }
        return self::jsonError('Xảy ra lỗi khi cập nhật hồ sơ! Vui lòng thử lại...');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function listActive(Request $request) {
        $page = $request->input('page', 1);
        $size = $request->input('size', 10);
        $offset = ($page-1) * $size;
        /** @var $data */
        $data = BOUser::select([
            BOUser::ID_KEY,
            'ub_title AS name',
            'ub_avatar AS avatar',
            'ub_account_tvc AS account'
        ])
            ->whereNotNull('ub_avatar')
            ->orderBy('ub_last_logged_time', 'DESC')
            ->skip($offset)
            ->take($size)
            ->get();
        if (!$data) return self::jsonError('Xảy ra lỗi khi lấy DS người dùng mới online');
        return self::jsonSuccess($data);
    }

    /**
     * @param $uid
     * @param $email
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function verifyEmail($uid, $email) {
        if (!$uid || !$email) abort(302, 'Yêu cầu không hợp lệ!');
        $user = BOUser::where([
            BOUser::ID_KEY => (int) $uid,
            'ub_status'     => env('STATUS_ACTIVE', 1)
        ])->first();
        $msg = '';
        $error = '';
        if (!$user) {
            $error = 'Tài khoản không tồn tại hoặc đang bị khóa!';
            LogController::logUser('[verifyEmail] '. $error, 'error', (int) $uid);
        } else {
            if ($user->ub_email !== $email) abort(302, 'Yêu cầu không hợp lệ!');
            $user->is_verified = true;
            if ($user->save()) {
                $msg = 'Xác thực tài khoản thành công!';
            } else {
                $error = 'Xảy ra lỗi khi xác thực tài khoản của bạn! Vui lòng liên hệ BQT!';
            }
        }
        session()->flash('message', $msg);
        session()->flash('error', $error);
        return view('notification-simple');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function listDepartments(Request $request) {
        $page = $request->input('page', 1);
        $size = $request->input('size', 10);
        $keyword = $request->input('keyword', null);
        /** @var $type: null - all, 1 - department, 2 - branch */
        $type = $request->input('type', null);
        /** @var $offset */
        $offset = ($page - 1) * $size;
        /** @var $data */
        $data = BOUserGroup::select([
            BOUserGroup::ID_KEY . ' AS id',
            'gb_title AS title',
            'reference_code AS code'
        ])
            ->where('gb_status', env('STATUS_ACTIVE', 1));
        if ($type===1) $data = $data->where('gb_code', 'P');
        $data = $data->whereNotNull('gb_title')
            ->orderBy('gb_title', 'ASC')
            ->skip($offset)
            ->take($size);
        if ($keyword) $data = $data->where('gb_title', 'LIKE', "%$keyword%");
        $data = $data->get();
        if (!$data) return self::jsonError('Không tìm thấy phòng ban/sàn');
        return self::jsonSuccess($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getDepartmentAjax(Request $request) {
        $page = $request->input('page', 1);
        $size = $request->input('size', 10);
        $keyword = $request->input('keyword', null);
        /** @var $type: null - all, 1 - department, 2 - branch */
        $type = $request->input('type', null);
        /** @var $offset */
        $offset = ($page - 1) * $size;
        /** @var $data */
        $data = BOUserGroup::select([
            BOUserGroup::ID_KEY . ' AS id',
            'gb_title AS title',
            'gb_description AS alias',
            'reference_code AS code'
        ])
            ->where('gb_status', env('STATUS_ACTIVE', 1))
            ->whereNotNull('gb_title');
        if ($type===1) $data = $data->where('gb_code', 'P');
        $data = $data->orderBy('gb_title', 'ASC')
            ->skip($offset)
            ->take($size);
        if ($keyword) $data = $data->where('gb_title', 'LIKE', "%$keyword%");
        $data = $data->get();
        if (!$data) return self::jsonError('Không tìm thấy phòng ban/sàn');
        return self::jsonSuccess($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getUsersAjax(Request $request) {
        $page = $request->input('page', 1);
        $size = $request->input('size', 10);
        $keyword = $request->input('keyword', null);
        /** @var $offset */
        $offset = ($page - 1) * $size;
        $users = BOUser::select([BOUser::ID_KEY . ' AS id', 'ub_title AS name', 'ub_account_tvc AS account'])
            ->where('ub_status', env('STATUS_ACTIVE'));
        if ($keyword) {
            $users = $users->where(function ($query) use ($keyword) {
                    $query
                        ->where('ub_title', 'LIKE', "%$keyword%")
                        ->orWhere('ub_account_tvc', 'LIKE', "%$keyword%");
            });
        }
        $users = $users
            ->skip($offset)
            ->take($size)
            ->get();
        if (!$users) return self::jsonError('Không tìm thấy người dùng');
        return self::jsonSuccess($users);
    }

    /** Get Product Manager by Project
     * @param int $project
     * @return array|bool
     */
    public static function getManagerIDsByProject($project = 0) {
        if (!$project) return false;
        /** @var  $roles */
        $roles = BORole::select(['ro_id', 'ro_object_ids', 'ro_group_ids', 'ro_role_detail'])
            ->where([
                'ro_key' => 'project',
                'ro_status' => 1
            ])
            ->where([
                "ro_object_ids->$project" => (string) $project,
                "ro_role_detail->approve" => self::ROLE_GROUPS['PRODUCT_MANAGER']
            ])
            ->get();

        if (!$roles) return false;

        /** @var $role_groups */
        $role_groups = [];
        foreach ($roles as $role) {
            if ($role->ro_group_ids) $role_groups = array_merge($role_groups, array_values($role->ro_group_ids));
        }
        $role_staff = BORoleGroup::select(['rg_staff_ids'])
            ->where([
                'rg_status' => env('STATUS_ACTIVE', 1)
            ])
            ->whereIn(BORoleGroup::ID_key, $role_groups)
            ->get();
        if (!$role_staff) return false;

        /** @var $managers */
        $managers = [];
        foreach ($role_staff as $staff) {
            $managers = array_merge($managers, array_values($staff->rg_staff_ids));
        }
        return $managers;
    }

    /** return array of User ID_keys
     * @param bool $api_gate
     * @return array|bool
     */
    public static function getCurrentDepartmentHeads($api_gate=true) {
        $department = BOUser::getCurrent('group_ids', $api_gate);
        return self::getUsersByDepartment($department, self::ROLE_GROUPS['MANAGER']);
    }

    /**
     * @param BOUser $user
     * @return array|bool
     */
    public static function getDepartmentHeads(BOUser $user) {
        $department = $user? $user->group_ids : null;
        if (!$department) return false;
        return self::getUsersByDepartment($department, self::ROLE_GROUPS['MANAGER']);
    }

    /**
     * @param int $group_id
     * @param string $role_action
     * @return array|bool
     */
    private static function getUsersByDepartment($group_id = 0, $role_action = 'view') {
        if (!$group_id) return false;
        $role_groups = AuthController::getRoleGroupsByAction($role_action);
        $group_users = AuthController::getUsersByRoleGroups($role_groups);
        /** @var $users */
        $users = BOUser::select(BOUser::ID_KEY)->where('ub_status', '!=', env('STATUS_DELETE',-1))
                ->where('group_ids', (int) $group_id)->whereIn(BOUser::ID_KEY, $group_users)->pluck(BOUser::ID_KEY)->toArray();
        return $users?? [];
    }

    /**
     * @param $group_ids
     * @return array
     */
    public static function getStaffByGroups($group_ids) {
        $group_ids = array_wrap($group_ids);
        $users = BOUser::select(BOUser::ID_KEY)->where('ub_status', '!=', env('STATUS_DELETE', -1))
                ->whereIn('group_ids', $group_ids)
                ->pluck(BOUser::ID_KEY);
        return $users?? [];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function notifications(Request $request) {
        $page = (int) $request->input('page', 1);
        $type = $request->input('type', null);
        $offset = ($page-1)*env('PAGE_SIZE', 25);

        $data = [];

        return $data? self::jsonSuccess($data, "Thành công!", array_merge($request->all()))
            : self::jsonError('Không thành công!', $request->all());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeAvatar(Request $request) {
        $image = $request->input('avatar', null);
        if (!$image) return self::jsonError('Chưa chọn ảnh đại diện!');
        if (starts_with($image, url(''))) {
            $image = str_after($image, url(''));
        } else {
            $image = str_replace(' ', '+', $image);
            $image = str_after($image, 'data:image/jpeg;base64,');
            $imageName = 'oba' . AuthController::getCurrentUID() . '.jpg';
            $root_path = public_path('avatars/' . $imageName);
            File::put($root_path, base64_decode($image));
            $image = '/avatars/' . $imageName;
            if (file_exists(public_path($image))) {
                $optimizerChain = OptimizerChainFactory::create();
                $optimizerChain->optimize(public_path($image));
            }
        }
        $query = BOUser::where(BOUser::ID_KEY, AuthController::getCurrentUID())->update(['ub_avatar' => $image, 'ub_last_logged_time' => now()]);
        if (!$query) return self::jsonError('Cập nhật ảnh đại diện không thành công!');
        return self::jsonSuccess(['url' => url($image)], 'Cập nhật ảnh đại diện thành công!');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkRead() {
        $count_unread = BOUser::select(BOUser::ID_KEY)->where(BOUser::ID_KEY, AuthController::getCurrentUID())
            ->with(['notifications' => function($query) {
                return $query->where('is_read', false);
            }])->first();
        return self::jsonSuccess(['count' => $count_unread]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncFCMstaffDXMB() {
        $data = APIController::DXMB_POST('/api/app/getAllFcm', '123', []);
        $count = 0;
        $success_count = 0;
        $failures = [];
        if (is_array($data)) {
            foreach ($data as $item) {
                if ($item->is_dxmb&&$item->tvcAccount) {
                    $count += 1;
                    $query = BOUser::where('ub_account_tvc', $item->tvcAccount)->update(['ub_token' => $item->token]);
                    if ($query) {
                        $success_count += 1;
                    } else {
                        $failures[] = $item->tvcAccount;
                    }
                }
            }
            return self::jsonSuccess(
                ['total' => count($data), 'processed' => $count, 'success' => $success_count, 'failure' => count($failures)],
                "Đã xử lý $count/".count($data)." nhân viên, $success_count thành công, ".count($failures)." thất bại!",
                $failures
            );
        } else {
            return self::jsonError('Không tìm thấy dữ liệu');
        }
    }

}
