<?php

namespace App\Http\Controllers\API;

use App\BOCustomer;
use App\BOUser;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordResetController extends Controller
{
    /** Create token password reset
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        /** @var $is_customer */
        $is_customer = $request->input('customer', true);
        $request->validate([
            'email' => 'required|string|email',
            'customer' => 'required|bool'
        ]);
        /** @var $email */
        $email = trim($request->email);
        if ($is_customer) {
            $user = BOCustomer::where('cb_email', $email)->first();
        } else {
            $user = BOUser::where('ub_email', $email)
                    ->where('ub_status', '!=', env('STATUS_DELETE', -1))
                    ->first();
        }

        if (!$user) return self::jsonError('Người dùng không tồn tại trên hệ thống!', $user);
        /** @var $reset_request */
        $reset_request = PasswordReset::updateOrCreate(
            ['email' => $email],
            [
                'token' => str_random(60),
                'is_customer' => $is_customer
            ]
        );

        if ($user && $reset_request) {
            $name = $is_customer? 'bạn' : $user->ub_title;
            $reset = $user->notify(new PasswordResetRequest($reset_request->token, $email, $name));
            return self::jsonSuccess($reset, 'Đã gửi mail xác nhận reset mật khẩu! '.$email, [
                'is_customer' => $is_customer,
                'user'        => $user
            ]);
        }
        return self::jsonError('Không thành công! Vui lòng thử lại sau!');
    }

    /**
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function find($token)
    {
        $reset_request = PasswordReset::where('token', $token) ->first();
        if (!$reset_request) abort(403,'Yêu cầu không hợp lệ hoặc đã hết hạn!');
        if (Carbon::parse($reset_request->updated_at)->addMinutes(20)->isPast()) {
            $reset_request->delete();
            abort(403, 'Yêu cầu khôi phục mật khẩu đã hết hạn');
        }
        return view('auth.passwords.reset', ['token' => $reset_request->token, 'is_customer' => $reset_request->is_customer]);
    }

     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return string
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string',
        ]);

        $password_reset = PasswordReset::where([
            ['token', $request->token],
            ['email', trim($request->email)]
        ])->first();

        if (!$password_reset) abort(404, 'Yêu cầu không hợp lệ hoặc đã hết hạn!');
        /** Find User */
        if ($request->input('customer', true)) {
            $user = BOCustomer::where('cb_email', $password_reset->email)->first();
            $password_field = 'cb_password';
            $name = 'bạn';
        } else {
            $user = BOUser::where('ub_email', $password_reset->email)->first();
            $password_field = 'password';
            $name = $user->ub_title;
        }
        if (!$user) abort(403, 'Không tìm thấy người dùng với địa chỉ email: '.$password_reset->email);

        $user->{$password_field} = bcrypt($request->password);
        if (!$user->save()) abort(500, 'Cập nhật mật khẩu không thành công!');

        $password_reset->delete();
        $user->notify(new PasswordResetSuccess($password_reset->email, $name));

        return "Thay đổi mật khẩu thành công!";
    }
}
