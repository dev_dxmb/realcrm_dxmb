<?php

namespace App\Http\Controllers\API;

use App\BOBill;
use App\BOPayment;
use App\BOProduct;
use App\BOUser;
use App\Http\Controllers\MailController;
use App\UserCustomerMapping;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class BOPaymentController extends Controller
{
    /**
     * BOPaymentController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5")?? null;
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware("laravel.jwt");
            $this->middleware("CheckStoredJWT");
        }
    }

    /**
     * @param Request $request
     * @param $bid
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $bid = 0) {
        $options = $request->all();
        $page = isset($options['page'])? (int) $options['page'] : 1;
        $page_size = isset($options['size'])? (int) $options['size'] : env("PAGE_SIZE", 15);

        $offset = ($page-1)*$page_size;

        $data = BOPayment::select("*")->where($bid > 0? ['bill_code' => (int) $bid] : [])
                ->skip($offset)->take($page_size)
                ->with([
                    'bill:bill_id,bill_title,bill_reference_code,bill_product_ids',
                    'customer:cb_id,cb_name',
                    'paymentMethod:pm_id,pm_title'
                ])->get();
        return self::jsonSuccess($data);
    }

    /**
     * @param int $bill_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaymentsByBill($bill_id = 0) {
        if (!$bill_id) return self::jsonError('Không tồn tại hợp đồng!');
        $where = AuthController::is_sale_staff()? ['staff_id' => AuthController::getCurrentUID()] : [];
        /** @var $bill */
        $bill = BOBill::select([
            BOBill::ID_KEY,
            'bill_code',
            'status_id',
        ])
            ->where($where)
            ->where(BOBill::ID_KEY, (int) $bill_id)
            ->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng!', ['item' => $bill_id]);
        /** @var $data */
        $data = BOPayment::select([
            'id', 'pb_id',
            'pb_status AS status',
            'pb_code AS code',
            'bill_code',
            'pb_note AS note',
            'customer_id', 'request_staff_id', 'request_time', 'response_staff_id',
            'pb_request_money AS request_money',
            'pb_response_time AS response_time',
            'pb_response_money AS response_money',
            'method_id', 'pb_bank_number_id AS bank_number',
            'pb_images'
        ])
            ->where(['bill_code' => $bill->bill_code])->with([
                'paymentMethod:pm_id,pm_title',
                'responseStaff' => function($accountant) {
                    $accountant->select([BOUser::ID_KEY, 'ub_title AS name', 'group_ids'])->with('group:gb_id,gb_title');
                },
                'requestStaff' => function($sale) {
                    $sale->select([BOUser::ID_KEY, 'ub_title AS name', 'group_ids'])->with('group:gb_id,gb_title');
                },
            ])
            ->orderBy('request_time', 'DESC')->get();

        $info = ['statuses' => BOPayment::STATUS_TEXTS];
        $info['addPaymentRequest'] = false;
        if (AuthController::is_sale_staff()) {
            $status_for_payment = [
                BOBill::STATUSES['STATUS_APPROVED_BY_CUSTOMER'],
                BOBill::STATUSES['STATUS_PAYING'],
                BOBill::STATUSES['STATUS_PAID'],
                BOBill::STATUSES['STATUS_DEPOSITED'],
                BOBill::STATUSES['STATUS_BOOKED'],
            ];
            $info['addPaymentRequest'] = in_array($bill->status_id, $status_for_payment);
        }
        return self::jsonSuccess($data, 'Thành công', $info);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id=0) {
        if (!$id) {
            return self::jsonError("Không tìm thấy ID giao dịch thanh toán");
        }
        $data = BOPayment::where([BOPayment::ID_KEY => (int) $id])
                ->with([
                    'bill:bill_id,bill_title,bill_reference_code,bill_product_ids',
                    'customer:cb_id,cb_name',
                    'paymentMethod:pm_id,pm_title',
                    'requestStaff:ub_id,ub_title'
                ])
                ->first();
        if ($data) {
            return self::jsonSuccess($data);
        } else {
            return self::jsonError('Không tìm thấy giao dịch thanh toán', ['item' => $id]);
        }
    }

    /*** Nhân viên kinh doanh gửi y/c  xác nhận thanh toán. Chờ KT duyệt
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitForm(Request $request) {
        $form_data = $request->validate([
            'bill_id' => 'required',
            'amount' => 'required',
            'method_id' => 'required',
            'images' => 'nullable',
            'note' => 'nullable'
        ]);
        /** @var $bill */
        $where = [BOBill::ID_KEY => (int) $form_data['bill_id']];
        if (AuthController::is_sale_staff()) { $where['staff_id'] = AuthController::getCurrentUID(); };
        /** @var $bill */
        $bill = BOBill::where($where)->withCount('payments')->first();
        if (!$bill) return self::jsonError('Không tìm thấy hợp đồng');

        $query = new BOPayment;
        $query->{BOPayment::ID_KEY} = null;
        $query->pb_status = BOPayment::STATUS['PENDING'];
        $query->pb_code = null;
        $query->reference_code = null;
        $query->bill_code = $bill["bill_code"];
        $query->pb_note = $form_data["note"]?? null;
        $query->customer_id = $bill->customer_id;
        $query->request_staff_id = AuthController::getCurrentUID();
        $query->request_time = now();
        $query->pb_request_money = (double) $form_data["amount"];
        $query->method_id =(int) $form_data["method_id"];
        $query->pb_bank_number_id = null;
        $query->type = $bill->type != BOBill::TYPE_DEPOSIT ? 2 : BOBill::TYPE_DEPOSIT ;
        if (isset($form_data['images']) && count($form_data['images'])>0) {
            $images = [];
            foreach ($form_data['images'] as $image) {
                if (starts_with($image, url(''))) {
                    $images[] = str_after($image, url(''));
                } else {
                    $image = str_replace(' ', '+', $image);
                    $image = str_after($image, 'data:image/jpeg;base64,');
                    $imageName = time() . str_random(2) . '.jpg';
                    $root_path = public_path('uploads' . '/' . $imageName);
                    File::put($root_path, base64_decode($image));
                    $images[] = '/uploads/' . $imageName;
                }
            }
            $query->pb_images = $images;
        }
//        return self::jsonSuccess($bill);
        if (!$query->save()) self::jsonError("Tạo yêu cầu không thành công!", $query);
        /** todo: Update bill status */
        if (!$bill->payments_count || $bill->payments_count<1) {
            $bill->status_id = BOBill::STATUSES['STATUS_PAID_ONCE'];
        }
        $bill->save();
        /** Push fcm & store */
        \App\Http\Controllers\NotificationController::notifyPaymentCreated($query, $bill->product_id);
        /** Save logs */
        LogController::logPayment('Y/C xác nhận thanh toán: '.$query->pb_code, 'info', $query->{BOPayment::ID_KEY});

        //Send mail///
        MailController::customerPayment($query);
        return self::jsonSuccess($query, 'Thêm dữ liệu thành công!', $bill->payments_count);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListPaymentMethod()
    {
        $data = [
          [
            'id' => '1536405015',
            'title' => 'Chuyển khoản',
            'bank_numbers' => [
                '1536678017' => 'VPBank - 29839283498893',
                '1536722290' => 'TPbank - 12312313123',
            ]
          ],
            [
                'id' => '1536405030',
                'title' => 'Quẹt thẻ',
                'bank_numbers' => [
                    '1536404933' => 'Vietcombank - 1014101444',
                    '1536405380' => 'TCB - 15830.3656',
                ]
            ],
            [
                'id' => '123456',
                'title' => 'Tiền mặt',
                'bank_numbers' => [
                ]
            ],
        ];

        return self::jsonSuccess($data);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(Request $request, $id=0) {
        if ($id>0) {
            $query = BOPayment::where([BOPayment::ID_KEY => $id])->first();
            if (!$query) {
                return self::jsonError('Không tìm thấy giao dịch', ['item' => $id]);
            }
        } else {
            return self::jsonError('Không tìm thấy ID giao dịch', ['item' => $id]);
        }
    }

    /**
     * @param $order_id
     * @param string $order_description
     * @param int $amount
     * @return bool|string
     */
    public static function vnpay($order_id, $order_description='', $amount=0) {
        $vnp_OrderType = 'user';
        $vnp_Amount = $amount * 100;
        $vnp_Locale = 'vi';
        $vnp_BankCode = '';
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => self::$vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $order_description,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => route('vnpay-response'),
            "vnp_TxnRef" => $order_id,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hash_data = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hash_data .= '&' . $key . "=" . $value;
            } else {
                $hash_data .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = self::$vnp_Url . "?" . $query;
        if (isset(self::$vnp_HashSecret)) {
            $vnpSecureHash = md5(self::$vnp_HashSecret . $hash_data);
            $vnp_Url .= 'vnp_SecureHashType=MD5&vnp_SecureHash=' . $vnpSecureHash;
            return $vnp_Url;
        } else {
            return false;
        }
    }

    /**
     * @auth: HuyNN
     * @since: 05/09/2018
     * @Des: add, edit Payment Tavico
     *      */
    public static function createPaymentTvc($data,$type,$bill){
        $product = BOProduct::where([BOProduct::ID_KEY => $bill->product_id])->first();
        $customerMap = UserCustomerMapping::where(["id_customer" =>$bill->customer_id])->first();
        $staff = BOUser::where([BOUser::ID_KEY => $bill->staff_id])->first();
        switch ($type) {
            case BOPayment::TYPE_PAYMENT_TAVICO['DCHO']:
                $revenuetype = 'TTDCH';
                $d_c = 'C';
                $anal_r0 = '';
                $anal_r2 = $customerMap->potential_reference_code;
                $anal_r3 = $bill->bill_reference_code;
                $anal_r6 = 'Z';
                $customer = '';
                break;
            case BOPayment::TYPE_PAYMENT_TAVICO['DCO']:
                $revenuetype = 'TTDCO';
                $d_c = 'C';
                $anal_r0 = $bill->contract_reference_code;
                $anal_r2 = '';
                $anal_r3 = '';
                $anal_r6 = $customerMap->customer_reference_code;
                $customer = $customerMap->customer_reference_code;
                break;
            case BOPayment::TYPE_PAYMENT_TAVICO['CANCEL_DCHO']:
                $revenuetype = 'TTDCH';
                $d_c = 'D';
                $anal_r0 = '';
                $anal_r2 = $customerMap->potential_reference_code;
                $anal_r3 = $bill->bill_reference_code;
                $anal_r6 = 'Z';
                $customer = '';
                break;
            default:
        }
        $DataPayment =
            array (
                'revenuetype' => $revenuetype, /// Loại thu
                'transdate' => date('Y-m-d',time()),
                'transmonth' => '',
                'transref' => '+',
                'property' => $product->pb_code, //Mã căn hộ
                'customer' => $customer,
                'bankcode' => '',
                'd_c' => $d_c, // C- Thu, D -Chi
                'amount' => $data['pb_response_money']? str_replace(',','',$data['pb_response_money']): "",
                'anal_r0' => $anal_r0, // Mã giao dịch
                'anal_r1' => $staff->ub_tvc_code, ///
                'anal_r2' => $anal_r2, //Mã KHTN
                'anal_r3' => $anal_r3, //Mã Phiếu đặt chỗ
                'anal_r6' => $anal_r6, //Mã KH-NCC
                'extreference1' => $customerMap->c_title, // Ng nộp
                'extreference2' => '', //Số bảng kê
                'extdescription1' => $data['pb_note']?? "", //Diễn giải
                'extdescription2' => $data['pb_note']?? "", // Ghi chú
//                'extdescription3' => Auth::guard('loginfrontend')->user()->ub_title, //Người thu tiền
                'extdescription3' => ''
        );

        $body = array(
            'action' => 'FrmPmTransaction',
            'method' => 'roughEntry',
            'data' => [$DataPayment],
            'type' => 'rpc',
            'tid' => 1,
        );
        $res = APIController::TVC_POST_DECODE($body);
        $referenceCode = '';
        if($res['success'] == true){
            $referenceCode = $res['data'][0]['transref'];
        }
        return $referenceCode;
    }
}
