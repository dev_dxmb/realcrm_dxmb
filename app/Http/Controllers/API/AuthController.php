<?php

namespace App\Http\Controllers\API;

use App\BORole;
use App\BORoleGroup;
use App\BOUser;
use App\BOUserGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\RoleController;



class AuthController extends Controller
{
    /**
     * AuthController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $DXMB_MD5 = $request->header("Content-MD5")?? null;
        if ($DXMB_MD5 != APIController::BO_PRIORITY_CODE) {
            $this->middleware('laravel.jwt', ['except' => ['login', 'register', 'tvcLogin']]);
            $this->middleware('CheckStoredJWT', ['except' => ['login', 'register', 'tvcLogin', 'checkRole']]);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function testLogin()
    {
        $user = BOUser::where(['ub_email' => 'abc@example.com'])->first();
        $token = JWTAuth::attempt(['ub_email' => 'abc@example.com', 'password' => '123456']);
        if (!$token) {
            return response()->json(['invalid_email_or_password'], 422);
        }
        return($token);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'username' => 'string|required',
            'password' => 'string|required',
        ]);
        /** @var $username */
        $username = trim($request->input('username'));
        /** @var $password */
        $password = trim($request->input('password'));

        /** @var $token */
        $token = $query->remember_jwt?? Auth::guard('api')
                ->setTTL(3600 * 24 * 7)
                ->attempt([
                    'ub_account_tvc' => $username,
                    'password'  => $password
                ]);
        if (!$token) return self::jsonError('Thông tin đăng nhập không chính xác!');

        /** @var $query */
        $query = BOUser::where(['ub_account_tvc' => trim($username)])
                        ->with('group:gb_title,' . BOUserGroup::ID_KEY)
                        ->first();
        if ($query->remember_jwt) {
            $token = $query->remember_jwt;
        } else {
            $query->remember_jwt = $token;
        }
        $query->ub_last_logged_time = now();
        $query->save();
        /** @var $roles */
        $roles = RoleController::getAppMenuByUID($query->{BOUser::ID_KEY}, true);
        return $this->respondWithToken($token, [
            'roles' => $roles,
            'name' => $query->ub_title,
            'avatar' => $query->ub_avatar,
            'department' => $query->group? [
                'name'  => $query->group->gb_title,
                'id'    => $query->group->{BOUserGroup::ID_KEY}
            ] : null
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $form_data = $request->all();
        if (!$form_data['ub_email'] || !$form_data['password']) {
            return response()->json(['error' => 'Chưa đủ thông tin đăng ký', 'success'=>false]);
        } else {
            $query = new BOUser;
            $query->ub_id = strtotime("now");
            $query->ub_status = env("STATUS_ACTIVE", 1);
            $query->ub_email = $form_data['ub_email'];
            $query->ub_created_time = now();
            $query->password = Hash::make($form_data['password']);
            if ($query->save()) {
                return self::jsonSuccess([], "Đăng ký thành công");
            } else {
                return response()->json(['success' => false, 'msg' => 'Đăng ký không thành công']);
            }
        }
    }

    /***
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function massCreate(Request $request) {
        if (!$request->input('data')) {
            return response()->json(['success' => false, 'msg' => 'Không có dữ liệu']);
        }
        $data = $request->input('data');
        $success_count = 0;
        $results = [];
        foreach ($data as $key => &$item) {
            $item['ub_id'] = strtotime("now")+$key;
            $item['ub_created_time'] = now();
            $init = BOUser::where(['ub_email' => $item['ub_email']])->orWhere(['ub_account_name' => $item['ub_account_name']])->first();
            if (!$init) {
                $init = BOUser::create($item);
                $success_count += $init? 1 : 0;
                $results[] = 'Đã thêm người dùng có ub_email = '.$item['ub_email'] . ' và account = ' . $item['ub_account_name'];
            } else {
                $results[] = 'Đã tồn tại người dùng có ub_email = '.$item['ub_email'] . ' hoặc account = ' . $item['ub_account_name'];
            }
        }
        if ($success_count) {
            return response()->json(['success' => true, 'msg' => 'Thành công! Đã thêm '. $success_count . ' bản ghi', 'data' => $results ]);
        } else {
            return response()->json(['success' => false, 'msg' => 'Người dùng đã tồn tại hoặc xảy ra lỗi khi lưu dữ liệu!' ]);
        }
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        if (Auth::guard('api')->check()) {
            return response()->json([
                'success'=>true,
                'loggedIn' => true,
                'data' => Auth::user(),
//                'payload' => Auth::guard('api')->payload()->get('__TVC__')
            ]);
        } else {
            return response()->json(['success'=>false, 'loggedIn' => false, 'msg'=>'Hết hạn đăng nhập...'], 401);
        }
    }

    /** todo: Middleware - Validate api JWT by comparing to Database remember_jwt
     * @return bool
     */
    public static function checkJWT() {
        $valid = false;
        if (Auth::guard('api')->check()) {
            $jwt = (string) Auth::guard('api')->getToken();
            $query = BOUser::where([
                BOUser::ID_KEY => self::getCurrentUID(),
                'remember_jwt' => $jwt
            ])->first();
            if ($query) $valid = true;
        }
        return $valid;
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(Auth::refresh());
    }

    /**
     * @param $token
     * @param array $info
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $info=[])
    {
        return self::jsonSuccess([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 60,
            'info' => $info //** todo: Deprecated soon */
        ], 'Đăng nhập thành công', $info);
    }

    /**
     * @param bool $api_gate
     * @return int
     */
    public static function getCurrentUID($api_gate = true) {
        return BOUser::getCurrentUID($api_gate);
    }

    /**
     * @param bool $api_gate
     * @return array
     */
    private static function getCurrentRoleGroups($api_gate = true) {
        $uid = self::getCurrentUID($api_gate);
        $groups = BORoleGroup::select(['rg_id'])->where([
            'rg_status' => env('STATUS_ACTIVE', 1),
            "rg_staff_ids->$uid" => (string) $uid
        ])->get();
        $data = [];
        foreach ($groups as $group) {
            $data[] = $group->rg_id;
        }
        return $data;
    }

    /**
     * @param string $object
     * @param string $role
     * @param bool $api_gate
     * @return array
     */
    public static function getObjectsByUserRole($object='project', $role='view', $api_gate = true) {
        /** @var $groups */
        $groups = self::getCurrentRoleGroups($api_gate);
        /** @var $objects */
        $objects = [];
        /** @var $group */
        foreach ($groups as $group) {
            $role_query = BORole::select(['ro_object_ids'])
                ->where([
                    "ro_status" => env("STATUS_ACTIVE", 1),
                    "ro_key"    => $object,
                    "ro_group_ids->$group" => (string) $group,
                    "ro_role_detail->$role" => (string) $role
                ])->get();

            /** @var $roles */
            $roles = [];
            if ($role_query) {
                foreach ($role_query as $role_item) {
                    if ($role_item->ro_object_ids) $roles = array_values($role_item->ro_object_ids);
                }
            }
            $objects = array_merge($roles, $objects);
        }
        return $objects;
    }

    /**
     * @param array $role_groups
     * @return array
     */
    public static function getUsersByRoleGroups($role_groups = []) {
        $role_groups = array_wrap($role_groups);
        $groups = BORoleGroup::select('rg_staff_ids')->where([
            'rg_status' => env('STATUS_ACTIVE',1)
        ])->whereIn(BORoleGroup::ID_key, $role_groups)->get();
        if (!$groups) return [];
        $users = [];
        foreach ($groups as $group) {
            if ($group->rg_staff_ids) $users = array_merge($users, $group->rg_staff_ids);
        }
        return array_unique($users);
    }

    /**
     * @param string $action
     * @return array
     */
    public static function getRoleGroupsByAction($action = 'view') {
        /** @var $roles */
        $roles = BORole::select('ro_group_ids')->where([
            'ro_status' => env('STATUS_ACTIVE', 1),
            "ro_role_detail->$action" => $action
        ])->get();
        if (!$roles) return [];
        /** @var $role_groups */
        $role_groups = [];
        foreach ($roles as $role) {
            if ($role->ro_group_ids) $role_groups = array_merge($role_groups, $role->ro_group_ids);
        }
        return array_unique($role_groups);
    }

    /**
     * @param string $action
     * @param bool $api_gate
     * @return bool
     */
    private static function checkRoleGroupAction($action = '',$api_gate = true) {
        /** @var $groups */
        $groups = self::getCurrentRoleGroups($api_gate);
        if (!$groups || count($groups)===0) return false;
        /** @var $group */
        foreach ($groups as $group) {
            $role_query = BORole::where([
                "ro_status" => env("STATUS_ACTIVE", 1),
                "ro_key"    => 'project',
                "ro_group_ids->$group" => (string) $group,
                "ro_role_detail->$action" => (string) $action
            ])->count();
            if ($role_query>0) {
                return true;
                break;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function is_admin() {
        return BOUser::is_admin(true);
    }

    /** Is Sale staff ?
     * @return bool
     */
    public static function is_sale_staff() {
        return !self::is_accountant()&&!self::is_product_manager()&&!self::is_cs_staff()&&!self::is_manager();
    }

    /** Is Product Manager ?
     * @param $api_gate bool
     * @return bool
     */
    public static function is_product_manager($api_gate=true) {
        return self::checkRoleGroupAction(self::ROLE_GROUPS['PRODUCT_MANAGER'],$api_gate);
    }

    /** Is Accountant ?
     * @param $api_gate bool
     * @return bool
     */
    public static function is_accountant($api_gate = true) {
        return self::checkRoleGroupAction(self::ROLE_GROUPS['ACCOUNTANT'], $api_gate);
    }

    /** Is Customer Service Staff ?
     * @param $api_gate bool
     * @return bool
     */
    public static function is_cs_staff($api_gate = true) {
        return self::checkRoleGroupAction(self::ROLE_GROUPS['CUSTOMER_SERVICE_STAFF'] , $api_gate);
    }

    /**
     * @return bool
     */
    public static function is_manager() {
        return self::checkRoleGroupAction(self::ROLE_GROUPS['MANAGER']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkRole(Request $request) {
//        $object = $request->input('object');
//        $uid = $request->input('uid');
        $apartments = []; $projects = [];
        if (AuthController::is_product_manager()) {
            $projects = self::getObjectsByUserRole('project', self::ROLE_GROUPS['PRODUCT_MANAGER']);
            $apartments = BOProductController::getApartmentsByProject($projects);
        }
        return self::jsonSuccess([
            'cs_staff'          => AuthController::is_cs_staff(),
            'accountant'       => AuthController::is_accountant(),
            'manager'       => AuthController::is_manager(),
            'product_manager' => AuthController::is_product_manager(),
            'sale'              => AuthController::is_sale_staff()
        ], 'Lấy DS Phân quyền thành công', [
            'projects' => $projects,
            'apartments' => $apartments
        ]);
    }

    /**
     * @param $plain
     * @return string
     */
    public static function makeHash($plain) {
        return Hash::make($plain);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tvcLogin(Request $request) {
        //** STEP 1: LOG IN  TAVICO - get user group */
        //** STEP 2: LOG IN  MONGODB */
        //** STEP 3: LOG IN  BO */
        $form_data = $request->only(['username', 'password', 'security_key', 'id']);
        /** @var $login_info */
        $login_info = [
            "id" => 1,
            "username" => $form_data['username'],
            "password" => $form_data['password'],
            "appLog" => "Y",
            "securityKey" => $form_data['security_key']
        ];
        $body = array (
            'action' => 'ConnectDB',
            'method' => 'getConfig',
            'data' => [$login_info],
            'type' => 'rpc',
            'tid' => time(),
        );
        /** @var $tvc_response */
        $tvc_response = APIController::TVC_POST_DECODE($body, '', $form_data['username'], true);
//        return self::jsonSuccess($tvc_response);
        if ($tvc_response && $tvc_response['success']) {

            /** @var $data */
            $data = $tvc_response['data'];
            $tvc_account = $data['operatorid'];
            $tvc_staff_code = $data['employee'];
            $dxmb_code = 'DXBO' . ltrim(substr($tvc_staff_code, -4), '0');

            /** Todo: Get TVC group - Update to BO User Group */
            $tvc_group = APIController::TVC_GET_EMPLOYEE_GROUP($tvc_staff_code, $tvc_account);
            if ($tvc_group) {
                $loginDomain = str_after($form_data['username'], '@');
                ///Search
                $group_code = $tvc_group['site'].'@'.$loginDomain;
                $group_name = $tvc_group['name1'];
                /** @var $group */
                $group = BOUserGroup::where(['reference_code' => $group_code])->first();
                if (!$group) {
                    $group = new BOUserGroup();
                    $group->{BOUserGroup::ID_KEY} = time();
                    $group->level_id = BOUserGroup::LEVELS['DEPARTMENT'];
                    $group->gb_created_time = now();
                    $group->reference_code = $group_code;
                    $group->gb_status = env('STATUS_ACTIVE', 1);
                }
                $group->gb_title = $group_name;
                $group->gb_updated_time = now();
                $group->save();
            } else {
                $group = null;
                $group_code = null;
            }

            /** TODO: LOGIN MONGO */
            $mongo_response = APIController::DXMB_POST('/api/app/resJwtForBo', null, [
                "tvcCode" => $tvc_staff_code,
                "tvcAccount" => $tvc_account,
                "tvcPass" => $form_data['password'],
                "tvcCompany" => $data['company'],
                "fullname" => $data['operatorname'],
                "code" => $dxmb_code,
                "username" => $form_data['username'],
                "site"      => $group_code
            ]);

//            return self::jsonSuccess($mongo_response);
            $mongo_jwt = null;
            if (!env('MAINTENANCE_MODE', false)) {
                if (!$mongo_response || !$mongo_response['success'] || !$mongo_response['alert']) {
                    return self::jsonError($mongo_response && $mongo_response['alert'] ?? 'Lỗi máy chủ Đất Xanh Miền Bắc!', [$mongo_response]);
                }
                $mongo_jwt = $mongo_response['alert'];
            }

                /** @var $query */
            $query = BOUser::firstOrNew(['ub_account_tvc' => $tvc_account], [
                BOUser::ID_KEY => time()
            ]);

            if (!$query->exists) {
                //** Sync Tavico user to BOUser */
                $query->ub_account_tvc = $tvc_account;
                $query->ub_status = env('STATUS_ACTIVE', 1);
                $query->ub_title = $data['operatorname'];
                $query->ub_account_name = $tvc_account;
                $query->ub_email = isset($data['email'])&&$data['email']!=''? $data['email'] : null;
                $query->ub_created_time = null;
                $query->ub_staff_code = $dxmb_code;
            }
            $query->ub_tvc_code = $tvc_staff_code;
            $query->group_ids = $group? $group->{BOUserGroup::ID_KEY} : null;
            $query->security_code = $form_data['security_key'];
            $query->password = self::makeHash(trim($form_data['password']));
            $token = null;

            //** Update Security code + add info */
            if ($query->save()) {
                $token = Auth::guard('api')->setTTL(3600 * 24 * 7)->claims(['__TVC__' => $form_data['password']])->login($query);
//                return $token;
                //** Save JWT */
                $query->remember_jwt = $token;
                $query->ub_last_logged_time = null;
                $query->save();
            } else {
                LogController::logUser('[TVCLogin] Cập nhật TT user vào BO không thành công', 'error');
                return self::jsonError('Lỗi xử lý thông tin người dùng!', $query);
            }

            /** @var $token */
            if ($token) {
                $roles = RoleController::getAppMenuByUID($query->{BOUser::ID_KEY}, true);
                return self::jsonSuccess([
                    'mongo_jwt' => $mongo_jwt,
                    'ob_jwt' => $token
                    ],
                    'Đăng nhập thành công',
                    ['staff' => $query, 'roles' => $roles, 'department' => $group? $group->gb_title : null]
                );
            } else {
                return self::jsonError('Xảy ra lỗi khi đăng nhập hệ thống Đất Xanh', [$query]);
            }
        } else {
            return self::jsonError('Không tìm thấy tài khoản trên Tavico', $tvc_response['data']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateFCMToken(Request $request)
    {
        $token = $request->input('token', null);
        $query = BOUser::where([BOUser::ID_KEY => self::getCurrentUID()])
                        ->update(['ub_last_logged_time' => now(), 'ub_token' => $token]);
        if (!$query) {
            LogController::logUser('Không thể cập nhật FCM token trên BO', 'warning', AuthController::getCurrentUID(), true);
            return self::jsonError('Không thành công', compact('token'));
        } else {
            $form_data = ['token' => $token, 'tvc_account' => BOUser::getCurrent('ub_account_tvc')];
            if (!env('MAINTENANCE_MODE', false)) {
                $mongo_query = APIController::DXMB_POST('/api/app/checkAndSaveToken', null, $form_data);
                if (!$mongo_query || $mongo_query['success'] != 1) {
                    LogController::logUser('Không thể cập nhật FCM token vào trang ĐXMB', 'warning', AuthController::getCurrentUID(), true);
                }
            }
            return self::jsonSuccess(compact('token'), 'Cập nhật FCM token thành công');
        }
    }

}
