<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\AuthController;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\BORole;
use App\BORoleGroup;
use App\BOUser;
use Auth;
use App\BOCategory;

class RoleController extends Controller
{
    private $BORole;
    private $BOUser;
    public function __construct() {
        $this->BORole = new BORole();
        $this->BOUser = new BOUser();
    }

    /**
     * @param string $role
     * @return array
     */
    public static function getProjectsByUserRole($role='view') {
        $role_groups = AuthController::getObjectsByUserRole('project', $role, false);
        return $role_groups;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function index(){
        $a_Data = $this->BORole->getAllSearch();
        
        $Data_view['a_Role'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];                
        
        return view('roles.index',$Data_view);
    }

    public function getAllRoleMenu(){
        $a_Data = $this->BORole->getAllSearchMenuRole();
        
        $Data_view['a_Role'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];                
        
        return view('roles.indexMenu',$Data_view);
        
    }
    public function getAllRoleMenuWeb(){
        
        $a_data = array();
        $o_Db = DB::table('role_menu_web')->select('*');
        $a_data = $o_Db->orderBy('updated_at', 'desc')->paginate(30);
        foreach ($a_data as $key => &$val) {
            $val->stt = $key + 1;
        }
        
        $Data_view['a_Role'] = $a_data;
        return view('roles.indexMenuWeb',$Data_view);
        
    }
    
    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit Role
     *      */
    public function addEditRole(){
        $a_DataView = array();
        $a_RoleGroups = BORoleGroup::getAllRoleGroups();
        $a_DataView['a_RoleGroups'] = $a_RoleGroups;
        $a_DataView['role_key'] = config('cmconst.role_key');
        $a_DataView['role_action'] = config('cmconst.role_action');
        
        $BOCategory = new BOCategory();
        $BOCategory->getAllCategoriesByParentID(0, $a_project);
        $a_DataView['a_projects'] = $a_project;
        
        $RoleId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $RoleId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $this->BORole->AddEditRole($RoleId);
                return redirect(route('role-list'))->with('status', 'Cập nhật thành công!');
        }
        
        $a_DataView['RoleData'] = $RoleId != 0 ? $this->BORole->getRoleById($RoleId) : array();

        return view('roles.edit', $a_DataView );
    }

    /**
     * @param null $uid
     * @param bool $api_gate
     * @return array
     */
    public static function getAppMenuByUID($uid = null, $api_gate = true){
        $uid = $uid?? BOUser::getCurrentUID($api_gate);
        $role_groups = self::getUserRoleGroups($uid, $api_gate);
        /** @var $app_menu */
        $app_menu = [];
        if ($role_groups) {
            foreach ($role_groups as $role_group) {
                $menu = self::getAppMenuByRoleGroup($role_group);
                $app_menu = array_merge($app_menu, $menu);
            }
        }
        if (count($app_menu)>0) return array_unique($app_menu);
        return [
            'personal',
            'customer',
            'project',
            'projectNews',
            'requestPersonal',
            'contract',
            'forum',
            'internalNews',
            'qrScanner',
            'report'
        ];
    }

    /**
     * @param null $uid
     * @param bool $api_gate
     * @return array
     */
    public static function getUserRoleGroups($uid = null, $api_gate = false) {
        $uid = $uid?? BOUser::getCurrentUID($api_gate);
        $groups = BORoleGroup::select(['rg_id'])->where([
            'rg_status' => env('STATUS_ACTIVE', 1),
            "rg_staff_ids->$uid" => (string) $uid
        ])->get();
        $data = [];
        foreach ($groups as $group) {
            $data[] = $group->rg_id;
        }
        return $data;
    }

    /**
     * @param null|integer $role_group
     * @return array
     */
    private static function getAppMenuByRoleGroup($role_group = null) {
        if (!$role_group) return [];
        $menu = DB::table('b_o_role_menu')
            ->select('mb_menu_detail')
            ->where([
                'mb_status' => env('STATUS_ACTIVE', 1),
                "role_group_id->$role_group"    => (string) $role_group
            ])
            ->get()
            ->pluck('mb_menu_detail')->first();
        return $menu? collect(json_decode($menu))->values()->toArray() : [];
    }
    
    public function ConfigRoleMenu(){
        
        $a_DataView = array();
        $a_RoleGroups = BORoleGroup::getAllRoleGroups();
        $a_DataView['a_RoleGroups'] = $a_RoleGroups;
        $a_DataView['app_menu'] = config('cmconst.app_menu');
        
        $RoleId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $RoleId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $this->BORole->AddEditRoleMenu($RoleId);
                return redirect(route('app-menu'))->with('status', 'Cập nhật thành công!');
        }
        
        $a_DataView['RoleData'] = $RoleId != 0 ? $this->BORole->getRoleMenuById($RoleId) : array();
        return view('roles.editRoleMenu', $a_DataView);
    }
    
    public function editRoleMenuWeb(){
        
        
        $a_AllRole = config('cmconst.web_menu');
        $a_RoleGroups = BORoleGroup::getAllRoleGroups();
        $a_NameController = config('cmconst.name_module');
        
        
        $a_DataView = array();
        $i_RoleGroup_id = (int)Input::get('id',0);
        $checksubmit = Input::get('submit');
        if(isset($checksubmit) && $checksubmit != "")
        {
            $this->BORole->AddEditRoleMenuWeb($i_RoleGroup_id); 
            return redirect(route('web-menu'))->with('status', 'Cập nhật thành công!');
        }
        $a_DataView['a_RoleActive'] = $this->BORole->a_GetAllRoleByRoleGroupIDFilter($i_RoleGroup_id);
        $a_DataView['i_id'] = $i_RoleGroup_id;
        $a_DataView['a_AllRole'] = $a_AllRole;
        $a_DataView['a_NameController'] = $a_NameController;
        $a_DataView['a_RoleGroups'] = $a_RoleGroups;
        $a_DataView['a_RoleMenu'] = isset($a_DataView['a_RoleActive']) ? (array)$a_DataView['a_RoleActive']->role_menu : null;
        
        return view('roles.editMenuWeb', $a_DataView);
    }
    
    public static function getRoleMenuWebByUserId($userId = '1537242545'){
        $userId = Auth::guard('loginfrontend')->user()->ub_id;
        $aryReturn = [];
        $aGroupRole = DB::table('b_o_role_groups')->select('*')->where('rg_status', 1)->where('rg_staff_ids', 'like', '%'.$userId.'%')->get();
        if($aGroupRole){
            $MenuRole = array();
            foreach($aGroupRole as $val){
                $aRoleMenu = DB::table('role_menu_web')->select('*')->where('status', 1)->where('role_group_ids', 'like', '%'.$val->rg_id.'%')->get();                
                if($aRoleMenu){
                    foreach($aRoleMenu as $valMenu){
                        
                        $fr1 = (array) json_decode($valMenu->role_menu);                                                
                        foreach($fr1 as $key1 => $valMenu1){
                            if(isset($aryReturn[$key1])){
                                $aryReturn[$key1] = array_merge($aryReturn[$key1],$valMenu1);
                            }else{
                                 $aryReturn[$key1] = $valMenu1;
                            }
                            $aryReturn[$key1] = array_unique($aryReturn[$key1]);
                        }
                    }
                        
                    }
                }                
            }            

            return $aryReturn;
            
        }
    
    
}
