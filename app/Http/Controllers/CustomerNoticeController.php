<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\CustomerNotice;

class CustomerNoticeController extends Controller
{
    private $o_CustomerNotice;
    public function __construct() {
        $this->o_CustomerNotice = new CustomerNotice();
    }
    public function getAllCustomerNotice(){
        $a_Data = $this->o_CustomerNotice->getAllSearch();        
        
        $Data_view['a_CustomerNotice'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];        
        return view('customerNotice.index',$Data_view);
        
    }
    
    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit customer
     */
    public function addEditCustomerNotice(){
        $a_DataView = array();
        $CustomerNoticeId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $CustomerNoticeId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $this->o_CustomerNotice->AddEditCustomerNotice($CustomerNoticeId);
                return redirect('customer_notice/list')->with('status', 'Cập nhật thành công!');
        }
        $a_DataView['CustomerNoticeData'] = $CustomerNoticeId != 0 ? $this->o_CustomerNotice->getCustomerNoticeById($CustomerNoticeId) : array();

        return view('customerNotice.edit', $a_DataView );
    }
}
