<?php

namespace App\Http\Controllers;
use App\BOPayment;
use App\BOProduct;
use App\BOTransaction;
use App\BOUser;
use App\Http\Controllers\API\APIController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\BOBillController;
use App\Http\Controllers\API\BOProductController;
use App\Http\Controllers\API\LogController;
use App\TemplateEmail;
use App\UserCustomerMapping;
use Illuminate\Support\Facades\Auth;
use App\BOCategory;
use App\BOCustomer;
use Illuminate\Http\Request;
use DB;
use App\BOBill;
use Illuminate\Support\Facades\Mail;
use Pusher\Laravel\Facades\Pusher;
use PDF;
class BillController extends Controller
{
    public function __construct() {

    }
    public function getAllBill(Request $request){
//        $a = 'ub_id';
//        echo "<pre>";
//        print_r(Auth::guard('loginfrontend')->user()->{$a});
//        echo "</pre>";
//        die();
//        $this->view->disable();

        $a_Data = BOBill::getAllSearch();
        $a_DataView['a_AllBill'] = $a_Data['a_data'];
//        $Data_view['a_search'] = $a_Data['a_search'];
        $a_DataView['a_Project'] = BOCategory::getAllProject();
        return view('bill.index',$a_DataView);
    }

    public function confirm(Request $request){
        $request = $request->input();
        $staff_id = $request['staff'];
        $bill = BOBill::select(['*','bill_total_money as total_money','bill_required_money as required_money'])->where([
            BOBill::ID_KEY => $request['bill_id']])->with([
            'staff' => function($staff) {
                $staff->select([
                    'ub_id',
                    'ub_title AS name',
                    'ub_account_name AS account',
                    'ub_email AS email',
                    'ub_avatar AS avatar',
                    'signature'
                ]);
            },
            'transaction' => function($transaction) {
                $transaction->select([
                    'trans_id', 'trans_deposit AS transDeposit'
                ]);
            },
//            'payments' => function($payments) {
//                $payments->select([
//                    'bill_code', 'pb_response_money AS responseMoney'
//                ]);
//            },
            'product' => function($apartment) {
                return $apartment->select([
                    "pb_id",
                    "pb_title AS apartment", "pb_code AS code",
                    "category_id",
                    "pb_required_money AS money",
                    "pb_used_s", "pb_built_up_s", "pb_code_real", "pb_price_total",
                    "pb_price_per_s", "price_used_s"
                ])
                    ->with([ "category" => function($building) {
                        return $building->select([
                            "id", "cb_id", "parent_id", "cb_title AS building"
                        ])
                            ->where("cb_level", 2)
                            ->with(["sibling" => function($project) {
                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])
                                    ->where("cb_level", 1);
                            }]);
                    }]);
            }
        ])->first();

        if($bill){
//            $deposit = $bill->transaction? $bill->transaction->transDeposit : 0;
            if(isset(BOBill::STATUSES[$request['status']])){
                $userInfo = Auth::guard('loginfrontend')->user();
                $current_log = $bill->bill_approval_log;
                $current_log[] = BOBill::generateApprovalLogWeb(BOBill::STATUSES[$request['status']],$userInfo->ub_id);
                $bill->bill_approval_log = $current_log;
                $bill->status_id = BOBill::STATUSES[$request['status']];
                if($bill->save()){
                    if($request['status'] == 'STATUS_APPROVED'){
                        $apartmentCode = $bill->product->code;
                        $numberFloor = (string) substr($apartmentCode, -6, 3);
                        $typeApart = (string) substr($apartmentCode, -3);
                        $buildingCode = substr($apartmentCode, 0, 6);

                        $templateEmail = TemplateEmail::where(['project' => $bill->product->category->sibling->cb_id])->first();
                        if ($templateEmail){
                            $content = str_replace("@tenkhachhang",$bill->bill_customer_info->name,$templateEmail->html);
                            $content = str_replace("@cmnd",$bill->bill_customer_info->id_passport,$content);
                            $content = str_replace("@ngaycap",$bill->bill_customer_info->issue_date,$content);
                            $content = str_replace("@noicap",$bill->bill_customer_info->issue_place,$content);
                            $content = str_replace("@diachi",$bill->bill_customer_info->address,$content);
                            $content = str_replace("@thuongtru",$bill->bill_customer_info->permanent_address,$content);
                            $content = str_replace("@sodienthoai",$bill->bill_customer_info->phone,$content);
                            $content = str_replace("@email",$bill->bill_customer_info->email,$content);
                            $content = str_replace("@duan",$bill->product->category->sibling->project,$content);
                            $content = str_replace("@macanho",$bill->product->code,$content);
                            $content = str_replace("@tang",$numberFloor,$content);
                            $content = str_replace("@toa",$buildingCode,$content);
                            $content = str_replace("@canso",$typeApart,$content);
                            $content = str_replace("@tonggia",number_format($bill->total_money),$content);
                            $content = str_replace("@dttimtuong", $bill->product->pb_built_up_s, $content);
                            $content = str_replace("@dtthongthuy", $bill->product->pb_used_s, $content);
                            $content = str_replace("@giatimtuong", number_format($bill->product->pb_price_per_s), $content);
                            $content = str_replace("@giathongthuy", number_format($bill->product->price_used_s), $content);

                            $randomStr = self::randString();
                            BOCustomer::where([BOCustomer::ID_KEY => $bill->customer_id])->update(array('cb_password' => bcrypt($randomStr) ,'cb_status' => 1));
                            $content = str_replace("@matkhau", $randomStr, $content);

                            $tongtienbangchu = ucfirst(self::convert_number_to_words($bill->total_money));
                            $content = str_replace("@tongtienbangchu",$tongtienbangchu,$content);

                            $tiendattruoc = str_replace(',','',$bill->transaction->transDeposit);
                            $content = str_replace("@tiendattruoc",number_format($tiendattruoc),$content);

                            $dattruocbangchu = ucfirst(self::convert_number_to_words($tiendattruoc));
                            $content = str_replace("@dattruocbangchu",$dattruocbangchu,$content);

                            $sotienconlai = (int)$bill->required_money - (int)$tiendattruoc;
                            $sotienconlai = $sotienconlai <= 0 ? 0 : $sotienconlai;
                            $content = str_replace("@tienconlai",number_format($sotienconlai),$content);

                            $content = str_replace("@sotienquydinh",number_format($bill->required_money),$content);
                            $content = str_replace("@timtuong",$bill->product->pb_built_up_s,$content);
                            $content = str_replace("@thongthuy",$bill->product->pb_used_s,$content);
                            $content = str_replace("@tennhanvien",$bill->staff->name,$content);

                            $chukynhanvien = $bill->staff->signature != '' ? '<img style="width: 150px; height: 150px" src="http://bo.dxmb.vn/'.$bill->staff->signature.'">' :'<img style="width: 250px; height: 150px" src="">';
                            $content = str_replace("@chukynhanvien",$chukynhanvien,$content);

                            ///Get chu ky cua TKKD//
                            $logBill = $bill->bill_approval_log;
                            $chukyTKKD = '';
                            $chukyQLKD = '';
                            $tenthuky = '';
                            $tengiamdocsan = '';
                            if(count($logBill)){
                                foreach ($logBill as $log){
                                    if($log['VALUE'] == BOBill::STATUSES['STATUS_APPROVED']){
                                        $createdUser = $log['CREATED_BY'];
                                        $user = BOUser::where([BOUser::ID_KEY => $createdUser])->first();
                                        if($user){
                                            $chukyTKKD = $user->signature == '' ? '<img style="width: 150px; height: 150px" src="http://bo.dxmb.vn/'.$user->signature.'">' : '<img style="width: 250px; height: 150px" src="">';
                                            $tenthuky = $user->ub_title;
                                        }
                                    }
                                }
                            }

                            $content = str_replace("@chukytkkd",$chukyTKKD,$content);;
                            $content = str_replace("@chukygiamdocsan",$chukyQLKD,$content);

                            $content = str_replace("@tenthuky",$tenthuky,$content);
                            $content = str_replace("@giamdocsan",$tengiamdocsan,$content);
                            $content = str_replace("@ngay",date("d"),$content);
                            $content = str_replace("@thang",date("m"),$content);
                            $content = str_replace("@nam",date("Y"),$content);

                            $data = ['content' => $content];
                            $filePdfName = 'hop-dong-'.time();
                            PDF::loadView('pdf/index',compact('data'))->save('pdf/'.$filePdfName.'.pdf');

                            $email = 'huynn@dxmb.vn';
                            //$email = $bill->bill_customer_info->email;
                            Mail::send('mail.customer',array('content' => $content, 'bill' => $bill), function($message) use ($email,$filePdfName)
                            {
                                ///Gửi email test///
                                $message->from('noreply@dxmb.vn', 'Hệ thống Book Online Đất Xanh Miền Bắc');
                                $message->to($email);
                                $message->subject('Yêu cầu xác nhận đơn hàng từ Đất Xanh Miền Bắc');
                                $message->attach(public_path().'/pdf/'.$filePdfName.'.pdf', array());
                            });
                        }
                    }
                    if($request['status'] == 'STATUS_TVC_CONTRACT'){
                        BOBillController::AddContractTVC($request['bill_id']);
                    }
                    return redirect('bill')->with('status', 'Cập nhật thành công!');
                }
            }else{
                return redirect('bill')->with('status', 'Trạng thái không hợp lệ!');
            }
        }else{
            return redirect('bill')->with('status', 'Không tồn tại Yêu cầu này!');
        }
    }

    public function changeStatus(Request $request) {
        $status = $request->input("status_id", null);
        $id = $request->input("bill_id", null);
        $old_procedure = $request->input("old_procedure", null);
        $disallowed_status = [
            BOBill::STATUSES['STATUS_DISAPPROVED_BY_CUSTOMER'],
            BOBill::STATUSES['STATUS_APPROVED_BY_CUSTOMER']
        ];
        if ($id > 0 || !$status || in_array((int) $status, $disallowed_status)) {
            if (!in_array($status, BOBill::STATUSES) && $status!='RENEW_TRANSACTION') {
                return redirect('bill')->with('status', 'Trạng thái không hợp lệ!');
            }
            /** @var $query */
            $query = BOBill::where([BOBill::ID_KEY => $id])
                ->with([
                    'staff' => function($staff) {
                        $staff->select([
                            'ub_id',
                            'ub_title AS name',
                            'ub_account_name AS account',
                            'ub_email AS email',
                            'ub_avatar AS avatar',
                            'signature',
                            'ub_tvc_code',
                            "group_ids",
                            'ub_account_tvc'
                        ])
                            ->with(["group" => function($group) {
                                return $group->select(["gb_title"]);
                            }]);
                    },
                    'transaction' => function($transaction) {
                        $transaction->select([
                            'trans_id', 'trans_deposit AS deposit_amount', 'staff_code'
                        ]);
                    },
                    'product' => function($apartment) {
                        return $apartment->select([
                            "pb_id",
                            "pb_title AS apartment", "pb_code AS code",
                            "category_id", "pb_required_money",
                            "pb_required_money AS required_money",
                            "pb_used_s", "pb_built_up_s", "pb_code_real", "pb_price_total",
                            "price_used_s","pb_price_per_s",
                            "stage", "book_type"
                        ]);
                    },
                ])->first();
            if (!$query) {
                return redirect('bill')->with('status', 'Không tìm thấy hợp đồng!');
            }
            $query->bill_updated_time = now();
            $msg = "Cập nhật thành công";
            /** @var $branch_product_manager */
            $branch_product_manager = Auth::guard('loginfrontend')->user()->group_ids;
            /** @var $staff */
            $staff = $query->staff;
            $branch_check = $staff && $staff->group_ids == $branch_product_manager ? 1 : 0;
            $branch = $staff? str_after($staff->ub_account_tvc, '@') : null;

            // check CRM
            $crm_product_manager = Auth::guard('loginfrontend')->user()->ub_account_tvc ? str_after(Auth::guard('loginfrontend')->user()->ub_account_tvc, '@') : null;
            $crm_check = $crm_product_manager && $branch == $crm_product_manager ? 1 : 0; // 1 la cung san, 0 la khac san
            $cmConfigF1AccountCode = config('cmconst.account_CRM_F1');
            $cmConfigAccount = config('cmconst.account_CRM');

            //** Handle new status */
            switch ((int) $status) {
                case BOBill::STATUSES['STATUS_PAYING']:
                    if (!AuthController::is_product_manager(false)) return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
                    /** Save tvc contract */
                    if($query->type == BOBill::TYPE_DEPOSIT) {
                        $tvc_booking_status = 'XNDCO';
                        // ghi giao dich xuông F1
                        if($crm_check == 0){
                            $logs[] = LogController::logBill('[STATUS_PAYING] Khởi tạo GD trên F1 Tavico!', 'info', $id, false, false);
                            if (!$query->contract_reference_code_F1) {
                                $tvc_contract_F1 = APIController::TVC_CONTRACT_CREATE_F1($query, 0, $crm_product_manager, $cmConfigF1AccountCode[$crm_product_manager][$branch], false);
                                if (!$tvc_contract_F1['success'] || !$tvc_contract_F1['data']) {
                                    $logs[] = LogController::logBill('[STATUS_PAYING] Không thể tạo GD trên F1 Tavico!', 'error', $id, false, false);
                                    return redirect('bill')->with('status', $tvc_contract_F1['msg'] ?? 'Không thể tạo HĐ F1 trên Tavico');
                                }
                                $query->contract_reference_code_F1 = $tvc_contract_F1['data'];
                            } else {
                                $logs[] = LogController::logBill('[STATUS_PAYING] Đã tồn tại GD trên F1 Tavico: '.$query->contract_reference_code_F1, 'info', $id, false, false);
                            }
                        }
                        if (!$query->contract_reference_code) {
                            $logs[] = LogController::logBill('[STATUS_PAYING] Khởi tạo GD trên F2 Tavico!', 'info', $id, false, false);
                            /** @var $tvc_contract */
                            $tvc_contract = APIController::TVC_CONTRACT_CREATE($query, $branch_check, $branch, false);
                            if (!$tvc_contract['success']) return redirect('bill')->with('status', $tvc_contract_F1['msg'] ?? 'Không thể tạo HĐ trên Tavico');
                            $query->contract_reference_code = $tvc_contract['data'] ?? null;
                        } else {
                            $logs[] = LogController::logBill('[STATUS_PAYING] Đã tồn tại GD trên F2 Tavico: '.$query->contract_reference_code, 'info', $id, false, false);
                        }
                    }else{
                        $tvc_booking_status = 'XNDCH';
                    }
                    APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, $tvc_booking_status, $id, $branch_check, $branch, false);
                    if($crm_check == 0){
                        APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code_F1, $tvc_booking_status, $id, 0, $crm_product_manager, false);
                    }
                    /** todo: Update apartment status & trigger pusher */
                    BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['CDDCO'], true, false, false);
                    break;
                case BOBill::STATUSES['STATUS_DISAPPROVED']:
                    /** TKKD từ chối HĐ */
                    if (!AuthController::is_product_manager(false)) return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
                    if ($query->bill_reference_code) APIController::TVC_CANCEL_BOOKING_REQUEST($query->bill_reference_code, $id, true, $branch, false);
                    BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['MBA'], true, false , false);
                    break;
                case BOBill::STATUSES['STATUS_CANCEL_APPROVED']:
                    //** todo: TKKD duyệt Huỷ Cọc, chờ DVKH duyệt */
                    if (!AuthController::is_product_manager(false)) return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
                    if ($query->type!=BOBill::TYPE_DEPOSIT) return redirect('bill')->with('status', 'Thao tác không hợp lệ với HĐ đặt chỗ!');
                    break;
                case BOBill::STATUSES['STATUS_CANCELED']:
                    /** NVKD huỷ - TKKD, DVKH duyệt huỷ */
                    $current_status = $query->status_id;
                    $valid = false;
                    if (AuthController::is_product_manager(false)) {
                        //** todo:  TKKD duyệt huỷ chỗ ****** */
                        if ($current_status != BOBill::STATUSES['STATUS_CANCEL_REQUEST'] || $query->type == BOBill::TYPE_DEPOSIT) return redirect('bill')->with('status', 'Thao tác không hợp lệ!');
                        $valid = true;
                        if ($query->bill_reference_code) {
                            // chuyển tình trạng phiếu đặt chỗ
                            APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, 'XNHDCH', $id, $branch_check, $branch, false);
                        }
                        ///Send mail///
                        MailController::customerStatusBill($query,BOBill::STATUSES['STATUS_CANCELED']);
                    } elseif (AuthController::is_cs_staff(false)) {
                        //** todo:  DVKH duyệt huỷ cọc **** */
                        if ($current_status != BOBill::STATUSES['STATUS_CANCEL_APPROVED'] || $query->type != BOBill::TYPE_DEPOSIT) return redirect('bill')->with('status', 'Thao tác không hợp lệ!');
                        $valid = true;
                        if($query->contract_reference_code && $query->contract_reference_code != ''){
//                            APIController::TVC_CS_CANCEL_CONTRACT($query->contract_reference_code, null, $branch_check, $branch, $query->product->code, false);
                        }else{
                            APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, 'YCDCO', $id, $branch_check, $branch , false);
                        }

                        ///Send mail///
                        MailController::customerStatusBill($query,BOBill::STATUSES['STATUS_CANCELED']);
                    }
                    /** todo: Update BO Transaction **** */
                    if ($valid) {
                        /** @var  $transaction */
                        $transaction = BOTransaction::where(BOTransaction::ID_KEY, '=', $query->transaction_id)->first();
                        if ($transaction) {
                            $current_log = $transaction->trans_status_log;
                            $current_log[] = BOTransaction::generateStatusLog(BOTransaction::STATUS_CODES['BILL_CANCEL'], 0, false);
                            $transaction->save();
                        }
                        /** Release Apartment */
                        BOProductController::releaseApartment($query->product_id, true, $query->staff_id);
                    }
                    break;
                case BOBill::STATUSES['STATUS_APPROVED']:
                    /** TKKD duyệt yêu cầu HĐ */
                    if (!AuthController::is_product_manager(false)) return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
                    /** todo: F1 Customer & Bill Request */
                    if($crm_check == 0){
                        $logs[] = LogController::logBill('[STATUS_APPROVED] Ghi nhận phiếu sang F1', 'info', $id, false, false);
                        /** @var $tvc_potential_code_F1 */
                        $tvc_potential_code_F1 = APIController::TVC_GET_POTENTIAL_CUSTOMER_BY_PHONE($query->bill_customer_info->phone, false);
                        if (!$tvc_potential_code_F1) {
                            $logs[] = LogController::logBill('[STATUS_APPROVED] Chưa có khách hàng tiềm năng F1. Đang đồng bộ...'.$query->bill_customer_info->phone, 'info', $id, false, false);
                            $tvc_add_potential_customer_F1 = APIController::TVC_ADD_POTENTIAL_CUSTOMER_BK($query->bill_customer_info->name, $query->bill_customer_info->phone, $cmConfigF1AccountCode[$crm_product_manager][$branch], $cmConfigAccount[$crm_product_manager]['username'], false);
                            if (!$tvc_add_potential_customer_F1) {
                                $msg = 'Xảy ra lỗi khi thêm khách hàng F1 trên Tavico. Hãy đăng nhập lại!';
                                $logs[] = LogController::logBill('[F1-PotentialCustomer] '.$msg, 'warning', $id, false, false);
                                return redirect('bill')->with('status', $msg);
                            } else {
                                $tvc_potential_code_F1 = $tvc_add_potential_customer_F1["prospect"];
                                $msg = 'Đã thêm Khách hàng F1 tiềm năng trên Tavico: '.$tvc_potential_code_F1;
                                $logs[] = LogController::logBill('[F1-PotentialCustomer]' . $msg, 'info', $id, false, false);
                            }
                        }
                        // Update P.Customer F1
                        UserCustomerMapping::where([
                            'id_user'       => $query->staff_id,
                            'id_customer'   => $query->customer_id
                        ])->update(['potential_reference_code_F1' => $tvc_potential_code_F1]);
                        if (!$query->bill_reference_code_F1) {
                            /** Add F1 Bill request */
                            $logs[] = LogController::logBill('[F1-Bill Init] Đang khởi tạo phiếu F1 trên Tavico. KHTN: ' . $tvc_potential_code_F1, 'info', $id, false, false);
                            $tvc_add_bill_F1 = APIController::TVC_BILL_CREATE(
                                $query->product ? $query->product->code : null,
                                $tvc_potential_code_F1,
                                $query->product->stage,
                                $cmConfigF1AccountCode[$crm_product_manager][$branch],
                                "",
                                null,
                                null,
                                false
                            );
                            if (!$tvc_add_bill_F1 || !$tvc_add_bill_F1['systemno']) {
                                $logs[] = LogController::logTransaction('[F1-Bill] Tạo phiếu Tavico F1 không thành công!', 'warning', $query->transaction->trans_id, false, false);
                                return redirect('bill')->with('status', 'Xảy ra lỗi khi tạo phiếu yêu cầu xuống F1 Tavico');
                            } else {
                                $query->bill_reference_code_F1 = $tvc_add_bill_F1['systemno'];
                                $logs[] = LogController::logTransaction('[F1-Bill] Tạo phiếu Tavico F1 thành công: ' . $query->bill_reference_code_F1, 'info', $query->transaction->trans_id, false, false);
                            };
                        } else {
                            $logs[] = LogController::logBill('[F1-Bill Init] Đã tồn tại phiếu F1 trên Tavico: '.$query->bill_reference_code_F1.'. KHTN: ' . $tvc_potential_code_F1, 'info', $id, false, false);
                        }
                    }else {
                        $logs[] = LogController::logBill('[STATUS_APPROVED] TKKD duyệt HĐ F2!', 'info', $id, false, false);
                    }

                    /** todo: TKKD bỏ qua các bước duyệt */
                    if ($old_procedure != null) {
                        $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] TKKD duyệt HĐ theo quy trình cũ!', 'info', $id, false, false);
                        /** Save tvc contract */
                        if($query->type == BOBill::TYPE_DEPOSIT) {
                            /** todo: F1 Contract */
                            if($crm_check == 0){
                                $logs[] = LogController::logBill('Web - [STATUS_APPROVED-stepsAborted] Ghi GD F1 theo quy trình cũ!', 'info', $id, false, false);
                                if (!$query->contract_reference_code_F1) {
                                    $tvc_contract_F1 = APIController::TVC_CONTRACT_CREATE_F1($query, 0, $crm_product_manager, $cmConfigF1AccountCode[$crm_product_manager][$branch], false);
                                    if (!$tvc_contract_F1['success'] || !$tvc_contract_F1['data']) {
                                        /** Save F1 bill request */
                                        $query->save();
                                        return redirect('bill')->with('status', $tvc_contract_F1['msg'] ?? 'Không thể tạo GD F1 trên Tavico');
                                    }
                                    $query->contract_reference_code_F1 = $tvc_contract_F1['data'] ?? null;
                                } else {
                                    $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] F1 đã tồn tại giao dịch Tavico! '.$query->contract_reference_code_F1, 'info', $id, false, false);
                                }
                            }
                            /** @var $tvc_booking_status */
                            $tvc_booking_status = 'XNDCO';
                            if (!$query->contract_reference_code) {
                                /** @var $tvc_contract */
                                $tvc_contract = APIController::TVC_CONTRACT_CREATE($query, $branch_check, $branch, false);
                                if (!$tvc_contract['success'] || !$tvc_contract['data']) {
                                    /** Save F1 contract */
                                    $query->save();
                                    $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Không thể tạo GD trên F2 Tavico', 'error', $id, false, false);
                                    return redirect('bill')->with('status', $tvc_contract_F1['msg'] ?? 'Không thể tạo GD trên F2 Tavico');
                                }
                                $query->contract_reference_code = $tvc_contract['data'];
                            }
                        }else{
                            $tvc_booking_status = 'XNDCH';
                        }
                        /**  UPDATE BOOKING TVC F1 */
                        if($crm_check == 0){
                            if (!$query->bill_reference_code_F1) {
                                $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Chưa có Phiếu Tavico F1 để cập nhật!', 'error', $id, false, false);
                                return redirect('bill')->with('status', 'Không tồn tại phiếu F1 Tavico cho HĐ: '.$query->bill_code);
                            }
                            $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Cập nhật trạng thái Phiếu F1 Tavico: '.$query->bill_reference_code_F1, 'info', $id, false, false);
                            APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code_F1, $tvc_booking_status, $id, 0, $crm_product_manager, false);
                        }
                        /**  UPDATE BOOKING TVC F2 */
                        if (!$query->bill_reference_code) {
                            $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Chưa có Phiếu Tavico F2 để cập nhật!', 'error', $id, false, false);
                            return redirect('bill')->with('status', 'Không tồn tại phiếu Tavico cho HĐ: '.$query->bill_code);
                        }
                        $logs[] = LogController::logBill('[STATUS_APPROVED-stepsAborted] Cập nhật trạng thái Phiếu F2 Tavico: '.$query->bill_reference_code, 'info', $id, false, false);
                        APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, $tvc_booking_status, $id, $branch_check, $branch, false);
                        /** todo: Update apartment status & trigger pusher */
                        BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['CDDCO'], true, false, false);
                        if ($query->transaction) {
                            $payment = new BOPayment();
                            $payment->{BOPayment::ID_KEY} = null;
                            $payment->pb_status = BOPayment::STATUS['PENDING'];
                            $payment->pb_code = null;
                            $payment->reference_code = null;
                            $payment->bill_code = $query->bill_code;
                            $payment->pb_note = "Y/C thanh toán cho HĐ theo quy trình cũ";
                            $payment->customer_id = $query->customer_id;
                            $payment->request_staff_id = $query->staff_id;
                            $payment->request_time = now();
                            $payment->pb_request_money = (double) $query->transaction->deposit_amount;
                            $payment->method_id = 0;
                            $payment->pb_bank_number_id = null;
                            $payment->type = $query->type != BOBill::TYPE_DEPOSIT ? 2 : BOBill::TYPE_DEPOSIT ;
                            if ($payment->save()) {
                                $logs[] = LogController::logPayment('[BO] Đã tạo Y/C thanh toán cho HĐ theo quy trình cũ: '.$query->bill_code, 'info', $payment->{BOPayment::ID_KEY}, false, false);
                            } else {
                                $logs[] = LogController::logPayment('[BO] Xảy ra lỗi khi tạo Y/C thanh toán cho HĐ theo quy trình cũ: '.$query->bill_code, 'warning', null, false, false);
                            }
                        }
                        $status = BOBill::STATUSES['STATUS_PAYING'];
                        $query->bill_note = ($query->bill_note?? '') . ' - HĐ được duyệt theo quy trình cũ';
                    }
                    break;
                case BOBill::STATUSES['STATUS_BOOKED']:
                    /** Đặt chỗ */
                    if (!AuthController::is_cs_staff(false)) return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
                    /** todo: Update apartment status & trigger pusher */
                    BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['DCH'], true , false , false);
                    ///Send mail///
                    MailController::customerStatusBill($query,BOBill::STATUSES['STATUS_BOOKED']);
                    break;
                case BOBill::STATUSES['STATUS_DEPOSITED']:
                    /** Xác nhận Đặt cọc || Chuyển chỗ lên cọc trực tiếp */
                    if ($query->type == BOBill::TYPE_BOOK_ONLY)  return redirect('bill')->with('status', 'HĐ đặt chỗ không thể chuyển cọc trực tiếp!');
                    if (!$query->product || $query->product->stage!=BOProduct::STAGE_DEPOSIT)  return redirect('bill')->with('status', 'Căn hộ chưa cho phép đặt cọc!');
                    if ($query->status_id == BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST']) {
                        /** TKKD xác nhận chuyển cọc trực tiếp */
                        if (!AuthController::is_product_manager(false))  return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
                        // chuyển tình trạng phieus XNDCH => XNDCO, và tạo giao dịch.
                        APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, 'XNDCO', $id, $branch_check, $branch, false);
                        $tvc_contract = APIController::TVC_CONTRACT_CREATE($query, $branch_check, $branch, false);
                        if (!$tvc_contract['success']) return redirect('bill')->with('status', 'Không thể tạo HĐ trên Tavico', $tvc_contract);
                        $query->type = BOBill::TYPE_DEPOSIT;
                        $query->contract_reference_code = $tvc_contract['data'];

//                        $oldRequiredMoney = $query->bill_required_money;
//                        $newRequiredMoney = $query->product? $query->product->pb_required_money : 0;
//                        ///Nếu số tiền đặt chỗ = số tiền đặt cọc mới thì sẽ update payment cũ trên tavico///
//                        if($oldRequiredMoney == $newRequiredMoney && $newRequiredMoney != 0){
//                            ///Update Tavico Payment///
//                            PaymentController::createPaymentTvc($payment->toArray(),BOPayment::TYPE_PAYMENT_TAVICO['CANCEL_DCHO'],$query);
//                        }

                        // check thu du tien hay chua
                        $payments = BOPayment::where('bill_code', $query->bill_code)->get();
                        $paid = 0;
                        foreach($payments as $item){
                            $paid += (int) $item->pb_response_money;
                        }
                        $compareMoney = $paid > (int)$query->bill_required_money ? $paid : (int)$query->bill_required_money;
                        $status =  $compareMoney < (int)$query->product->pb_required_money ? BOBill::STATUSES['STATUS_PAYING'] : BOBill::STATUSES['STATUS_PAID'];
                        $current_required_money = $query->bill_required_money;
                        $query->bill_required_money = $query->product? $query->product->pb_required_money : null;
                        $edit_log = [
                            self::generateEditLog('type', $query->type, BOBill::TYPE_DEPOSIT, 0, false),
                            self::generateEditLog('bill_required_money', $current_required_money, $query->bill_required_money, 0, false),
                        ];
                        $query->bill_log_edit = is_array($query->bill_log_edit)? array_merge($query->bill_log_edit, $edit_log) : $edit_log;
                        ///Send mail///
                        MailController::customerContract($query, BOBill::STATUSES['STATUS_DEPOSITED'], false);

                        /** todo: Update apartment status & trigger pusher */
                        BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['CDDCO'], true , false , false);
                    } elseif ($query->status_id == BOBill::STATUSES['STATUS_PAID']) {
                        /** DVKH xác nhận HĐ đặt cọc */
                        if (!AuthController::is_cs_staff(false)) return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
                        APIController::TVC_CONTRACT_DEPOSITED($query->contract_reference_code, null, $branch_check, $branch, $query->product->code, false);
                        ///Send mail///
                        MailController::customerStatusBill($query, BOBill::STATUSES['STATUS_DEPOSITED']);

                        /** todo: Update apartment status & trigger pusher */
                        BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['DCO'], true , false , false);
                    } else {
                        return redirect('bill')->with('status', 'Yêu cầu không hợp lệ!');
                    }
                    break;
                case BOBill::STATUSES['STATUS_BOOK_TO_DEPOSIT']:
                    /** TKKD Xác nhận Huỷ chỗ - cọc mới */
                    if ($query->type != BOBill::TYPE_BOOK_ONLY || $query->status_id!=BOBill::STATUSES['BOOK_TO_DEPOSIT_REQUEST']) return redirect('bill')->with('status', 'Tình trạng hợp đồng không hợp lệ!');
                    if (!AuthController::is_product_manager(false)) return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
                    if (!$query->product || $query->product->stage!=BOProduct::STAGE_DEPOSIT) return redirect('bill')->with('status', 'Căn hộ chưa cho phép đặt cọc!');

                    /*******************************************
                     * 1. TVC UPDATE OLD BILL & CREATE NEW ONE
                     *******************************************/
                    // chuyển phiếu cũ thành XNHDCH, Tao phieu moi XNDCO, va tao giao dich
                    APIController::TVC_UPDATE_BOOKING_STATUS($query->bill_reference_code, 'XNHDCH', $id, $branch_check, $branch, false);
                    // tạo phiêu yêu cầu mới
                    $customer = UserCustomerMapping::where([
                        'id_user'       => $query->staff_id,
                        'id_customer'   => $query->customer_id
                    ])->first();
                    if (!$customer) return redirect('bill')->with('status', 'Không tìm thấy thông tin khách hàng!');
                    /** @var $tvc_potential_code */
                    $tvc_potential_code = $customer->potential_reference_code;
                    /** @var $product_code */
                    $product_code = $query->product? $query->product->code : null;
                    if (!$product_code) return redirect('bill')->with('status', 'Không tìm thấy thông tin căn hộ!');
                    if (!$staff || !$staff->ub_tvc_code) return redirect('bill')->with('status', 'Không tìm thấy thông tin nhân viên!');
                    /** @var $note */
                    $note = "Hủy chỗ, tạo phiếu cọc mới";
                    /** @var $tvc_add_bill */
                    $tvc_add_bill = APIController::TVC_BILL_CREATE($product_code, $tvc_potential_code, 3, $staff->ub_tvc_code, $note, null, null, false);
                    if (!$tvc_add_bill) {
                        return redirect('bill')->with('status', 'Xảy ra lỗi khi tạo GD trên Tavico!');
                    }

                    /*************************
                     * 2. BO ClONE NEW BILL
                     **************************/
                    $new_bill_id = time();
                    $new_bill = $query->replicate();
                    $new_bill->{BOBill::ID_KEY} = $new_bill_id;
                    $new_bill->bill_code = BOBill::generateBillCode(BOBill::TYPE_DEPOSIT, $query->product);
                    $new_bill->bill_title = null;
                    /** @var $paid */
                    $paid = 0;
                    /** check Payment status */
                    if ($query->product) {
                        $new_bill->bill_total_money = $query->product->pb_price_total;
                        $new_bill->bill_required_money = $query->product->pb_required_money;
                        /** @var $payments */
                        $paid += BOPayment::where(['bill_code' => $query->bill_code, 'pb_status' => BOPayment::STATUS['APPROVED']])
                            ->where('pb_response_money', '>', 0)
                            ->sum('pb_response_money');
                        $new_bill->status_id = $new_bill->bill_required_money && $new_bill->bill_required_money <= $paid?
                            BOBill::STATUSES['STATUS_PAID'] : BOBill::STATUSES['STATUS_PAYING'];
                    } else {
                        $new_bill->status_id = BOBill::STATUSES['STATUS_PAYING'];
                    }
                    $new_bill->bill_created_time = now()->addSeconds(2);
                    $new_bill->bill_updated_time = now()->addSeconds(2);
                    $new_bill->type = BOBill::TYPE_DEPOSIT;

                    ///Save Log Manager approve///
                    $logsQuery = $query->bill_approval_log;
                    $bill_approval_log = array();
                    foreach ($logsQuery as $log){
                        if($log['VALUE'] == BOBill::STATUSES['STATUS_APPROVED_BY_MANAGER']){
                            $bill_approval_log[] = $log;
                        }
                    }
                    $bill_approval_log[] = BOBill::generateApprovalLog(BOBill::STATUSES['STATUS_PAID_ONCE'], 0 , false);

                    $new_bill->bill_approval_log = $bill_approval_log;
                    $new_bill->bill_reference_code = $tvc_add_bill&&$tvc_add_bill['systemno']? $tvc_add_bill['systemno'] : null;
//                    unset($new_bill->total_money);
//                    unset($new_bill->required_money);
                    /** @var $tvc_contract */
                    $tvc_contract = APIController::TVC_CONTRACT_CREATE($new_bill, $branch_check, $branch, false);
                    if (!$tvc_contract['success']) return redirect('bill')->with('status', 'Không thể tạo HĐ trên Tavico', $tvc_contract);
                    $new_bill->contract_reference_code = $tvc_contract['data']?? null;
                    $new_bill->bill_reference_code = $tvc_add_bill&&$tvc_add_bill['systemno']? $tvc_add_bill['systemno'] : null;
                    if ($new_bill->save()) {
                        LogController::logBill('Web - Chuyển HĐ chỗ sang cọc mới thành công: '.$new_bill->bill_code, 'info', $new_bill_id, true, false);
                        $edit_log = [self::generateEditLog('type', $query->type, BOBill::TYPE_DEPOSIT, 0, false)];
                        $query->bill_log_edit = is_array($query->bill_log_edit)? array_merge($query->bill_log_edit, $edit_log) : $edit_log;
                        /** Generate first payment */
                        $payment = new BOPayment();
                        $payment->pb_id = null;
                        $payment->pb_status = env('STATUS_ACTIVE', 1);
                        $payment->pb_code = null;
                        $payment->bill_code = $new_bill->bill_code;
                        $payment->pb_note = "HĐ chỗ sang cọc mới: TT lần đầu tự động";
                        $payment->customer_id = $new_bill->customer_id;
                        $payment->request_staff_id = $new_bill->staff_id;
                        $payment->request_time = now();
                        $payment->pb_request_money = $paid;
                        $payment->pb_response_money = $paid;
                        $payment->response_staff_id = Auth::guard('loginfrontend')->user()->ub_id;
                        $payment->pb_response_time = now();
                        $payment->type = $new_bill->type != BOBill::TYPE_DEPOSIT ? 2 : BOBill::TYPE_DEPOSIT ;
                        if (!$payment->save()) {
                            LogController::logPayment('Web-Không thể tạo thanh toán tự động cho HĐ chuyển chỗ sang cọc mới: '.$query->bill_code, 'warning' ,null, false, false);
                        }
                        ///Create Tavico Payment///
                        // BOPaymentController::createPaymentTvc($payment->toArray(),BOPayment::TYPE_PAYMENT_TAVICO['CANCEL_DCHO'],$query);
                        // $referenceCode = BOPaymentController::createPaymentTvc($payment->toArray(),BOPayment::TYPE_PAYMENT_TAVICO['DCO'],$new_bill);
                        // $payment->reference_code = $referenceCode;
                        /** Tạo phiếu chi **/
                        $spending = new BOPayment();
                        $spending->pb_id = $payment->pb_id+1;
                        $spending->pb_status = env('STATUS_ACTIVE', 1);
                        $spending->pb_code = null;
                        $spending->bill_code = $query->bill_code;
                        $spending->pb_note = "HĐ chỗ sang cọc mới: Tạo phiếu chi tự động";
                        $spending->customer_id = $query->customer_id;
                        $spending->request_staff_id = $query->staff_id;
                        $spending->request_time = now();
                        $spending->pb_request_money = $paid;
                        $spending->pb_response_money = $paid;
                        $spending->response_staff_id = Auth::guard('loginfrontend')->user()->ub_id;
                        $spending->pb_response_time = now();
                        $spending->type = $new_bill->type != BOBill::TYPE_DEPOSIT ? 2 : BOBill::TYPE_DEPOSIT ;
                        $spending->type_of_payment = 'D' ;
                        if (!$spending->save()) {
                            LogController::logPayment('Web-Không thể tạo phiếu chi tự động cho HĐ chuyển chỗ sang cọc mới: '.$query->bill_code, 'warning', null, false, false);
                        }
                        $msg = 'Đã chuyển chỗ sang cọc, hợp đồng mới: ' . $new_bill->bill_code;
                        //Send mail///
                        MailController::customerContract($new_bill, BOBill::STATUSES['STATUS_BOOK_TO_DEPOSIT'], false);
                    } else {
                        LogController::logBill('Web - Không thể chuyển HĐ chỗ sang cọc mới: '. $query->bill_code, 'error', $id, true, false);
                        return redirect('bill')->with('status', "Không thể chuyển HĐ chỗ sang cọc mới", $query);
                    }
                    break;
                case BOBill::STATUSES['STATUS_SUCCESS']:
                    /** TKKD xác nhận hoàn thành HĐ */
                    if (!AuthController::is_product_manager(false)) return redirect('bill')->with('status', 'Bạn không có quyền thực hiện thao tác này!');
//                    APIController::TVC_PRODUCT_MANAGER_UPDATE_CONTRACT_STATUS_CDGDTC($query->contract_reference_code, $query->product->code, $branch_check, $id, $branch, false);
                    $query->bill_completed_time = now();
                    ///Send mail///
                    MailController::customerStatusBill($query, BOBill::STATUSES['STATUS_SUCCESS']);
                    BOProductController::changeStatus($query->product_id, BOProduct::STATUSES['HDO'], true, false , false);
                    break;
                default:
            }
            $current_log = $query->bill_approval_log;
            $current_log[] = BOBill::generateApprovalLog((int) $status, 0, false);
            $query->status_id = (int) $status;
            $query->bill_approval_log = $current_log;
            if ($query->save()) {
                NotificationController::notifyBillStatusUpdated($query, [], true, false);
                return redirect('bill')->with('status', $msg);
            } else {
                return redirect('bill')->with('status', "Không thành công");
            }

        } else {
            return redirect('bill')->with('status','Dữ liệu không hợp lệ', ['item' => $id]);
        }
    }
}
