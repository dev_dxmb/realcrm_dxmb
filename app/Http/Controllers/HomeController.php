<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\BOUser;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $o_UserInbo;
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return array
     */
    public function index()
    {
        $data = Category::all();
        return $data;
//        return view('home');
    }

    public function vnpay() {

        return view('test.vnpay');
    }
    public function vnpay_submit(Request $request) {
        $form_data = $request->all();
        $vnp_TxnRef = $form_data['order_id']; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = $form_data['order_desc'];
        $vnp_OrderType = $form_data['order_type'];
        $vnp_Amount = $form_data['amount'] * 100;
        $vnp_Locale = $form_data['language'];
        $vnp_BankCode = $form_data['bank_code'];
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => self::$vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => route('vnpay-response'),
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hash_data = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hash_data .= '&' . $key . "=" . $value;
            } else {
                $hash_data .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = self::$vnp_Url . "?" . $query;
        if (isset(self::$vnp_HashSecret)) {
            $vnpSecureHash = md5(self::$vnp_HashSecret . $hash_data);
            $vnp_Url .= 'vnp_SecureHashType=MD5&vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array(
            'code' => '00',
            'message' => 'success',
            'data' => $vnp_Url,
            'debug' => $inputData
        );

        return $returnData;
    }
    
    
}
