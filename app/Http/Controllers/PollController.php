<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\Poll;
use App\BOUser;
use App\BORoleGroup;

class PollController extends Controller
{
    private $o_Poll;
    public function __construct() {
        $this->o_Poll = new Poll();
    }
    public function getAllPoll(){
        $a_Data = $this->o_Poll->getAllSearch();        
        
        $Data_view['a_Poll'] = $a_Data['a_data'];
        $Data_view['a_search'] = $a_Data['a_search'];        
        return view('customerNotice.index',$Data_view);
        
    }
    
    /**
     * @auth: Dienct
     * @since: 05/09/2018
     * @Des: add, edit customer
     */
    public function addEditPoll(){
        $a_DataView = array();
        $PollId = (int) Input::get('id', 0);
        $a_DataView['i_id'] = $PollId;
        $checksubmit = Input::get('submit');
        if (isset($checksubmit) && $checksubmit != "") {
            $this->o_Poll->AddEditPoll($PollId);
                return redirect('customer_notice/list')->with('status', 'Cập nhật thành công!');
        }
        
        $a_DataView['type_news'] = config('cmconst.type_news');
        $a_DataView['category_news'] = config('cmconst.category_news');
        $a_DataView['PollData'] = $PollId != 0 ? $this->o_Poll->getPollById($PollId) : array();
        
        // get all user
        $a_User = BOUser::getAllUserInBo();
        $a_DataView['a_Users'] = $a_User;
        
         
        // get all groups
        $a_userGroups = BORoleGroup::getAllRoleGroups();
        $a_DataView['a_userGroups'] = $a_userGroups;

        return view('poll.edit', $a_DataView );
    }
}
