<?php

namespace App\Http\Controllers;
use App\BankInfo;
use App\BOPayment;
use App\BOProduct;
use App\BOTransaction;
use App\Http\Controllers\API\BOProductController;
use App\TemplateEmail;
use DB;
use Faker\Provider\fr_CH\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\BOCategory;
use App\BOBill;
use App\BOUser;
use App\Http\Controllers\API\LogController;
use Illuminate\Support\Facades\Auth;
class AjaxController extends Controller
{
    private $i_id;
    private $i_type;
    private $sz_func;
    private $sz_tbl;
    private $sz_field;
    private $sz_val;
    private $o_LeaveRequestModel;
    protected $_o_MailApi;


    /**
     * function __Contruct    
     */
    public function __construct() {

        $this->i_id = Input::get('id',0);
        $this->i_type = Input::get('type',0);
        $this->sz_func = Input::get('func');
        $this->sz_tbl = Input::get('tbl');
        $this->sz_field = Input::get('field');
        $this->sz_val = Input::get('val');
    }
    
    public function SetProcess(){
        if($this->sz_func == "") exit;
        switch ($this->sz_func) {
            case "delete-row":
                $this->DeleteRow();
                break;
            case "recover-row":
                $this->RecoverRow();
                break;
            case "save-session-job-statistics":
                $this->SaveSessionStatistics();
                break;
            case "add-customer-info":
                $this->addCustomerData();
                break;
            case "transfer-data":
                $this->transferDataCustomer();
                break;
            case "transfer-data-tmp":
                $this->transferTMPDataCustomer();
                break;
            case "get-objectId":
                $this->getAllObjectId();
                break;
            case "get-bill-info":
                $this->getbillInfo();
                break;
            case "loadBuildingByProjectId":
                $this->loadBuildingByProjectId();
                break;
            case "loadApartmentByBuildingId":
                $this->loadApartmentByBuildingId();
                break;
            case "loadBillByApartmentId":
                $this->loadBillByApartmentId();
                break;
            case "loadPayments":
                $this->loadPayments();
                break;
            case "login-tvc":
                $this->loginTVC();
                break;
//            case "confirm-payment":
//                $this->confirmPayment();
//                break;
            case "loadBankInfo":
                $this->loadBankInfo();
                break;
            case "checkTemplateDefaultEmail":
                $this->checkTemplateDefaultEmail();
                break;
            case "showDetailApartment":
                $this->showDetailApartment();
                break;
            case "updateDetailApartment":
                $this->updateDetailApartment();
                break;
            case "getParamDatatable":
                $this->getParamDatatable();
                break;
            case "paymentDeny":
                $this->paymentDeny();
                break;
            case "showImgBill":
                $this->showImgBill();
                break;
            case "showDetailPayment":
                $this->showDetailPayment();
                break;
//            case "test":
//                $this->test();
//                break;
            default:
                break;
        }
    }
    protected function showDetailPayment(){
        $paymentId = Input::get('paymentId');
        $payment = BOPayment::where("id", $paymentId)
            ->with([
            'responseStaff' => function($responseStaff) {
                $responseStaff->select([
                    'ub_id','ub_title'
                ]);
            },
            'bankInfo' => function($bankInfo) {
                $bankInfo->select([
                    'bank_id','bank_code','bank_title' ,'bank_holder', 'bank_number'
                ]);
            },
            'paymentMethod' => function($paymentMethod) {
                $paymentMethod->select([
                    'pm_id','pm_title'
                ]);
            },
        ])->first();
        echo json_encode($payment);
    }
    protected function showImgBill(){
        $billId = Input::get('billId');
        $field = Input::get('showType');
        $bill = BOBill::find($billId);
        $arrImg = $bill->media;
        if($field != 'media'){
            $arrImg = $bill->bill_customer_info->images;
        }
        echo json_encode($arrImg);
    }
    protected function paymentDeny(){
        $res = array(
            'alert' => 'Cập nhật thành công!',
            'result' => 1
        );
        $paymentId = Input::get('paymentId');
        $payment = BOPayment::find($paymentId);

        if(!$payment){
            $res = array(
                'alert' => 'Không tìm thấy Payment!',
                'result' => 0
            );
            echo json_encode($res) ;
            return false;
        }
        if($payment->pb_status != 0){
            $res = array(
                'alert' => 'Trạng thái hiện tại của Payment không hợp lệ nên không thể từ chối!',
                'result' => 0
            );
            echo json_encode($res) ;
            return false;
        }

        $payment->pb_status = -1;
        $payment->response_staff_id = Auth::guard('loginfrontend')->user()->ub_id; // ke toan thu tien
        if(!$payment->save()){
            $res = array(
                'alert' => 'Cập nhật thất bại!',
                'result' => 0
            );
            echo json_encode($res) ;
            return false;
        }
        echo json_encode($res) ;
    }


//    protected function test(){
//        $colum = array('product.pb_code','bank_title','bank_code','bank_holder','bank_number');
//        $arrGet = Input::get();
//        $data = Input::get('data');
//        $perPage = $arrGet['length']; // Số bản ghi 1 trang
//        $start = $arrGet['start']; // Vị trí bắt đầu
//        $getOrder = $arrGet['order'][0]['column'];
//        $totalTrans = BOTransaction::count();
//        $allTrans = BOTransaction::select('*')->with(
//            [
//                "staff" => function($staff) {
//                    return $staff->select(["ub_id", "ub_account_name AS account", "ub_title", "group_ids"])
//                        ->with(["group" => function($group) {
//                            return $group->select(['gb_id', 'gb_title AS name']);
//                        }]);
//                },
//                "product" => function($product)  {
//                    return $product->select([
//                        "pb_id",
//                        "pb_title AS apartment", "pb_code AS code",
//                        "category_id",
//                        "pb_required_money AS required_money"
//                    ])->with([ "category" => function($category) {
//                        return $category->select([
//                            "id", "cb_id", "parent_id", "cb_title AS building"
//                        ])->where("cb_level", 2)
//                            ->with(["sibling" => function($project){
//                                return $project->select(["id", "cb_id", "parent_id", "cb_title AS project"])->where("cb_level", 1);
//                            }]);
//                    }]);
//                }
//            ]
//        )->orderBy('trans_created_time','desc')->skip($start)->take($perPage)->get();
//        if($allTrans){
//            $data = array();
//            foreach ($allTrans as $index => $trans){
//                $data[] = array(
//                    $start + $index + 1,
//                    '
//                        <p><strong>Mã căn hộ: </strong>' .$trans->product->code .'</p>
//                        <p><strong>Tòa: </strong>'. $trans->product->category->building. '</p>
//                        <p><strong>Dự án: </strong>'. $trans->product->category->sibling->project.'</p>
//                    ',
//                    '
//                        <p><strong>Họ tên: </strong>'. $trans->staff->ub_title .'</p>
//                        <p><strong>Phòng ban: </strong>'.($trans->staff->group != NULL ? $trans->staff->group->name : '') .'</p>
//                    ',
//                    date('d-m-y H:i:s',strtotime($trans->trans_created_time)),
//                    $trans->trans_title
//                );
//            }
//        }
//        $res = array(
//            'recordsTotal' => $totalTrans,
//            'recordsFiltered' => $totalTrans,
//            'data' => $data
//        );
//        echo json_encode($res);
//    }
    protected function getParamDatatable(){
        $arrGet = Input::get();
        $table = $arrGet['table'];
        if($table == 'bank_infos'){
            $colum = array('id','bank_title','bank_code','bank_holder','bank_number');
        }
        $perPage = $arrGet['length']; // Số bản ghi 1 trang
        $start = $arrGet['start']; // Vị trí bắt đầu
        $getOrder = $arrGet['order'][0]['column'];
        $fieldOrder = $colum[$getOrder];// field order
        $criteria = $arrGet['order'][0]['dir']; // Tiêu chí order
        $totalBank = BankInfo::count();
        $allBank = BankInfo::orderBy($fieldOrder,$criteria)->where('bank_status', '!=', -1)->skip($start)->take($perPage)->get();
        if($allBank){
            $data = array();
            foreach ($allBank as $index => $bank){
                $data[] = array(
                    $start + $index + 1,
                    $bank->bank_title,
                    $bank->bank_code,
                    $bank->bank_holder,
                    $bank->bank_number,
                    'cscsc'
                );
            }
        }
        $res = array(
            'recordsTotal' => $totalBank,
            'recordsFiltered' => $totalBank,
            'data' => $data
        );
        echo json_encode($res);
    }
    protected function updateDetailApartment(){
        $data = Input::get('data');
        $product = BOProduct::where('pb_id',$data['productId'])->first();
        if($product){
            $product->stage = $data['stage'];
            $product->status_id = $data['status'];
            $product->pb_status = $data['pb_status'];
            $product->book_type = $data['book_type'];
            $product->pb_used_s = $data['pb_used_s'];
            $product->pb_built_up_s = $data['pb_built_up_s'];
            $product->pb_door_direction = $data['pb_door_direction'];
            $product->pb_balcony_direction = $data['pb_balcony_direction'];
            $product->view = $data['view'];
            $product->price_used_s =  $data['price_used_s'] != '' ? str_replace(' ','',$data['price_used_s']) : Null;
            $product->pb_price_per_s = $data['pb_price_per_s'] != '' ?  str_replace(' ','',$data['pb_price_per_s']) : Null;
            $product->price_min = $data['price_min'] != '' ? str_replace(' ','',$data['price_min']) : Null;
            $product->price_max = $data['price_max'] != ''  ? str_replace(' ','',$data['price_max']): Null;
            $product->price_maintenance = $data['price_maintenance'] != '' ? str_replace(' ','',$data['price_maintenance']): Null;
            $product->pb_price_total = $data['pb_price_total'] != '' ?  str_replace(' ','',$data['pb_price_total']): Null;
            $product->number_lock = $data['number_lock'];
            $product->pb_required_money = $data['pb_required_money'] != '' ?  str_replace(' ','',$data['pb_required_money']): Null;
            $product->note = $data['note'];
            $product->updated_user_id = Auth::guard('loginfrontend')->user()->ub_id;
            $product->ub_updated_time = date('Y-m-d H:i:s', time());
            // Login and update Product.
            $config_account = config('cmconst.account_CRM');
            foreach ($config_account as $key => $val){
                $product_exists = API\APIController::TVC_CHECK_EXIST_PRODUCT($product->pb_code, $val['username']);
                if ($product_exists != 0 && $product_exists != '') {
                    $apartment_data = [
                        'property' => $product->pb_code,
                        "block" => (string) substr($product->pb_code, 0, 6),
                        "area" => $product->pb_built_up_s ? $product->pb_built_up_s : 0,
                        "value1" => $product->price_used_s ? $product->price_used_s : 0
                    ];
                    switch ($data['status']) {
                        case 0:
                            $statusTVC = 'MBA';
                            break;
                        case -4:
                            $statusTVC = 'DCH';
                            break;
                        case 4:
                            $statusTVC = 'CDDCO';
                            break;
                        case 5:
                            $statusTVC = 'DCO';
                            break;
                        case 6:
                            $statusTVC = 'HDO';
                            break;
                        default:
                            $statusTVC = false;
                    }
                    if($statusTVC) $apartment_data['status'] = $statusTVC;
                    
                    $new_apartment = API\APIController::TVC_UPDATE_PRODUCT($apartment_data, 'upd', $val['username']);
                    if (!$new_apartment) {
                        LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Đồng bộ căn hộ '.$product->pb_code. ' không thành công. TK: '.$val['username'], 'error', null);
                        return false;
                    } else {
                        LogController::logTransaction('[TVC_SYNC_BRANCH_ITEMS] Đã bộ căn hộ '.$product->pb_code. '. TK: '.$val['username'], 'info', null);
                    }
                    
                    $aryInsertPriceBooking = $aryUpdatePriceBooking = array();
                    // array update price
                    $aryUpdatePriceBooking = [];
                    $dataPrice = API\APIController::TVC_LIST_PRICEBOOKING($product->pb_code, $val['username']);
                    if (is_array($dataPrice) && count($dataPrice) > 0) {
                        foreach ($dataPrice as $val1) {
                            $aryUpdatePrice['pricebookid'] = $val1['pricebookid'];
                            $aryUpdatePrice['status'] = 'C';
                            $aryUpdatePriceBooking[] = $aryUpdatePrice;
                        }
                    }

                    // add price booking id
                    $aryInsertPriceBooking[] = [
                        "pricebookid" => "",
                        "pymtterm" => "z",
                        "status" => "W",
                        "property" => $product->pb_code,
                        "startdate" => "",
                        "enddate" => "",
                        "notes" => "",
                        "propertyvalue" => isset($product->pb_price_total) ? $product->pb_price_total : 0,
                        "value0" => 0,
                        "value1" => 0,
                        "value2" => isset($product->price_maintenance) ? $product->price_maintenance: 0,
                        "value3" => 0,
                        "value4" => "",
                        "value5" => 0,
                        "value6" => isset($product->pb_price_per_s) ? $product->pb_price_per_s : 0,
                        "value7" => isset($product->pb_required_money) ? $product->pb_required_money : 0,
                        "value8" => isset($product->price_min) ? $product->price_min : 0,
                        "value9" => isset($product->price_max) ? $product->price_max : 0,
                    ];
                    if ($aryUpdatePriceBooking && count($aryUpdatePriceBooking) > 0) {
                        API\APIController::TVC_UPD_PRICEBOOKING($aryUpdatePriceBooking, $val['username']);
                    }
                    if ($aryInsertPriceBooking && count($aryInsertPriceBooking) > 0) {
                        API\APIController::TVC_ADD_PRICEBOOKING($aryInsertPriceBooking, $val['username']);
                    }
                }
            }

            if($product->save()){
//                BOProductController::changeStatus($data['productId'], $data['status'], true);
                echo json_encode(array(
                    'alert' => 'Cập nhật dữ liệu thành công!',
                    'result' => 1
                ));
            }else{
                echo json_encode(array(
                    'alert' => 'Không thể cập nhật dữ liệu!',
                    'result' => 0
                ));
            }
        }
    }
    protected function showDetailApartment(){
        $arrProjects = BOProductController::getProjectsByUserRoleWeb('approve');
        $arrApartment = BOProductController::getApartmentsByProject($arrProjects);
        $productId = Input::get('productId');
        $apartment = BOProduct::where('pb_id',$productId)->first();
        $bill = BOBill::where('product_id',$productId)->whereNotIn('status_id', [
            BOBill::STATUSES['STATUS_DISAPPROVED'],
            BOBill::STATUSES['STATUS_CANCELED'],
        ])->first();
        if($apartment){
            $apartment->price_used_s = $apartment->price_used_s != Null ? number_format($apartment->price_used_s,0,'.',' ') : '';
            $apartment->pb_price_per_s = $apartment->pb_price_per_s != Null ? number_format($apartment->pb_price_per_s,0,'.',' ') : '';
            $apartment->price_min = $apartment->price_min != Null ? number_format($apartment->price_min,0,'.',' ') : '';
            $apartment->price_max = $apartment->price_max != Null ? number_format($apartment->price_max,0,'.',' ') : '';
            $apartment->price_maintenance = $apartment->price_maintenance != Null ? number_format($apartment->price_maintenance,0,'.',' ') : '';
            $apartment->pb_price_total = $apartment->pb_price_total != Null ? number_format($apartment->pb_price_total,0,'.',' ') : '';
            $apartment->pb_required_money = $apartment->pb_required_money != Null ? number_format($apartment->pb_required_money,0,'.',' ') : '';
            $apartment->disabledOptionStatus = $bill ? true : false;
            $showTrans = array();
            if(in_array($apartment->status_id,[BOProduct::STATUSES['CHLOCK'],BOProduct::STATUSES['LOCKED']])){
                $trans = $trans = BOTransaction::where('product_id',$productId)
                    ->whereNotIn('trans_code', [BOTransaction::STATUS_CODES['CANCELED_AUTO'], BOTransaction::STATUS_CODES['CANCELED_MANUAL']])->first();
                if($trans){
                    $staff = BOUser::where('ub_id',$trans->created_user_id)
                        ->with(["group" => function($group) {
                            return $group->select(["gb_title"]);
                        }])->first();
                    $showTrans[] = array(
                        'staff' => $staff ? $staff->ub_title : '',
                        'department' => $staff->group ? $staff->group->gb_title : '',
                        'end_lock_time' => $trans->auto_cancel_time != Null ? date('d-m-Y H:i',$trans->auto_cancel_time) : ''
                    );
                }
            }else if(in_array($apartment->status_id,[BOProduct::STATUSES['ADD_CUSTOMER'],BOProduct::STATUSES['CUSTOMER_CONFIRM'],BOProduct::STATUSES['DCH'],BOProduct::STATUSES['CDDCO'],BOProduct::STATUSES['DCO']])){
                $bill = BOBill::where('product_id',$productId)
                    ->whereNotIn('status_id', [BOBill::STATUSES['STATUS_SUCCESS'],
                        BOBill::STATUSES['STATUS_DISAPPROVED_BY_MANAGER'],
                        BOBill::STATUSES['STATUS_DISAPPROVED'],
                        BOBill::STATUSES['STATUS_BOOK_TO_DEPOSIT'],
                        BOBill::STATUSES['STATUS_CANCELED']])
                    ->first();
                if($bill){
                    $staff = BOUser::where('ub_id',$bill->staff_id)
                        ->with(["group" => function($group) {
                            return $group->select(["gb_title","gb_id"]);
                        }])->first();

                    $showTrans[] = array(
                        'staff' => $staff ? $staff->ub_title : '',
                        'department' => $staff->group ? $staff->group->gb_title : '',
                        'end_lock_time' => ''
                    );
                }
            }
            $apartment->showTrans = $showTrans;
            $titleTransLog = '';
            if($apartment->status_id == BOProduct::STATUSES['CHLOCK']){
                $titleTransLog = 'Danh sách nhân viên yêu cầu Lock';
            }
            else if($apartment->status_id == BOProduct::STATUSES['DCH']){
                $titleTransLog = 'Danh sách nhân viên đang đặt chỗ';
            }
            else if($apartment->status_id == BOProduct::STATUSES['DCO']){
                $titleTransLog = 'Danh sách nhân viên đặt cọc thành công';
            }
            else if(in_array($apartment->status_id,[BOProduct::STATUSES['LOCKED'],BOProduct::STATUSES['ADD_CUSTOMER'],BOProduct::STATUSES['CUSTOMER_CONFIRM']])){
                $titleTransLog = 'Danh sách nhân viên đang Lock';
            }else if($apartment->status_id == BOProduct::STATUSES['CDDCO']){
                $titleTransLog = 'Danh sách nhân viên đang chờ duyệt cọc';
            }
            $apartment->titleTransLog = $titleTransLog;
            $apartment->showBtnUpdate = in_array($productId,$arrApartment) ? true : false;
            echo json_encode(array(
                'apartment' => $apartment,
                'result' => 1
            ));
        }else{
            echo json_encode(array(
                'result' => 0
            ));
        }
    }

    protected function checkTemplateDefaultEmail(){
        $isDefault = Input::get('isDefault');
        $type = Input::get('type');
        $is_apartment = Input::get('is_apartment');
        if($isDefault == 1){
            $countDefaultTemplate = TemplateEmail::where(['is_default' => 1, 'type' => $type, 'is_apartment' => $is_apartment])->count();
            if($countDefaultTemplate == 1){
                echo json_encode(array(
                    'result' => 0,
                ));
            }else{
                echo json_encode(array(
                    'result' => 1,
                ));
            }
        }else{
            echo json_encode(array(
                'result' => 1
            ));
        }
    }

    protected function loadBankInfo(){
        $bankId = Input::get('idBank');
        $bank = BankInfo::find($bankId);
        if($bank){
            echo json_encode(array(
                'bank' => $bank,
                'result' => 1,
            ));
        }else{
            echo json_encode(array(
                'result' => 0
            ));
        }
    }

    /**

     * Auth: HuyNN
     * Des: load Payments
     * Since: 09/10/2018
     */
    protected function loadPayments(){
        $billCode = Input::get('billcode');
        $allPayments = BOPayment::loadPaymentByBillCode($billCode);
        echo json_encode($allPayments);

    }
    protected function loginTVC(){
        $username = Input::get('username');
        $pwd = Input::get('pwd');
        
        // get security code
        $res = DB::table('b_o_users')->where('ub_account_tvc', $username)->first();
        if(isset($res) && count((array) $res) > 0){
            $security_code = $res->security_code;
            // Login TVC
            $login_info =  [
                "id"=> 1,
                "username"=> $username,
                "password"=> $pwd,
                "appLog"=> "Y",
                "securityKey"=> $security_code
              ];
            $bodyLogin = array (
                'action' => 'ConnectDB',
                'method' => 'getConfig',
                'data' => [$login_info],
                'type' => 'rpc',
                'tid' => 998,
            );
            // Process Login
            $dataLogin = API\APIController::TVC_POST_DECODE($bodyLogin,'',$username);            
            if($dataLogin['success'] == 1){
                // update pwd BO
                DB::table('b_o_users')->where('ub_account_tvc', $username)->update(['password'=> bcrypt($pwd)]);
                $o_User = new BOUser();
                
                $loginCheck = $o_User->loginUser($username, $pwd);
                
                if($loginCheck){
                    $arrayRes = array('success' => "Done",
                        'result' => 1,
                    );
                    
                }else{
                    $arrayRes = array('success' => "Không thể Đăng nhập",
                        'result' => 0,
                    );
                    echo json_encode($res);
                }
            }else{
                $arrayRes = array('success' => "Kiểm tra mật khẩu hoặc hết hạn security code",
                        'result' => 0,
                );
            }
            
        }else{
            $arrayRes = array('success' => "Không tồn tại username",
                        'result' => 0,
            );
        }
        echo json_encode($arrayRes);
        
    }
//    protected function confirmPayment(){
////      print_r(self::decrypte(session('encryptPass'),self::ENCRYPTION_KEY));
//
//        $id = Input::get('id');
//        $bill_code = Input::get('bill_code');
//        $type = Input::get('type');
//        $money = (int)str_replace(' ','',Input::get('money'));
//        $pb_bank_number_id = Input::get('pb_bank_number_id');
//        $method_id = Input::get('method_id');
//
//        $ary['pb_status'] = $type == '0' ? -1 : 1;
//        $ary['pb_response_money'] = $money;
//        $ary['method_id'] = $method_id;
//        $ary['pb_bank_number_id'] = $pb_bank_number_id;
//        $ary['pb_response_time'] = date('Y-m-d H:i:s', time());
//        $ary['response_staff_id'] = Auth::guard('loginfrontend')->user()->ub_id;
//
//        $res = DB::table('b_o_payments')->where('id', $id)->update($ary);
//        // check thu du tien hay chua
//        $a_allPaymentByBillCode = DB::table('b_o_payments')->where('bill_code', $bill_code)->get();
//        $totalResponMoney = 0;
//        foreach($a_allPaymentByBillCode as $val){
//            $totalResponMoney += (int) $val->pb_response_money;
//        }
//        // get bill
//        $o_Bill = DB::table('b_o_bills')->where('bill_code', $bill_code)->first();
//
//        if($totalResponMoney >= (int) $o_Bill->bill_required_money){
//            DB::table('b_o_bills')->where('bill_code', $bill_code)->update(['status_id'=>BOBill::STATUSES['STATUS_PAID']]);
//        }else{
//            DB::table('b_o_bills')->where('bill_code', $bill_code)->update(['status_id'=>BOBill::STATUSES['STATUS_PAYING']]);
//        }
//
//        if($res){
//            if(self::checkLoginTvcWeb()){
//                $payment = BOPayment::find($id)->toArray();
//                $type = $o_Bill->type == 1 ? BOPayment::TYPE_PAYMENT_TAVICO['DCO'] : BOPayment::TYPE_PAYMENT_TAVICO['DCHO'];
//                $referenceCode = PaymentController::createPaymentTvc($payment,$type,$o_Bill);
//                if($referenceCode != '')
//                    DB::table('b_o_payments')->where('id', $id)->update(array('reference_code' => $referenceCode));
//            }
//            $arrayRes = array('success' => "Cập nhật dữ liệu thành công!",
//                              'result' => 1
//                );
//        }else{
//            $arrayRes = array('success' => "Không thể cập nhật dữ liệu!",
//                               'result' => 0,
//                );
//        }
//        echo json_encode($arrayRes);
//    }

    /**

     * Auth: HuyNN
     * Des: load Builidng when change Project
     * Since: 19/09/2018
     */
    protected function loadBuildingByProjectId(){
        $projectId = Input::get('projectId');
        $res = DB::table('b_o_categories')->select('id', 'cb_title', 'cb_id', 'cb_code')->where(array('parent_id' => $projectId , 'cb_level' => 2))->get();
        echo json_encode($res);
    }
    /**

     * Auth: HuyNN
     * Des: load Apartment when change Building
     * Since: 19/09/2018
     */
    protected function loadApartmentByBuildingId(){
        $buildingId = Input::get('buildingId');
        $res = BOProduct::where('category_id',$buildingId)->get()->toArray();
        echo json_encode($res);
    }

    /**
     * Auth: HuyNN
     * Des: load Bill when change Apartment
     * Since: 19/09/2018
     */
    protected function loadBillByApartmentId(){
        $apartmentId = Input::get('apartmentId');
        $typeOfPayment= Input::get('typeOfPayment');
        if($typeOfPayment == 'C'){
            $whereIn  = [BOBill::STATUSES['STATUS_PAYING'],BOBill::STATUSES['STATUS_PAID_ONCE']];
        }else{
            $whereIn  = [BOBill::STATUSES['STATUS_CANCELED']];
        }
        $res = BOBill::where('product_id',$apartmentId)->whereIn('status_id',$whereIn)->get()
            ->toArray();
        echo json_encode($res);
    }


    /**
     * Auth: DienCt
     * Des: Delete record
     * Since: 31/12/2015
     */
    protected function DeleteRow(){

        $statusField = Input::get('field');
        if($this->i_id == 0 || $this->sz_tbl == "") exit;
        if($this->i_type == 1){
            // update            
            $res = DB::table($this->sz_tbl)->where('id',(int)$this->i_id)->update(array($statusField => 0));
            
        }else if($this->i_type == 0){
            $res = DB::table($this->sz_tbl)->where('id', '=', $this->i_id)->update(array($statusField => -1));
        }
        if($res){
            $arrayRes = array('success' => "Cập nhật dữ liệu thành công!",
                              'result' => 1 
                );
           
        }else{
            $arrayRes = array('success' => "Không thể cập nhật dữ liệu!",
                               'result' => 0,
                );
        }
        echo json_encode($arrayRes);       
    }
    /**
     * @Auth: DienCt
     * @Des: Recover record
     * @Since: 31/12/2015
     */
    protected function RecoverRow(){

        if($this->i_id == 0 || $this->sz_tbl == "") exit;
        $statusField = Input::get('field');
        
            // update
            $res = DB::table($this->sz_tbl)->where('id',(int)$this->i_id)->update(array($statusField => 1));
        
        if($res){
            $arrayRes = array('success' => "Cập nhật dữ liệu thành công!",
                              'result' => 1 
                );
        }else{
            $arrayRes = array('success' => "Không thể cập nhật dữ liệu!",
                               'result' => 0,
                );
        }
        echo json_encode($arrayRes);

    }
    /**
     * @Auth: DienCt
     * @Des: Recover record
     * @Since: 31/12/2015
     */
    protected function addCustomerData(){

        $name = Input::get('name');
        $phone = Input::get('phone');
        $email = Input::get('email');
        $data = array('name'=>$name, 'phone'=>$phone, 'email'=>$email);
        Mail::send('data.mailH', array('a_EmailBody' => $data), function($message) {
                        $message->from('hello@app.com', 'Quảng Cáo Thủ Đô');
                        $message->to('vutruongxuan112@gmail.com');
                        $message->subject('Khách hàng đăng ký');
        });
        $arrayRes = array();
        echo json_encode($arrayRes);

    }
    /**
     * @Auth: DienCt
     * @Des: transfer Data Customer
     * @Since: 06/09/2018
     */
    protected function transferDataCustomer(){
        $staff = Input::get('staff');
        $chkArray = Input::get('chkArray');
        $fail = 0;
        $success = 0;
        // check staff in customer
        foreach($chkArray as $val){
            // mapping
            $o_Customer_Mapping = DB::table('user_customer_mappings')->where('id', $val)->first();
            
            // check tranfer data
            $checktranfer = DB::table('user_customer_mappings')->where('c_phone', $o_Customer_Mapping->c_phone)->where('id_user', $staff)->first();
            
            //add mapping
            if(!$checktranfer){
                
                //insert user_customer_mappings
                $aryMapping['id_user'] = $staff;
                $aryMapping['id_customer'] = $o_Customer_Mapping->id_customer;
                $aryMapping['created_at'] = date('Y-m-d H:i:s', time());
                $aryMapping['updated_at'] = date('Y-m-d H:i:s', time());
                $aryMapping['c_title'] = $o_Customer_Mapping->c_title;
                $aryMapping['c_phone'] = $o_Customer_Mapping->c_phone;
                $aryMapping['email'] = $o_Customer_Mapping->email;
                DB::table('user_customer_mappings')->insert($aryMapping);
                //insert user_customer_mappings
                $success += 1;
            }else{
                $fail += 1;
            }            
                        
        }
        $msg = '';
        $msg .= $success != 0 ? "Phân bổ thành công ".$success ." khách hàng \n" : '';
        $msg .= $fail != 0 ? "Phân bổ KHÔNG thành công ".$fail ." khách hàng \n" : '';
        echo json_encode(array('msg' => $msg,
                               'result' => 1,
                ));
    }
    /**
     * @Auth: DienCt
     * @Des: transfer Data Customer
     * @Since: 06/09/2018
     */
    protected function transferTMPDataCustomer(){
        $staff = Input::get('staff');
        $chkArray = Input::get('chkArray');
        $success = $fail = 0;
        // check staff in customer
        $k = time();
        
        foreach($chkArray as $val){
            $employName = '';
            $o_tmp_customer_data = DB::table('tmp_customer_data')->where('id',$val)->first();
            if($o_tmp_customer_data->tc_employ_ids != ''){
                $ary_employ_id = (array) json_decode($o_tmp_customer_data->tc_employ_ids);                
                $ary_employ_id[$staff] = $staff;
                $ary_employ_id = array_unique($ary_employ_id);
                
                $str_json_employId = json_encode($ary_employ_id);                
                foreach($ary_employ_id as $valUser){
                    $employName .= isset(DB::table('b_o_users')->where('ub_id', $valUser)->first()->ub_account_tvc) ? '__'.DB::table('b_o_users')->where('ub_id', $valUser)->first()->ub_account_tvc : '';
                }                
                
            }else{
                $ary_employ_id[$staff] = $staff;
                $str_json_employId = json_encode($ary_employ_id);
                $employName = isset(DB::table('b_o_users')->where('ub_id', $staff)->first()->ub_account_tvc) ? DB::table('b_o_users')->where('ub_id', $staff)->first()->ub_account_tvc : '';
            }
            
            DB::table('tmp_customer_data')->where('id',$val)->update(array('tc_employ_ids' => $str_json_employId ,'tc_employ_name' => $employName));            
            // insert to b_o_customers
            
            $o_checkIssetCus = DB::table('b_o_customers')->where('cb_phone', $o_tmp_customer_data->tc_phone)->first();
            $o_checkIssetMapping = DB::table('user_customer_mappings')->where('c_phone', $o_tmp_customer_data->tc_phone)->where('id_user', $staff)->first();
        
            if(!$o_checkIssetCus){
                $aryInsertCusBO['cb_name'] = $o_tmp_customer_data->tc_name;
                $aryInsertCusBO['cb_phone'] = $o_tmp_customer_data->tc_phone;
                $aryInsertCusBO['source_id'] = $o_tmp_customer_data->source_id;
                $aryInsertCusBO['cb_status'] = 1;
                $aryInsertCusBO['cb_email'] = $o_tmp_customer_data->tc_email;
                $aryInsertCusBO['project_id'] = $o_tmp_customer_data->project_id;
                $aryInsertCusBO['created_at'] = date('Y-m-d H:i:s', time());
                $aryInsertCusBO['cb_id'] = (int) $k += 1;
                $aryInsertCusBO['updated_at'] = date('Y-m-d H:i:s', time());
                $aryInsertCusBO['tc_created_by'] = Auth::guard('loginfrontend')->user()->ub_id;
                $aryInsertCusBO['cb_staff_id'] = $o_tmp_customer_data->tc_staff_ids;
                DB::table('b_o_customers')->insert($aryInsertCusBO);
                
            }else{
                $a_staff = $o_checkIssetCus->cb_staff_id != '' ? explode(',',$o_checkIssetCus->cb_staff_id) : array();
                
                if (in_array($staff, $a_staff)){
                    
                }else{
                    $a_staff[] = $staff;
                    $str_staff = implode(',',$a_staff);
                    DB::table('b_o_customers')->where('cb_phone', $o_tmp_customer_data->tc_phone)->update(array('cb_staff_id' => $str_staff));
                }
            }
            
            //add mapping
            if(!$o_checkIssetMapping){
                $o_checkIssetCus2 = DB::table('b_o_customers')->where('cb_phone', $o_tmp_customer_data->tc_phone)->first();
                //insert user_customer_mappings
                $aryMapping['id_user'] = $staff;
                $aryMapping['id_customer'] = $o_checkIssetCus2->cb_id;
                $aryMapping['created_at'] = date('Y-m-d H:i:s', time());
                $aryMapping['updated_at'] = date('Y-m-d H:i:s', time());
                $aryMapping['c_title'] = $o_tmp_customer_data->tc_name;
                $aryMapping['c_phone'] = $o_tmp_customer_data->tc_phone;
                $aryMapping['email'] = $o_tmp_customer_data->tc_email;
                DB::table('user_customer_mappings')->insert($aryMapping);
                //insert user_customer_mappings
                $success += 1;
            }else{
                $fail += 1;
            }
        }
        
        $msg = '';
        $msg .= $success != 0 ? "Phân bổ thành công ".$success ." khách hàng \n" : '';
        $msg .= $fail != 0 ? "Đã phân bổ  ".$fail ." khách hàng trước đó\n" : '';
        echo json_encode(array('msg' => $msg,
                               'result' => 1,
                ));
    }
    
    
    protected function getAllObjectId(){
        $key = Input::get('key');// lay cho vui
        $BOCategory = new BOCategory();
        $BOCategory->getAllCategoriesByParentID(0, $a_project);
        
        if($a_project){
            $htmlReturn = '';
            foreach ($a_project as $key => $val){
                
                if($val['cb_level'] == 1){
                    $beautiFomat = '';
                }else if($val['cb_level'] == 2){
                    $beautiFomat = '--';
                }else if($val['cb_level'] == 3){
                    $beautiFomat = '----';
                }
                        
                $htmlReturn .="<option value='{$key}'>{$beautiFomat}{$val['cb_title']}</option>";
            }   
            echo json_encode(array('html' => $htmlReturn,
                               'result' => 1,
                ));
        }else{
            echo json_encode(array('html' => '',
                               'result' => 1,
                ));
        }
    }
    
    
    protected function getbillInfo(){
        $billId = Input::get('bill_id');// lay cho vui
        $a_DataBill = BOBill::find($billId);
        $htmlTotalMoney = "<input type='hidden' name ='bill_required_money' id='bill_required_money' class='bill_required_money' value='{$a_DataBill->bill_required_money}'>";
        
        if($a_DataBill->customer_id){
            $o_Cus = DB::table('user_customer_mappings')->where('id_customer', $a_DataBill->customer_id)->first();
            if($o_Cus){
                $htmlCus = "<option>{$o_Cus->c_name}</option>";
            }
        }
        
        if($a_DataBill->staff_id){
            $o_Staff = DB::table('b_o_users')->where('ub_id', $a_DataBill->staff_id)->first();
            if($o_Staff){
                $htmlStaff = "<option value='{$o_Staff->ub_id}'>{$o_Staff->ub_title}</option>";
            }
        }
        $type = $a_DataBill->type == 1 ? 'đặt cọc' : 'đặt chỗ';

        $product = BOProduct::where('pb_id',$a_DataBill->product_id)->first();
        $cate = BOCategory::where('cb_id',$product->category_id)->first();
        $project = BOCategory::find($cate->parent_id);
        $note2 = $a_DataBill->status_id == BOBill::STATUSES['STATUS_CANCELED'] ? '' : 'Khách hàng '. $o_Cus->c_name.' '.$type.' căn '.$product->pb_code;
        if($htmlCus && $htmlStaff){
            $ary_Return = array(
                'success'=> 1,
                'htmlCus'=>$htmlCus,
                'htmlStaff'=>$htmlStaff,
                'htmlTotalMoney'=>$htmlTotalMoney,
                'htmlnote2' => $note2,
                'htmlProductCode' => $product->pb_code,
                'htmlBuildingCode' => $cate->cb_code,
                'htmlProjectName' => $project->cb_title
            );
            
        }else{
            $ary_Return = array(
                'success'=> 0,
                'msg'=> 'Kiểm tra lại thông tin nhân viên hoặc khách hàng'
            );
        }
        echo json_encode($ary_Return);
    }
    /**

     * @auth: Dienct
     * @since: 14/03/2017
     * @des: save session
     * 
     *      */
    protected function SaveSessionStatistics(){
        $sz_filter_by = Input::get('sz_filter_by','');
        $szfrom_date = Input::get('szfrom_date','');
        $szto_date = Input::get('szto_date','');
        Session::forget('ss_from_date');
        Session::forget('ss_to_date');
        Session::put('ss_filter_by', $sz_filter_by);
        Session::put('ss_from_date', $szfrom_date);
        Session::put('ss_to_date', $szto_date);
        
    }
    
    
}
