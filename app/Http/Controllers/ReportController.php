<?php

namespace App\Http\Controllers;
use App\BOProduct;
use App\BOTransaction;
use App\BOUser;
use App\BOUserGroup;
use App\Report;
use App\UserCustomerMapping;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\BOCategory;
use Illuminate\Http\Request;
use DB;
use App\BOBill;
use App\Http\Controllers\API\LogController;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
class ReportController extends Controller
{
    public function __construct() {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getBillReport(Request $request) {
        /** @var $filter */
        $filter = [
            'from' => $request->input('from', date('Y-m-d')),
            'to'    => $request->input('to', date('Y-m-d'))
        ];

        /** @var $project_ids */
        $project_ids = RoleController::getProjectsByUserRole('report');
        /** @var $projects */
        $projects = BOCategory::select(
            'id',
            BOCategory::ID_KEY,
            'cb_code AS code',
            'cb_title AS title',
            'parent_id'
        )
            ->whereIn(BOCategory::ID_KEY, $project_ids)
            ->where('cb_status', env('STATUS_ACTIVE',1))
            ->orderBy('cb_title', 'ASC')
            ->get()->keyBy(BOCategory::ID_KEY);
        /** @var $report */
        $report = Report::whereIn('project', $project_ids)->whereBetween('report_date', [$filter['from'], $filter['to']]);
        if ($filter['from'] !== $filter['to']) {
            $report = $report->select('building', 'department', 'project', \Illuminate\Support\Facades\DB::raw('SUM(value) AS value'))->groupBy('building', 'department', 'project');
        }
        $report = $report->get();
//        return $report;
        $project_ids = $projects->pluck('id');
        /** @var $buildings */
        $buildings = BOCategory::select(
            BOCategory::ID_KEY,
            'cb_code AS code',
            'cb_title AS title',
            'parent_id'
        )
            ->whereIn('parent_id', $project_ids)
            ->where('cb_status', env('STATUS_ACTIVE',1))
            ->where('cb_level', 2)
            ->orderBy('cb_title', 'ASC')
            ->get()->keyBy(BOCategory::ID_KEY);
//        return  [
//            'projects' => $projects,
//            'buildings' => $buildings,
//            'data'  => $report
//        ];
        /** @var $departments */
        $departments = BOUserGroup::select([
            BOUserGroup::ID_KEY . ' AS id',
            'gb_title AS text'
        ])
            ->where('gb_status', env('STATUS_ACTIVE', 1))
            ->orderBy('gb_title', 'ASC')
            ->get()->keyBy('id');
        return view('bill.report', [
            'projects' => $projects,
            'buildings' => $buildings,
            'data'  => $report,
            'departments' => $departments,
            'filter'    => $filter
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function submitBillReport(Request $request) {
        $request->validate([
            'department' => 'required',
            'building' => 'required',
            'figure' => 'required',
        ]);
        $department = $request->input('department');
        $building = $request->input('building');
        $figure = $request->input('figure');
        $date = $request->input('date', date('Y-m-d'));
        /** @var $category */
        $category = BOCategory::where(BOCategory::ID_KEY, (int) $building)->with('sibling')->first();
        if (!$category->sibling) {
            session()->flash('error', 'Không tồn tại dự án!');
            return back();
        }
        /** @var $project */
        $project = $category->sibling->{BOCategory::ID_KEY};

        /** @var $query */
        $query = Report::where([
            'department' => (int) $department,
            'building'  => (int) $building,
            'report_date'   => $date
        ])->first();
        if (!$query) {
            $query = new Report();
            $query->department = (int) $department;
            $query->building = (int) $building;
            $query->report_date = $date;
            $query->created_at = now();
            $query->created_by = BOUser::getCurrentUID(false);
            $query->project = $project;
        }
        $query->value = (int) $figure;
        $query->updated_at = now();
        if ($query->save()) {
            session()->flash('message', 'Cập nhật thành công!');
            return redirect(route('report-bill'));
        }
        session()->flash('error', 'Xả ra lỗi khi cập nhật dữ liệu!');
        return back();
    }

    /**
     * @return mixed
     */
    protected function downloadBillTemplate() {
        $today = Carbon::today()->format('d-m-Y');

        return Excel::create('bao-cao-giao-dich-'.$today, function ($excel) use ($today) {
            $excel->setTitle('Báo cáo giao dịch ngày '.$today)
                ->setCreator('PCN')
                ->setCompany('DXMB')
                ->setDescription('Mẫu import báo cáo giao dịch hàng ngày - Đất Xanh Miền Bắc');
            $excel->sheet('Báo cáo ngày '.$today, function($sheet) {
                // Sheet manipulation
                $departments = BOUserGroup::select('gb_description')
                    ->where('gb_status', env('STATUS_ACTIVE', 1))
                    ->where('gb_code', 'S')
                    ->whereNotNull('gb_description')
                    ->orderBy('gb_title', 'ASC')
                    ->get()
                    ->pluck('gb_description')->toArray();
                $projects = BOCategory::select(['reference_code AS code', 'cb_title AS title', 'alias'])
                ->where([
                    'cb_status' => env('STATUS_ACTIVE', 1),
                    'cb_level'  => 1
                ])->get();
                $sheet->loadView('report.template-bill')
                ->with([
                    'departments' => $departments,
                    'projects'     => $projects
                ]);
            });
        })
            ->download('xlsx');
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function importBillReport(Request $request) {
        $request->validate([
            'file' => 'required',
            'date' => 'required|string',
        ]);
        $date = $request->input('date');
        /** @var $file */
        $file = $request->file('file');
        $file_name = $file->getClientOriginalName();
        $extension = strtolower($file->getClientOriginalExtension());
        if (!in_array($extension, ['xlsx', 'xls', 'csv'])) return abort(302, 'Định dạng file không hợp lệ!');

        /** @var $departments */
        $departments = BOUserGroup::select([BOUserGroup::ID_KEY, 'gb_description AS key'])
            ->where('gb_status', env('STATUS_ACTIVE', 1))
            ->where('gb_code', 'S')
            ->orderBy('gb_title', 'ASC')
            ->get()->keyBy('key');
        foreach ($departments as $key => $department) {
            $departments[str_replace('-', '_', str_slug($key))] = $department->{BOUserGroup::ID_KEY};
            unset($departments[$key]);
        }
        /** @var $projects */
        $projects = BOCategory::select(['reference_code AS code', BOCategory::ID_KEY])
            ->where([
                'cb_status' => env('STATUS_ACTIVE', 1),
                'cb_level'  => 1
            ])
            ->get()->keyBy('code')->toArray();

//        return $projects;

        $file->move('uploads/', $file_name);
        /** @var $results */
        $results = Excel::load('uploads/'.$file_name)->get();
        if (!$results) {
            session()->flash('error', 'Không tìm thấy dữ liệu import!');
            return back();
        }
        /** @var $data */
        $data = $logs = [];
        $uid = BOUser::getCurrentUID(false);
//        return $results;
        foreach ($results as $result) {
            $tvc_code = (string) $result->ma_crm;
            if (!array_key_exists($tvc_code, $projects)) {
                $logs[] = LogController::logBill('[importBillReport] Không tồn tại Dự án: '.$tvc_code, 'warning', null, false, false);
            } else {
                foreach ($departments as $title => $id) {
                    if (isset($result->{$title}) && $result->{$title} > 0) {
                        $data[] = [
                            'value' => $result->{$title},
                            'department' => $id,
                            'building' => null,
                            'project' => $projects[$tvc_code][BOCategory::ID_KEY],
                            'created_by' => $uid,
                            'report_date' => $date,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                    } else {
                        $logs[] = LogController::logBill('[importBillReport] Không tồn tại Sàn: '.$title, 'warning', null, false, false);
                    }
                }
            }
        }

        if (!Report::insert($data)) {
            session()->flash('error', 'Xảy ra lỗi khi lưu '. count($data) . ' bản ghi!');
            return back();
        };
        session()->flash('message', "Import thành công ".count($data) . '/'. count($results) .' bản ghi!');
        LogController::saveLogs($logs);
        return back();
    }

    /**
     * @param string $from
     * @param string $to
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function composeReportPost($from='', $to='') {
        $start = Carbon::createFromFormat('Y-m-d', $from)->format('d/m/Y');
        $end = Carbon::createFromFormat('Y-m-d', $to)->format('d/m/Y');
        $item = [
            'title' => 'Báo cáo giao dịch tuần từ '.$start.' - ' . $end
        ];
        /** @var $data */
        $data = ['item' => $item];
        $data['projects'] = BOCategory::getAllProject();
        $data['users'] = BOUser::select([BOUser::ID_KEY, 'id', 'ub_title AS name', 'ub_account_tvc AS account'])
            ->where('ub_status', env('STATUS_ACTIVE', 1))->orderBy('ub_title', 'ASC')
            ->get();
        $data['groups'] = BOUserGroup::select([BOUserGroup::ID_KEY, 'id', 'gb_title AS title'])
            ->where('gb_status', env('STATUS_ACTIVE', 1))
            ->where('reference_code', '!=', 'ALL')
            ->get();
        $data['group_all'] = BOUserGroup::select(['id', BOUserGroup::ID_KEY, 'gb_title AS title'])
            ->where('reference_code', 'ALL')
            ->first();
        return view('post.form-report', $data);
    }

    public function index($type, $action)
    {     
        // parent::index($action, $id);
        $roleMenuWeb = RoleController::getRoleMenuWebByUserId();
        if(isset($roleMenuWeb['report_admin'])){
            $request = \Request::all();
        
            $words = explode('-', $action);
            if ($words && count($words) > 1) {

                $strConcat = '';
                foreach ($words as $word) {
                    if ($word != reset($words)) {
                        $strConcat .= ucfirst($word);
                    } else {
                        $strConcat .= $word;
                    }
                }
                $action = $strConcat;
            }       
        
            $kind = isset($request['kind'])?$request['kind']:'bill';
            if ($action!='listAction' && method_exists($this, $action)) {
                return $this->$action();
            }else{
                return $this->listAction($kind);
            }
        }
        else{

            // return 'Bạn không có quyền đăng nhập!';
            $url = url('sign-out');
            return redirect( $url);
        }
       
      
    }
    public function listAction($kind){
        $request = \Request::all();
        // return $kind;
        switch ($kind) {
            case 'bill':
                return self::reportBill($request);
                break;
            case 'transaction':
                return self::reportTransaction($request);
                break;
            case 'customer':
                return self::reportCustomer($request);
                break;
             case 'product':
                return self::reportProduct($request);
                break;
            case 'staff':
                return self::reportStaff($request);
                break;
            default:
                return self::reportBill($request);
                break;
        }        
        
    }
    public function reportBill($request)
    {       
        $listStatus = BOBill::STATUSES;
        if(isset($request['type_report'])&&$request['type_report']==2){
            return $this->chartReport();
        }
         if(!isset($request['itemperpage'])){
            $request['itemperpage'] = 20;
        }
        $tpl['staffList'] = BOUser::get()->keyBy(BOUser::ID_KEY)->toArray();
        $a_Data = BOBill::where('id','!=','0');
        // $request = $request->all();
        // return $a_Data->get();
        $tpl['r_departments'] = BOUserGroup::getSanCCN();
        $tpl['r_project'] = BOCategory::where('cb_status','!=','-1')->where('cb_level',BOCategory::PROJECT_LEVEL)->orderBy('cb_title','ASC')->get();
        if($request){
            if(isset($request['status_id']))
            {
                $a_Data = $a_Data->where('status_id',$request['status_id']);
            }
            if(isset($request['r_text']))
            {
                $q_text = '%'.$request['r_text'].'%';
                $a_Data = $a_Data->where(function ($q) use ($q_text){
                    $q->whereIn('product_id',function ($q1) use ($q_text){
                        $q1->select('pb_id')->from('b_o_products')->where('pb_code','like', $q_text)->orWhere('pb_code_real','like', $q_text)->select(BOProduct::ID_KEY)->get();
                         },'or')
                    ->orWhere('bill_code','like',$q_text)->orWhere('bill_reference_code','like',$q_text)->orWhere('contract_reference_code','like',$q_text);

                });
            }
            if(isset($request['r_project_id']))
            {
                $project_id = $request['r_project_id'];
                $arr_products = BOProduct::whereIn('category_id',function($q) use ($project_id){
                    $q->select('cb_id')->from('b_o_categories')->where(['cb_level'=>2,'parent_id'=>$project_id]);})->select(BOProduct::ID_KEY)->get();
                $a_Data = $a_Data->whereIn('product_id',$arr_products);
            }
            if(isset($request['start_date']))
            {
                $start_date = date_create_from_format('d-m-Y',$request['start_date'])->format('Y-m-d 00:00:00');
                $a_Data = $a_Data ->where('bill_created_time','>=',$start_date);
            }
            if(isset($request['end_date']))
            {
                $end_date = date_create_from_format('d-m-Y',$request['end_date'])->format('Y-m-d 23:59:59');
                $a_Data = $a_Data ->where('bill_created_time','<=',$end_date);
            }
            if(isset($request['staff_id']))
            {
                $a_Data = $a_Data ->where('staff_id',$request['staff_id']);
            }
            if(isset($request['department_id']))
            {
                $arr_staff = BOUser::where('group_ids',$request['department_id'])->select('ub_id')->get();
                $a_Data = $a_Data ->whereIn('staff_id',$arr_staff);
            }
          
        }
        $tpl['request'] = $request;     
        if(!$request['itemperpage']){
             $request['itemperpage'] = 1000000000;
        }
       
        if(isset($request['export_excel'])){
            $a_Data = $a_Data->get();
            return $this->exportExcel($a_Data,'Danh sách Hợp đồng _ Xuất ngày'.date('d-m-Y'),'Hợp đồng','xlsx');
        }
        $a_Data = $a_Data->with('staff.group')->with('product');
        $a_Data = $a_Data->orderBy('bill_updated_time','DESC')->paginate($request['itemperpage']);
       
        $a_Data->appends($request);      
        $tpl['list_bills'] = $a_Data;
       
        return view('report.list_bill',$tpl);
    }    
    public function reportTransaction($request)
    {
        $tpl['staffList'] = BOUser::get()->keyBy(BOUser::ID_KEY)->toArray();
        $listStatus = BOTransaction::STATUSES;
        if(isset($request['type_report'])&&$request['type_report']==2){
            return $this->chartReport();
        }
         if(!isset($request['itemperpage'])){
            $request['itemperpage'] = 20;
        }

        $a_Data = BOTransaction::where(BOTransaction::ID_KEY,'!=','0');
        // return $a_Data->get();
        // $request = $request->all();
        $tpl['r_departments'] = BOUserGroup::getSanCCN();
        $tpl['r_project'] = BOCategory::where('cb_status','!=','-1')->where('cb_level',BOCategory::PROJECT_LEVEL)->orderBy('cb_title','ASC')->get();
        if($request){
            if(isset($request['status_id']))
            {
                $a_Data = $a_Data->where('trans_code',$request['status_id']);
            }
            if(isset($request['r_text']))
            {
                $q_text = '%'.$request['r_text'].'%';
                $a_Data = $a_Data->whereIn('product_id',function ($q) use ($q_text){
                    $q->select('pb_id')->from('b_o_products')->where('pb_code','like', $q_text)->orWhere('pb_code_real','like', $q_text)->select(BOProduct::ID_KEY)->get();
                });
            }
            if(isset($request['r_project_id']))
            {
                $project_id = $request['r_project_id'];
                $arr_products = BOProduct::whereIn('category_id',function($q) use ($project_id){$q->select('cb_id')->from('b_o_categories')->where(['cb_level'=>2,'parent_id'=>$project_id]);})->select(BOProduct::ID_KEY)->get();
                $a_Data = $a_Data->whereIn('product_id',$arr_products);
            }
            if(isset($request['start_date']))
            {
                $start_date = date_create_from_format('d-m-Y',$request['start_date'])->format('Y-m-d 00:00:00');
                $a_Data = $a_Data ->where('trans_created_time','>=',$start_date);
            }
            if(isset($request['end_date']))
            {
                $end_date = date_create_from_format('d-m-Y',$request['end_date'])->format('Y-m-d 23:59:59');
                $a_Data = $a_Data ->where('trans_created_time','<=',$end_date);
            }
            if(isset($request['staff_id']))
            {
                $a_Data = $a_Data ->where('staff_code',$request['staff_id']);
            }
            if(isset($request['department_id']))
            {
                $arr_staff = BOUser::where('group_ids',$request['department_id'])->select('ub_id')->get();
                $a_Data = $a_Data ->whereIn('staff_code',$arr_staff);
            }
          
        }
        $tpl['request'] = $request;     
        if(!$request['itemperpage']){
             $request['itemperpage'] = 1000000000;
        }
        $a_Data = $a_Data->orderBy('trans_updated_time','DESC');
        if(isset($request['exportExcel'])){
            $a_Data = $a_Data->get();
            return $this->exportExcel($a_Data,'Danh sách Transaction _export ngày'.date('d-m-Y'),'Yêu cầu','xlsx');
        }

        $a_Data = $a_Data->with(['staff.group','product'])->paginate($request['itemperpage']);
        $a_Data->appends($request);      
        $tpl['list_trans'] = $a_Data;
       
       
        return view('report.list_transaction',$tpl);
    } 
    public function reportCustomer($request,$showContact=true)
    {
        $tpl['staffList'] = BOUser::get()->keyBy(BOUser::ID_KEY)->toArray();
        // $listStatus = BOTransaction::STATUSES;
        if(isset($request['type_report'])&&$request['type_report']==2)
        {
            return $this->chartReport();
        }
         if(!isset($request['itemperpage'])){
            $request['itemperpage'] = 20;
        }

        $a_Data = UserCustomerMapping::where('id','!=','0');
        // return $a_Data = UserCustomerMapping::where('id','!=','0')->with(['bo_user.group'])->get();
        // $request = $request->all();r_staffs
        $tpl['r_staffs'] = BOUser::orderBy('ub_title','ASC')->get();
        $tpl['r_departments'] = BOUserGroup::getSanCCN();
          $tpl['r_project'] = BOCategory::where('cb_status','!=','-1')->where('cb_level',BOCategory::PROJECT_LEVEL)->orderBy('cb_title','ASC')->get();
        if($request){
            if(isset($request['status_id']))
            {
                $a_Data = $a_Data->where('trans_code',$request['status_id']);
            }
            if(isset($request['r_text']))
            {
                $q_text = '%'.$request['r_text'].'%';
                $a_Data = $a_Data->where(function ($q) use ($q_text){
                   $q->orWhere('permanent_address','like',$q_text)->orWhere('c_address','like',$q_text)
                   
                   ->orWhere('potential_reference_code','like',$q_text)->orWhere('customer_reference_code','like',$q_text)
                   ->orWhere('c_title','like',$q_text)->orWhere('c_name','like',$q_text)
                            ->orWhere('c_phone','like',$q_text)->orWhere('id_passport','like',$q_text)->orWhere('email','like',$q_text);
                });
                
            }
             
            if(isset($request['start_date']))
            {
                $start_date = date_create_from_format('d-m-Y',$request['start_date'])->format('Y-m-d 00:00:00');
                $a_Data = $a_Data ->where('created_at','>=',$start_date);
            }
            if(isset($request['end_date']))
            {
                $end_date = date_create_from_format('d-m-Y',$request['end_date'])->format('Y-m-d 23:59:59');
                $a_Data = $a_Data ->where('created_at','<=',$end_date);
            }
            if(isset($request['staff_id']))
            {
                $a_Data = $a_Data ->where('tc_created_by',$request['staff_id']);
            }
            if(isset($request['department_id']))
            {
                $arr_staff = BOUser::where('group_ids',$request['department_id'])->select('ub_id')->get();
                $a_Data = $a_Data ->whereIn('tc_created_by',$arr_staff);
            }
          
        }
        $tpl['request'] = $request;     
        if(!$request['itemperpage']){
             $request['itemperpage'] = 1000000000;
        }
        if(isset($request['export_excel'])){
            $a_Data = $a_Data->get();
            return $this->exportExcel($a_Data,'Danh sách khách hàng _ Xuất ngày'.date('d-m-Y'),'Nhân viên','xlsx');
        }
        $a_Data = $a_Data->with(['bo_user.group'])->orderBy('updated_at','DESC')->paginate($request['itemperpage']);
       
        $a_Data->appends($request);      
        $tpl['list_customer'] = $a_Data;
        foreach ($a_Data as $key => &$customer) {
          if($showContact){
                $customer->c_phone = mb_substr($customer->c_phone, 0, 3). '****' . mb_substr($customer->c_phone,  -3);
                $customer->email =  mb_substr($customer->email, 0, 2) . '****@' . str_after($customer->email, '@');
            }
        }
       
        return view('report.list_customer',$tpl);
    } 
    public function reportProduct($request)
    {
        $listStatus = BOProduct::STATUS_DISPLAY;
        if(isset($request['type_report'])&&$request['type_report']==2){
            return $this->chartReport();
        }
         if(!isset($request['itemperpage'])){
            $request['itemperpage'] = 20;
        }

        $a_Data = BOProduct::where(BOProduct::ID_KEY,'!=','0')->orderBy('last_sync_tvc','desc');

        // $request = $request->all();
        $tpl['r_status'] = $listStatus;
        // $tpl['r_departments'] = BOUserGroup::getSanCCN();
        $tpl['r_project'] = BOCategory::where('cb_status','!=','-1')->where('cb_level',BOCategory::PROJECT_LEVEL)->orderBy('cb_title','ASC')->get();
        if($request){
            if(isset($request['r_text']))
            {
                $q_text = '%'.$request['r_text'].'%';
                $a_Data = $a_Data->where(function ($q) use ($q_text){
                    $q ->orWhere('pb_code','like',$q_text)->orWhere('pb_code_real','like',$q_text);
                });
            }
            if(isset($request['status_id']))
            {
                $a_Data = $a_Data->where('status_id',$request['status_id']);
            }
            if(isset($request['r_project_id']))
            {
                $project_id = $request['r_project_id'];
                $arr_products = BOProduct::whereIn('category_id',function($q) use ($project_id){$q->select('cb_id')->from('b_o_categories')->where(['cb_level'=>2,'parent_id'=>$project_id]);})->select(BOProduct::ID_KEY)->get();
                $a_Data = $a_Data->whereIn('pb_id',$arr_products);
            }
           
            
            if(isset($request['department_id']))
            {
                $arr_staff = BOUser::where('group_ids',$request['department_id'])->select('ub_id')->get();
                $a_Data = $a_Data ->whereIn('staff_code',$arr_staff);
            }
          
        }
        $tpl['request'] = $request;     
        if(!$request['itemperpage']){
             $request['itemperpage'] = 1000000000;
        }
        if(isset($request['export_excel'])){
            $a_Data = $a_Data->get();
            return $this->exportExcel($a_Data,'Danh sách Bảng hàng _ Xuất ngày'.date('d-m-Y'),'Bảng hàng','xlsx');
        }
        $a_Data = $a_Data->with(['category.sibling', 'updated_by:ub_title,ub_account_tvc,ub_id'])->orderBy('pb_title','ASC')->paginate($request['itemperpage']);
        $a_Data->appends($request);
//        return response()->json($a_Data);
        $tpl['list_product'] = $a_Data;
       
       
        return view('report.list_product',$tpl);
    }
    public function reportStaff($request)
    {
                
        if(!isset($request['itemperpage'])){
            $request['itemperpage'] = 20;
        }

        $a_Data = BOUser::where(BOUser::ID_KEY,'!=','0')->orderBy('ub_title','ASC');
        // return $a_Data->get();
        // $request = $request->all();
        // $tpl['r_status'] = $listStatus;
        $tpl['r_departments'] = BOUserGroup::getSanCCN();
         $tpl['staffList'] = BOUser::get()->keyBy(BOUser::ID_KEY)->toArray();
        if($request){
            if(isset($request['r_text']))
            {
                $q_text = '%'.$request['r_text'].'%';
                $a_Data = $a_Data->where(function ($q) use ($q_text){
                    $q ->orWhere('ub_title','like',$q_text)->orWhere('ub_account_name','like',$q_text)
                    ->orWhere('ub_staff_code','like',$q_text)->orWhere('ub_tvc_code','like',$q_text)
                    ->orWhere('ub_email','like',$q_text);
                });
            }         
            
            if(isset($request['department_id']))
            {
                $arr_staff = BOUser::where('group_ids',$request['department_id'])->select('ub_id')->get();
                $a_Data = $a_Data ->whereIn('ub_id',$arr_staff);
            }
            if(isset($request['staff_id']))
            {               
                $a_Data = $a_Data ->where('ub_id',$request['staff_id']);
            }
            if(isset($request['q_type']))
            {
                switch ($request['q_type']) {
                    case 1:
                       $a_Data = $a_Data ->whereNull('signature');
                       break;
                   
                   default:
                       
                       break;
               }               
                
            }
          
        }
        $tpl['request'] = $request;     
        if(!$request['itemperpage']){
             $request['itemperpage'] = 1000000000;
        }
        if(isset($request['export_excel'])){
            $a_Data = $a_Data->get();
            return $this->exportExcel($a_Data,'Danh sách nhân viên _ Xuất ngày'.date('d-m-Y'),'Nhân viên','xlsx');
        }
        $a_Data = $a_Data->with(['group'])->orderBy('ub_title','ASC')->paginate($request['itemperpage']);
       
        $a_Data->appends($request);      
        $tpl['list_staffs'] = $a_Data;
       
       
        return view('report.list_staff',$tpl);
    }
    public function chartReport(){
        $request = \Request::all();       
        $a_Data = BOBill::where('id','!=','0')->with('product');
        // $request = $request->all();
        $tpl['r_departments'] = BOUserGroup::getSanCCN();
        $tpl['r_project'] = BOCategory::where('cb_status','!=','-1')->where('cb_level',BOCategory::PROJECT_LEVEL)->get();
        if($request){
            if(isset($request['status']))
            {
                $a_Data = $a_Data->where('bill_status',$request['status']);
            }
            if(isset($request['start_date']))
            {
                $start_date = date_create_from_format('d-m-Y',$request['start_date'])->format('Y-m-d 00:00:00');
                $a_Data = $a_Data ->where('bill_created_time','>=',$start_date);
            }
            if(isset($request['end_date']))
            {
                $end_date = date_create_from_format('d-m-Y',$request['end_date'])->format('Y-m-d 23:59:59');
                $a_Data = $a_Data ->where('bill_created_time','<=',$end_date);
            }
            if(isset($request['staff_id']))
            {
                $a_Data = $a_Data ->where('staff_id',$request['staff_id']);
            }
            if(isset($request['department_id']))
            {
                $arr_staff = BOUser::where('group_ids',$request['department_id'])->select('ub_id')->get();
                $a_Data = $a_Data ->whereIn('staff_id',$arr_staff);
            }
          
        }
        $tpl['request'] = $request;
        $a_Data = $a_Data->paginate(20);
        // return $a_Data;
        $a_Data->appends($request);
        // $a_Data = $a_Data->render();

        $tpl['list_bills'] = $a_Data;
        // $tpl['table_bills'] = view('report.table',$tpl);   
        return view('report.chart',$tpl);
    }
    private  function getStatisticCCN()
    {
        $input = \Request::all();
        // return $input;
        $departments = BOBill::groupBy('gb_id') 
                    ->with('department')
                    ->select('gb_id',\DB::raw('SUM(bill_total_money) as money'))
                    ->get();
        foreach ($departments as $key => $department) {
          $data['department'][] = [$department['department']->reference_code,$department['money']];
        }
        
        $staffArr = BOBill::where('id','!=','0')                    
                    ->groupBy('staff_id') 
                    ->with('staff')
                    ->select('staff_id',\DB::raw('SUM(bill_total_money) as money'))
                    ->orderBy('money','DESC')
                    ->take(10)
                    ->get();
        foreach ($staffArr as $key => $staff) {
          $data['staff'][] = [$staff['staff']->ub_title,$staff['money']];
        }
        // $data['staff'] =  $staffArr;
       
        return $data;
    }
    public function updateGroupToBill()
    {
        // $result= \DB::table('b_o_bills')->join('b_o_user','b_o_bills.staff_id','=','b_o_user.ub_id')->select('b_o_user.group_ids')->update(['b_o_bills.gb_id'=>'b_o_user.group_ids']);
        $arrBills = BOBill::get();
        $count=0;
        $arrUser = BOUser::get()->keyBy('ub_id')->toArray();
        // return $arrUser;
        foreach ($arrBills as $key => $bill) {
            $count++;
            BOBill::where('id',$bill['id'])->update(['gb_id'=>isset($arrUser[$bill['staff_id']])?(int)($arrUser[$bill['staff_id']]['group_ids']):'0']);
        }
      
        if($count){
            return $count;
        }else{
            return "Thất bại!";
        }
        
    }
    private function resetToken()
    {
        $res = \Request::all();
        if($res['id']){
            $staffInfo = BOUser::where('ub_id',$res['id'])->first();
            if($staffInfo){
                // Update token to null
                $tt = BOUser::where('ub_id',$res['id'])->update(['remember_jwt'=>NULL,'remember_token'=>NULL,'ub_token'=>NULL]);
                return 'Thành Công! Báo nhân viên login lại.';
                
            }else{
                return 'Không thấy nhân viên có mã'.$res['id'];
            }
        }else{
            return 'Chưa chọn nhân viên nào';
        }
    }
    public static function exportExcel($arr = [], $fileTitle, $sheetTitle = '', $fileType = 'xls' | 'xlsx',$type_export='Giao dịch')
    {
        if (!$sheetTitle) {
            $sheetTitle = $fileTitle;
        }
        // public static function logTransaction($msg, $type = 'info', $object = null, $save = true) {
        $userInfo = Auth::guard('loginfrontend')->user();
        if($userInfo){
            // dump($userInfo);
            $msg = 'Người dùng: '.$userInfo->ub_title.' - Đơn vị: '.BOUserGroup::where('gb_id',$userInfo->group_ids)->first()['gb_title'].' - đang tải danh sách <b>'.$type_export.'</b>. Nếu người dùng này không có quyền, vui lòng gọi ngay Kiểm soát nội bộ. Hotline: 113!';
             // dump( $msg);
             LogController::logTransaction($msg,'warning',null,true);
            \Excel::create($fileTitle, function($excel) use ($arr,$sheetTitle) {

                $excel->sheet($sheetTitle, function($sheet) use ($arr) {
                    // foreach ($arr as $key => $row) {
                    //  $sheet->setColumnFormat($row);
                    // }
                    // $sheet->setColumnFormat($arr);
                    $sheet->fromArray($arr);

                });
            })->export($fileType, ['Content-Encoding' => 'UTF-8',]);
        }else
        { 
            $url = url('sign-out');
            return redirect( $url);
        }
    }

    
}
