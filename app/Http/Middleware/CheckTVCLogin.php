<?php

namespace App\Http\Middleware;

use App\Http\Controllers\API\APIController;
use Closure;

class CheckTVCLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!APIController::_TVC_CHECK_LOGIN()) {
            return response()->json(['msg' => 'Phiên đăng nhập Tavico hết hạn!', 'success' => false, 'status' => 401], 401);
        }
        return $next($request);
    }
}
