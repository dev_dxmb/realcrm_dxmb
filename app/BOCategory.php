<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class BOCategory extends Model
{
    const ID_KEY = "cb_id";
    public $timestamps = false;
    const PROJECT_LEVEL = 1;
    const BUILDING_LEVEL = 2;
    const TYPE_PRIVATE = 'private';
    const TYPE_SHARED = 'share';
    
    protected $fillable = [
        'cb_id', 'cb_status', 'parent_id', 'cb_code', 'reference_code', 'cb_title', 'cb_description', 'extra_ids', 'updated_user_id', 'investor_id', 'ub_updated_time',
        'created_user_id', 'ub_created_time', 'cb_level', 'last_sync_tvc', 'type', 'active_booking', 'alias'
    ];

    /**
     * @param $value
     */
    public function setCbIdAttribute($value) {
        $this->attributes[self::ID_KEY] = $value?? strtotime("now");
    }

    /**
     * @param $uid
     */
    public function setUpdatedUserIdAttribute($uid) {
        $this->attributes['updated_user_id'] = $uid?? BOUser::getCurrentUID();
    }
    /**
     * @param $uid
     */
    public function setCreatedUserIdAttribute($uid) {
        $this->attributes['created_user_id'] = $uid?? BOUser::getCurrentUID();
    }

    /** Building -> Project
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sibling() {
        return $this->belongsTo(self::class, "parent_id", "id");
    }

    /** Project -> Buildings
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() {
        return $this->hasMany(self::class, "parent_id", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investor() {
        return $this->belongsTo(BOInvestor::class, 'investor_id', 'id');
    }
    /**

     * @Auth: Dienct
     * @Des : get all channel by parent id- default = 0
     * @since: 8/3/2017
     * 
     */
    public function getAllCategoriesByParentID($parent_id = 0, &$aryChildID) {
        $aryResult = DB::table('b_o_categories')->select('*')->where('parent_id', $parent_id)->where('cb_title','!=', '')->where('cb_status', 1)->get();

        foreach ($aryResult as $o_val) {
            $ary = array();
            $ary['cb_title'] = $o_val->cb_title;
            $ary['cb_level'] = $o_val->cb_level;
            $ary['cb_status'] = $o_val->cb_status;
            $ary['ub_updated_time'] = Util::sz_DateTimeFormat($o_val->ub_updated_time);
                $aryChildID[$o_val->cb_id] = $ary;
            if (!empty($aryResult)) {
                $this->getAllCategoriesByParentID($o_val->id, $aryChildID);
            }
        }
    }
    
    /**
     * @Auth: Dienct
     * @Des : get all channel ChildID by parent id- default = 0
     * @since: 8/3/2017
     * 
     */
    public function getAllCategoriesIDByParentID($parent_id = 0, &$aryChildID) {
        $aryResult = DB::table('b_o_categories')->select('cb_id', 'cb_title', 'cb_level')->where('parent_id', $parent_id)->get();
        foreach ($aryResult as $o_val) {
                $aryChildID[] = $o_val->cb_id;
            if (!empty($aryResult)) {
                $this->getAllCategoriesIDByParentID($o_val->id, $aryChildID);
            }
        }
    }
    /**
     * @Auth: Dienct
     * @Des : get all channel ChildID by parent id- default = 0
     * @since: 8/3/2017
     *
     */
    public static function getAllProject() {
        $aryResult = DB::table('b_o_categories')->select('id', 'cb_title', 'cb_id', 'cb_code')->where(array('parent_id' => 0 , 'cb_level' => 1))->get();
        return $aryResult;
    }
    public function getAllBuildingByProject($pid) {
        return DB::table('b_o_categories')->select('id', 'cb_title', 'cb_id', 'cb_code')->where(array('parent_id' => $pid , 'cb_level' => 2))->get();
    }
}
